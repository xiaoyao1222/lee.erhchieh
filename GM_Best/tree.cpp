//=================================================================
//制作者　李爾捷
//木アクタークラス（CTree,CTree02）
//フィールド管理するため唯一フィールドマネージャー内のアクター
//
//課題：01と02の統合整理が必要	→	TYPE追加予定
//=================================================================

#include "main.h"
#include "renderer.h"

#include "tree.h"

#include "model.h"
#include "shader.h"

CTree::CTree(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;
}
void CTree::Init()
{
}

void CTree::Uninit()
{

}

void CTree::Update()
{

}

void CTree::Draw(CShader* s, CModel* m)
{
	// マトリクス設定
	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::SetDepthTexture(1);

	m->Draw();
}

CTree_02::CTree_02(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;
}

void CTree_02::Init()
{

}

void CTree_02::Uninit()
{

}

void CTree_02::Update()
{

}

void CTree_02::Draw(CShader* s, CModel* m)
{
	// マトリクス設定
	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::SetDepthTexture(1);

	m->Draw();
}