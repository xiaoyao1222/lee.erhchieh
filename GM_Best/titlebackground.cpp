#include "main.h"
#include "renderer.h"

#include "titlebackground.h"

#include "texture.h"
#include "shader.h"


CTitleBackGround::CTitleBackGround(float dx, float dy, float width, float height)
{
	m_Pos = XMFLOAT2(dx, dy);
	m_Width = width;
	m_Height = height;

	m_Flame = 0.0f;
}

void CTitleBackGround::Init()
{
	//=============================================================================
	//頂点データの準備
	VERTEX_3D vertex[4];

	//左上
	vertex[0].Position = XMFLOAT3(m_Pos.x, m_Pos.y, 0.0f);
	vertex[0].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[0].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[0].TexCoord = XMFLOAT2(0.0f, 0.0f);

	//右上
	vertex[1].Position = XMFLOAT3(m_Pos.x + m_Width, m_Pos.y, 0.0f);
	vertex[1].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[1].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[1].TexCoord = XMFLOAT2(1.0f, 0.0f);

	//左下
	vertex[2].Position = XMFLOAT3(m_Pos.x, m_Pos.y + m_Height, 0.0f);
	vertex[2].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[2].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[2].TexCoord = XMFLOAT2(0.0f, 1.0f);

	//右下
	vertex[3].Position = XMFLOAT3(m_Pos.x + m_Width, m_Pos.y + m_Height, 0.0f);
	vertex[3].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[3].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[3].TexCoord = XMFLOAT2(1.0f, 1.0f);

	//頂点バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VERTEX_3D) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.pSysMem = vertex;

	CRenderer::GetDevice()->CreateBuffer(&bd, &sd, &m_VertexBuffer);
	//=============================================================================
}

void CTitleBackGround::Uninit()
{
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}
}

void CTitleBackGround::Update()
{
	m_Flame += 0.001f;

	if (m_Flame >= 360)
	{
		m_Flame = 0;
	}
}

void CTitleBackGround::Draw(CShader* s, std::list<CTexture*> t)
{

	CRenderer::SetDepthEnable(false);
	UINT stride = sizeof(VERTEX_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定

	TIME time;
	time.frame = XMFLOAT4(0.0f, 0.0f, 0.0f, m_Flame);
	CRenderer::SetTime(time);

	int texcnt = 0;

	for (auto tex : t)
	{
		CRenderer::SetTexture(tex, texcnt);
		texcnt++;
	}

	s->Set();															//2Dマトリクス設定
	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）
	CRenderer::GetDeviceContext()->Draw(4, 0);														//ポリゴン描画
	CRenderer::SetDepthEnable(true);

}