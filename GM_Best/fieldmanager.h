//=================================================================
//制作者　李爾捷
//フィールド管理クラス（CFieldManager）
//=================================================================

#pragma once

#define FIELD_WIDTH 200
#define FIELD_HEIGHT 200

#include "gameobject.h"
#include "noise.h"

class CNoise;
class CField;
class COcean;
class CTree;
class CTree_02;
class CWayPoint;

class CModel;
class CTexture;
class CShader;

class CModelManager;
class CTextureManager;
class CShaderManager;

class CActorManager;
class CCameraManager;

class CFieldManager : public CGameObject
{
public:
	enum FIELD_TYPE
	{
		NORMAL = 0,
		MOUNTAIN,
		DESERT
	};
private:
	CNoise* m_pNoise;
	FIELD_TYPE m_Type;

	bool TreeDebugDraw;
	bool FieldDebugDraw;
	bool OceanDebugDraw;
	bool WayPointDebugDraw;


	std::list<CField*>				m_pFields;
	std::list<CField*>::iterator	m_pStandingField = m_pFields.end();
	std::list<COcean*>				m_pOceans;
	std::list<CTree*>				m_pTrees;
	std::list<CTree_02*>			m_pTree_02s;
	std::list<CWayPoint*>			m_pPoints;

	CModel* m_pTreeModel;
	CModel* m_pTree_02Model;
	CModel* m_pCactusModel;
	CModel* m_WayPointDebugModel;

	std::list<CTexture*> m_pFieldTextures;
	std::list<CTexture*> m_pOceanTextures;

	CShader* m_pFieldShader;
	CShader* m_pOceanShader;
	CShader* m_pTreeShader;
	CShader* m_pShadowShader;
	CShader* m_WayPointDebugShader;

	CActorManager*	m_pActorM;
	CCameraManager* m_pCamM;

	void SpawnField(float x, float y);
	void CheckArea(XMFLOAT3 pos);
	float Distance(XMFLOAT3 p1, XMFLOAT3 p2);

public:
	CFieldManager() :
		TreeDebugDraw(false), FieldDebugDraw(false), OceanDebugDraw(false), WayPointDebugDraw(false),
		m_pTreeModel(nullptr), m_pTree_02Model(nullptr), m_pCactusModel(nullptr), m_WayPointDebugModel(nullptr),
		m_pFieldShader(nullptr), m_pOceanShader(nullptr), m_pTreeShader(nullptr), m_pShadowShader(nullptr),
		m_WayPointDebugShader(nullptr), m_pActorM(nullptr), m_pCamM(nullptr)
	{
		m_pFields.clear();
		m_pOceans.clear();
		m_pTrees.clear();
		m_pTree_02s.clear();
		m_pPoints.clear();

		m_pFieldTextures.clear();
		m_pOceanTextures.clear();
	}
	~CFieldManager() { Uninit(); }

	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CModelManager* m);
	void Load(CTextureManager* m);
	void Load(CShaderManager* m);
	void Load(CActorManager* m)		{ m_pActorM = m; }
	void Load(CCameraManager* m)	{ m_pCamM = m; }

	void Build(FIELD_TYPE fType = NORMAL, CNoise::NoiseType ntype = CNoise::NoiseType::Perlin);
	void SpawnTree(XMFLOAT3 pos, int type);
	void SpawnWayPoint(XMFLOAT3 pos);

	float		GetHeight(XMFLOAT3 position);
	FIELD_TYPE	GetType()	{ return m_Type; }
	CNoise*		GetNoise()	{ return m_pNoise; }
	CWayPoint*	GetWayPoint(int id);
};
