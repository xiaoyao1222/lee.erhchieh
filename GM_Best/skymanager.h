//=================================================================
//制作者　李爾捷
//空管理クラス（CSkyManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "polygon.h"
#include "fieldbase.h"

class CShaderManager;
class CTimeManager;
class CActorManager;

class CShader;

class CSkyManager : public CGameObject
{
private:
	CFieldBase* m_pSkyDome;
	CPolygon*	m_pCloud;

	CShader* m_pSkyDomeShader;
	CShader* m_pCloudShader;

	CActorManager* m_pActorM;

public:
	CSkyManager() :
		m_pSkyDome(nullptr), m_pCloud(nullptr), m_pSkyDomeShader(nullptr), m_pCloudShader(nullptr), m_pActorM(nullptr){}
	~CSkyManager() { Uninit(); }

	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CShaderManager* m);
	void Load(CActorManager* m) { m_pActorM = m; }

	CActorManager* GetActorM() { return m_pActorM; }
};
