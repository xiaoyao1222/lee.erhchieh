//=================================================================
//制作者　李爾捷
//タイトルUI管理クラス（CTitleUIManager）
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "titleuimanager.h"
#include "texturemanager.h"
#include "shadermanager.h"
#include "titlebackground.h"
#include "titleword.h"

void CTitleUIManager::Init()
{
	m_pBackGround = new CTitleBackGround(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_pBackGround->Init();

	m_pUI = new CTitleWord(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	m_pUI->Init();
}

void CTitleUIManager::Uninit()
{
	m_pBackGroundTextures.clear();

	m_pWordTextures.clear();

	if (m_pUI != nullptr)
	{
		delete m_pUI;
	}

	if (m_pBackGround != nullptr)
	{
		delete m_pBackGround;
	}

}

void CTitleUIManager::Update()
{
	m_pBackGround->Update();
	m_pUI->Update();
}

void CTitleUIManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;

	case NORMAL_MODE:

		m_pBackGround->Draw(m_pBackGroundShader, m_pBackGroundTextures);
		m_pUI->Draw(m_pUIShader, m_pWordTextures);
		break;

	case IMGUI_MODE:


		break;
	}
}

void CTitleUIManager::Load(CTextureManager* m)
{
	m_pBackGroundTextures.push_back(m->SetTexture("asset/title_back.png"));
	m_pWordTextures.push_back(m->SetTexture("asset/title_word.png"));
}

void CTitleUIManager::Load(CShaderManager* m)
{
	m_pBackGroundShader = m->SetShader("shader/TitleBackGroundVS.cso", "shader/2DPS.cso");
	m_pUIShader = m->SetShader("shader/2DVS.cso", "shader/2DPS.cso");
}