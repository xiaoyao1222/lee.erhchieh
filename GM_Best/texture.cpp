#define STB_IMAGE_IMPLEMENTATION

#include "main.h"
#include "renderer.h"
#include "texture.h"
#include "stb-master\stb_image.h"


void CTexture::Load(const char *FileName)
{

	unsigned char	header[18];
	unsigned char	*image;
	unsigned int	width, height;
	unsigned char	depth;
	unsigned int	bpp;
	unsigned int	size;


	// ヘッダ読み込み
	FILE* file;
	file = fopen(FileName, "rb");
	assert(file);

	fread(header, sizeof(header), 1, file);


	// 画像サイズ取得
	width = header[13] * 256 + header[12];
	height = header[15] * 256 + header[14];
	depth = header[16];

	if (depth == 32)
		bpp = 4;
	else if (depth == 24)
		bpp = 3;
	else
		bpp = 0;

	if (bpp != 4) {
		//assert(false);
	}

	size = width * height * bpp;

	image = (unsigned char*)new unsigned char[size];

	if (bpp == 4)
	{
		// メモリ確保
		image = (unsigned char*)new unsigned char[size];

		// 画像読み込み
		fread(image, size, 1, file);

		// R<->B
		for (unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++)
			{
				unsigned char c;
				c = image[(y * width + x) * bpp + 0];
				image[(y * width + x) * bpp + 0] = image[(y * width + x) * bpp + 2];
				image[(y * width + x) * bpp + 2] = c;
			}
		}
	}
	else if(bpp == 3)
	{
		unsigned char	*imagework;
		// メモリ確保
		image = (unsigned char*)new unsigned char[size + width * height];
		imagework = (unsigned char*)new unsigned char[size];

		// 画像読み込み
		fread(imagework, size, 1, file);

		// R<->B
		for (unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++)
			{
				image[(y * width + x) * (bpp + 1) + 2] = imagework[(y * width + x) * bpp + 2];
				image[(y * width + x) * (bpp + 1) + 1] = imagework[(y * width + x) * bpp + 1];
				image[(y * width + x) * (bpp + 1) + 0] = imagework[(y * width + x) * bpp + 0];
				image[(y * width + x) * (bpp + 1) + 3] = 0;
			}
		}
		delete imagework;
	}

	fclose(file);


	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (bpp != 4)
	{
		desc.Format = DXGI_FORMAT_B8G8R8X8_UNORM;
	}

	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = image;
	initData.SysMemPitch = width * 4;
	initData.SysMemSlicePitch = size;

	auto hr = CRenderer::GetDevice()->CreateTexture2D(&desc, &initData, &m_Texture);
	if (FAILED(hr)) {
		assert(false);
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc = {};
	SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	if (bpp != 4)
	{
		SRVDesc.Format = DXGI_FORMAT_B8G8R8X8_UNORM;
	}
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;

	hr = CRenderer::GetDevice()->CreateShaderResourceView(m_Texture, &SRVDesc, &m_ShaderResourceView);
	if (FAILED(hr))
	{
		assert(false);
	}


	delete image;
}

void CTexture::LoadSTB(const char *FileName)
{
	unsigned char* image;
	int width;
	int height;
	int bpp;
	unsigned int	size;

	// ファイルを読み込み、画像データを取り出す
	//   最後の引数でピクセルあたりのバイト数を強制できる
	image = stbi_load(FileName, &width, &height, &bpp, 4);

	size = width * height * bpp;
	
	if (bpp == 3)
	{
		// R<->B
		for (unsigned int y = 0; y < height; y++)
		{
			for (unsigned int x = 0; x < width; x++)
			{
				unsigned char c;
				c = image[(y * width + x) * (bpp + 1) + 0];
				image[(y * width + x) * (bpp + 1) + 0] = image[(y * width + x) * (bpp + 1) + 2];
				image[(y * width + x) * (bpp + 1) + 2] = c;
			}
		}
	}
	
	D3D11_TEXTURE2D_DESC desc;
	desc.Width = width;
	desc.Height = height;
	desc.MipLevels = 1;
	desc.ArraySize = 1;
	if (bpp == 3)
	{
		desc.Format = DXGI_FORMAT_B8G8R8X8_UNORM;
	}
	else if (bpp == 4)
	{
		desc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	}

	desc.SampleDesc.Count = 1;
	desc.SampleDesc.Quality = 0;
	desc.Usage = D3D11_USAGE_DEFAULT;
	desc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = image;
	initData.SysMemPitch = width * 4;
	initData.SysMemSlicePitch = size;

	auto hr = CRenderer::GetDevice()->CreateTexture2D(&desc, &initData, &m_Texture);
	if (FAILED(hr)) {
		assert(false);
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc = {};
	if (bpp == 3)
	{
		SRVDesc.Format = DXGI_FORMAT_B8G8R8X8_UNORM;
	}
	else if (bpp == 4)
	{
		SRVDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	}
	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	SRVDesc.Texture2D.MipLevels = 1;

	hr = CRenderer::GetDevice()->CreateShaderResourceView(m_Texture, &SRVDesc, &m_ShaderResourceView);
	if (FAILED(hr))
	{
		assert(false);
	}


	// メモリ上の画像データを破棄
	stbi_image_free(image);
}


void CTexture::Unload()
{
	m_Texture->Release();
	m_ShaderResourceView->Release();
}



