//=================================================================
//§ìÒ@¢·
//_NXiCCloudj
//=================================================================

#pragma once

#include "polygon.h"

class CCloud : public CPolygon
{
private:
	//¸_obt@
	ID3D11Buffer* m_VertexBuffer = NULL;

	bool m_debugdraw = false;
public:
	CCloud(){}
	CCloud(XMFLOAT2 pos, float width, float height);
	~CCloud() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;
	void Draw(CShader* s) override;
};
