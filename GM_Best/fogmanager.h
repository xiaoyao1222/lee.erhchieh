//=================================================================
//制作者　李爾捷
//フォグ管理クラス（CFogManager）
//=================================================================

#pragma once

#include "gameobject.h"

class CTimeManager;

class CFogManager : public CGameObject
{
private:
	FOG				m_pFog;

	CTimeManager*	m_pTimeM;
public:
	CFogManager(): m_pTimeM(nullptr){}
	~CFogManager() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(RenderMode mode) override;

	void SetFog(COLOR color, XMFLOAT4 disFog, XMFLOAT4 HeiFog);
	FOG GetFog() { return m_pFog; }

	void Load(CTimeManager* m) { m_pTimeM = m; }
};
