//=================================================================
//制作者　李爾捷
//スカイスフィアクラス（CSkySphere）
//半径（radius）と角度(angle)で描画する範囲と精度を決める
//常にプレイヤーの座標を追跡する
//
//修正した後穴を発見した、修正必要
//=================================================================

#include "main.h"
#include "renderer.h"

#include "skysphere.h"

#include "skymanager.h"
#include "actormanager.h"

#include "shader.h"
#include "player.h"

CSkySphere::CSkySphere(float radius, XMFLOAT2 angle):
	m_Indexcnt(0), m_Radius(radius), m_Angle(angle), m_pSkyM(nullptr)
{
	m_Transform.Position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_Transform.Rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_Transform.Scale = XMFLOAT3(1.0f, 1.0f, 1.0f);
}

void CSkySphere::Init(CGameObject* m)
{
	m_pSkyM = (CSkyManager*)m;

	//頂点データの準備
	int VertexCnt = (360 / m_Angle.x + 1) * (90 / m_Angle.y + 1);
	VERTEX_3D *pV = new VERTEX_3D[VertexCnt];
	{
		for (int iCnty = 0; iCnty <= (90 / m_Angle.y); iCnty++)
		{
			for (int iCntx = 0; iCntx <= (360 / m_Angle.x); iCntx++)
			{
				float y = m_Radius * cosf(XMConvertToRadians(m_Angle.y * iCnty));
				float height = m_Radius * sinf(XMConvertToRadians(m_Angle.y * iCnty));

				float x = y * cosf(XMConvertToRadians(m_Angle.x * iCntx));
				float z = y * sinf(XMConvertToRadians(m_Angle.x * iCntx));

				float u = 0.0f + ((float)m_Angle.x * iCntx) / 360;
				float v = 1.0f - ((float)m_Angle.y * iCnty) / 90;

				int cnt = iCntx + ((360 / m_Angle.x) + 1) * iCnty;
				pV[cnt] = { XMFLOAT3(x, height, -z),
					XMFLOAT3(0.0f,  1.0f, 0.0f),
					XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
					XMFLOAT2(u, v) };
			}
		}
	}

	//インデクスデータの準備
	m_Indexcnt = (2 + 2 * 360 / m_Angle.x) * 90 / m_Angle.y + (90 / m_Angle.y - 1) * 2;

	WORD* pIndex = new WORD[m_Indexcnt];
	{
		int edge = (360 / m_Angle.x + 2) * 90 / m_Angle.y - 1;
		for (int iCnt = 0; iCnt < edge; iCnt++)
		{
			if (iCnt % (int)(360 / m_Angle.x + 2) == (360 / m_Angle.x + 1) && iCnt != 0)
			{
				pIndex[iCnt * 2] = pIndex[(iCnt - 1) * 2 + 1];
				pIndex[iCnt * 2 + 1] = iCnt - (iCnt / (360 / m_Angle.x + 2));
			}
			else
			{
				pIndex[iCnt * 2] = iCnt - (iCnt / (360 / m_Angle.x + 2));
				pIndex[iCnt * 2 + 1] = 360 / m_Angle.x + 1 + iCnt - (iCnt / (360 / m_Angle.x + 2));
			}
		}
	}

	//頂点バッファ生成
	{
		D3D11_BUFFER_DESC vbd;
		ZeroMemory(&vbd, sizeof(vbd));
		vbd.Usage = D3D11_USAGE_DEFAULT;
		vbd.ByteWidth = sizeof(VERTEX_3D) * VertexCnt;
		vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
		vbd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA vsd;
		ZeroMemory(&vsd, sizeof(vsd));
		vsd.pSysMem = pV;

		CRenderer::GetDevice()->CreateBuffer(&vbd, &vsd, &m_VertexBuffer);

		delete pV;
	}

	//インデクスバッファ生成
	{
		D3D11_BUFFER_DESC ibd;
		ZeroMemory(&ibd, sizeof(ibd));
		ibd.Usage = D3D11_USAGE_DEFAULT;
		ibd.ByteWidth = sizeof(WORD) * m_Indexcnt;
		ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		ibd.CPUAccessFlags = 0;
		ibd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA isd;
		ZeroMemory(&isd, sizeof(isd));
		isd.pSysMem = pIndex;

		CRenderer::GetDevice()->CreateBuffer(&ibd, &isd, &m_IndexBuffer);

		delete pIndex;
	}

}

void CSkySphere::Uninit()
{
	//後片付け
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}

	if (m_IndexBuffer != NULL)
	{
		m_IndexBuffer->Release();
	}
}

void CSkySphere::Update()
{
	XMFLOAT3 playerpos = m_pSkyM->GetActorM()->GetPlayer()->GetPos();

	m_Transform.Position = playerpos;	
}

void CSkySphere::Draw(CShader* s)
{
	//フィールド描画
	UINT stride = sizeof(VERTEX_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;																					//ワールドマトリクス
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);														//拡大縮小
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);										//回転
	world *= XMMatrixTranslation(m_Transform.Position.x, 0.0f, m_Transform.Position.z);													//移動

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexed(m_Indexcnt, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(m_VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)
}