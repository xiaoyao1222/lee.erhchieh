//=================================================================
//制作者　李爾捷
//テクスチャ管理クラス（CTextureManager）
//１.各テクスチャのパスを使って管理する
//２.生成命令が来た時まずマップ内を確認する
//３.存在する	→	アドレスをコピー
//４.存在しない	→	ロードしてアドレスをコピー
//５.リターンアドレス
//=================================================================

#include "main.h"
#include "renderer.h"
#include "texturemanager.h"

void CTextureManager::Init()
{

}

void CTextureManager::Uninit()
{
	for (auto t : m_pTextures)
	{
		t.second->Unload();
		delete t.second;
	}

	m_pTextures.clear();
}

CTexture* CTextureManager::SetTexture(std::string texture_path)
{
	auto iter = m_pTextures.find(texture_path);

	if (iter == m_pTextures.end())
	{
		m_pTextures[texture_path] = new CTexture(texture_path.c_str());
	}

	CTexture* t = new CTexture();
	t = m_pTextures[texture_path];

	return t;
}