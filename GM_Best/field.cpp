//=================================================================
//制作者　李爾捷
//フィールドクラス（CField）
//１.マネージャーから取得したノイズタイプで各頂点を計算用のパラメータを決める
//（ウェイポイントと木の生成も同時処理）
//２.各頂点のノーマルを算出
//３.GetHeight関数で地面上判断をする
//
//課題；境界線問題と一枚の最大値を200突破
//=================================================================

#define _CRT_SECURE_NO_WARNINGS

#include <io.h>

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "field.h"

#include "fieldmanager.h"
#include "texture.h"
#include "shader.h"
#include "noise.h"

CField::CField(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale):
	m_Indexcnt(0), m_Cnt_X(0), m_Cnt_Z(0)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;
}

//cnt_x→Xの枚数	cnt_z→Zの枚数
void CField::Init(int cnt_x, int cnt_z, CFieldManager* manager)
{
	m_pFieldM = manager;
	CNoise* pNoise = m_pFieldM->GetNoise();

	CFieldManager::FIELD_TYPE t = manager->GetType();

	XMFLOAT3 c1 = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMFLOAT3 c2 = XMFLOAT3(0.0f, 0.0f, 1.0f);
	XMVECTOR vC1 = XMLoadFloat3(&c1);
	XMVECTOR vC2 = XMLoadFloat3(&c2);

	m_Cnt_X = cnt_x + 1;
	m_Cnt_Z = cnt_z + 1;
	float y;

	//頂点データの準備
	int VertexCnt = m_Cnt_X * m_Cnt_Z;
	m_pV = new VERTEX_FIELD_3D[VertexCnt];
	{
		for (int iCntz = 0; iCntz <= cnt_z; iCntz++)
		{
			for (int iCntx = 0; iCntx <= cnt_x; iCntx++)
			{
				float x = m_Transform.Position.x - (cnt_x) / 2 + iCntx;
				float z = m_Transform.Position.z - (cnt_z) / 2 + iCntz;
				switch (t)
				{
				case CFieldManager::NORMAL:
					y = pNoise->Call((float)x / 10, (float)z / 10, 10.0f, 100.0f) - 45.0f;
					break;

				case CFieldManager::MOUNTAIN:
					y = pNoise->Call((float)x / 10, (float)z / 10, 30.0f, 1.0f) - 100.0f;
					break;

				case CFieldManager::DESERT:
					y = pNoise->Call((float)x / 10, (float)z / 10, 15.0f, 50.0f);
					break;
				}

				m_pV[iCntx + (cnt_x + 1) * iCntz] = { XMFLOAT3(x, y, z),
					XMFLOAT3(0.0f,  1.0f, 0.0f),
					XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),
					XMFLOAT2(0.0f + iCntx, cnt_z - iCntz) };

				if (y < 4 && (int)x % 7 == 0 && (int)z % 7 == 0)
				{
					m_pFieldM->SpawnWayPoint(XMFLOAT3(x, 4.0f, z));
				}

				//木の座標生成
				{
					float tree_y = pNoise->Call((float)x / 10, (float)z / 10, 5.0f, 25.0f);

					switch (t)
					{
					case CFieldManager::NORMAL:
						if (y > 6 && y < 10)
						{
							if (tree_y > 14 && (int)x % 3 == 0 && (int)z % 3 == 0)
							{
								m_pFieldM->SpawnTree(XMFLOAT3(x, y - 0.3f, z), 0);
							}
						}
						else if (y > 5 && y < 7)
						{
							if (tree_y > 14 && (int)x % 3 == 0 && (int)z % 3 == 0)
							{
								m_pFieldM->SpawnTree(XMFLOAT3(x, y - 0.5f, z), 1);
							}
						}
						break;

					case CFieldManager::MOUNTAIN:

						break;

					case CFieldManager::DESERT:
						if (y > 20)
						{
							if (tree_y > 14.5f && (int)x % 20 == 0 && (int)z % 20 == 0)
							{
								m_pFieldM->SpawnTree(XMFLOAT3(x, y - 0.3f, z), 0);
							}

						}
						break;
					}


				}
			}
		}

		//ノーマル設定
		for (int iCntz = 1; iCntz < cnt_z; iCntz++)
		{
			for (int iCntx = 1; iCntx < cnt_x; iCntx++)
			{
				XMFLOAT3 va, vb, n, t, bi;
				va.x = m_pV[(iCntz - 1) * (cnt_x + 1) + iCntx].Position.x - m_pV[(iCntz + 1) * (cnt_x + 1) + iCntx].Position.x;
				va.y = m_pV[(iCntz - 1) * (cnt_x + 1) + iCntx].Position.y - m_pV[(iCntz + 1) * (cnt_x + 1) + iCntx].Position.y;
				va.z = m_pV[(iCntz - 1) * (cnt_x + 1) + iCntx].Position.z - m_pV[(iCntz + 1) * (cnt_x + 1) + iCntx].Position.z;

				vb.x = m_pV[iCntz * (cnt_x + 1) + (iCntx + 1)].Position.x - m_pV[iCntz * (cnt_x + 1) + (iCntx - 1)].Position.x;
				vb.y = m_pV[iCntz * (cnt_x + 1) + (iCntx + 1)].Position.y - m_pV[iCntz * (cnt_x + 1) + (iCntx - 1)].Position.y;
				vb.z = m_pV[iCntz * (cnt_x + 1) + (iCntx + 1)].Position.z - m_pV[iCntz * (cnt_x + 1) + (iCntx - 1)].Position.z;

				XMVECTOR vA = XMLoadFloat3(&va);
				XMVECTOR vB = XMLoadFloat3(&vb);
				XMVECTOR vN = XMVector3Cross(vB, vA);
				vN = XMVector3Normalize(vN);

				XMVECTOR vT, vT1, vT2;
				vT1 = XMVector3Cross(vN, vC1);
				vT2 = XMVector3Cross(vN, vC2);

				float l1, l2;

				XMStoreFloat(&l1, XMVector3Length(vT1));

				XMStoreFloat(&l2, XMVector3Length(vT2));

				if (l2 > l1)
				{
					vT = vT1;
				}
				else
				{
					vT = vT2;
				}

				vT = XMVector3Normalize(vT);

				XMVECTOR vBi = XMVector3Cross(vT, vN);
				vBi = XMVector3Normalize(vBi);

				XMStoreFloat3(&n, vN);
				XMStoreFloat3(&t, vT);
				XMStoreFloat3(&bi, vBi);

				m_pV[iCntx + (cnt_x + 1) * iCntz].Normal = n;
				m_pV[iCntx + (cnt_x + 1) * iCntz].Tangent = t;
				m_pV[iCntx + (cnt_x + 1) * iCntz].Binormal = bi;
			}
		}
	}

	//インデクスデータの準備
	m_Indexcnt = (2 + 2 * cnt_x) * cnt_z + (cnt_z - 1) * 2;
	WORD* pIndex = new WORD[m_Indexcnt];
	{
		int edge = (cnt_x + 2) * cnt_z - 1;

		for (int iCnt = 0; iCnt < edge; iCnt++)
		{
			if (iCnt % (cnt_x + 2) == cnt_x + 1 && iCnt != 0)
			{
				pIndex[iCnt * 2] = pIndex[(iCnt - 1) * 2 + 1];
				pIndex[iCnt * 2 + 1] = iCnt - (iCnt / (cnt_x + 2));
			}
			else
			{
				pIndex[iCnt * 2] = iCnt - (iCnt / (cnt_x + 2));
				pIndex[iCnt * 2 + 1] = cnt_x + 1 + iCnt - (iCnt / (cnt_x + 2));
			}
		}
	}

	//頂点バッファ生成
	{
		D3D11_BUFFER_DESC vbd;
		ZeroMemory(&vbd, sizeof(vbd));
		vbd.Usage = D3D11_USAGE_DEFAULT;
		vbd.ByteWidth = sizeof(VERTEX_FIELD_3D) * VertexCnt;
		vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
		vbd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA vsd;
		ZeroMemory(&vsd, sizeof(vsd));
		vsd.pSysMem = m_pV;

		CRenderer::GetDevice()->CreateBuffer(&vbd, &vsd, &m_VertexBuffer);
	}

	//インデクスバッファ生成
	{
		D3D11_BUFFER_DESC ibd;
		ZeroMemory(&ibd, sizeof(ibd));
		ibd.Usage = D3D11_USAGE_DEFAULT;
		ibd.ByteWidth = sizeof(WORD) * m_Indexcnt;
		ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;					//インデクスバッファ設定
		ibd.CPUAccessFlags = 0;
		ibd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA isd;
		ZeroMemory(&isd, sizeof(isd));
		isd.pSysMem = pIndex;

		CRenderer::GetDevice()->CreateBuffer(&ibd, &isd, &m_IndexBuffer);

		delete pIndex;
	}

}

void CField::Uninit()
{
	//後片付け
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}

	if (m_IndexBuffer != NULL)
	{
		m_IndexBuffer->Release();
	}

	delete m_pV;
}

void CField::Update()
{

}

void CField::Draw()
{
	float arr[200];

	static float UIZ = 0.0f;
	static float max_Hei = 25.0f;

	for (int x = 0; x < 200; x++)
	{
		arr[x] = GetHeight(XMFLOAT3(m_Transform.Position.x -FIELD_WIDTH/2 + x, 0.0f, m_Transform.Position.z + UIZ));
	}

	ImGui::SliderFloat("FieldPos.z", &UIZ, -100.0f, 100.0f);
	ImGui::SliderFloat("MaxHeight", &max_Hei, 0.0f, 200.0f);
	ImGui::PlotLines("PlotLines", arr, IM_ARRAYSIZE(arr), 0, "StandingFieldHeight", -20.0f, max_Hei,ImVec2(350, 50));
}

void CField::Draw(CShader* s)
{
	//フィールド描画

	UINT stride = sizeof(VERTEX_FIELD_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(0.0f, 0.0f, 0.0f);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexedInstanced(m_Indexcnt, 1, 0, 0, 0);

	//CRenderer::GetDeviceContext()->DrawIndexed(m_indexcnt, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(m_VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)
}

void CField::Draw(CShader* s, std::list<CTexture*> t)
{
	//フィールド描画

	UINT stride = sizeof(VERTEX_FIELD_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(0.0f, 0.0f, 0.0f);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	int texcnt = 0;

	for (auto tex : t)
	{
		CRenderer::SetTexture(tex, texcnt);
		texcnt++;
	}

	CRenderer::SetDepthTexture(texcnt);


	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexedInstanced(m_Indexcnt, 1, 0, 0, 0);

	//CRenderer::GetDeviceContext()->DrawIndexed(m_indexcnt, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(m_VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)
}

float CField::GetHeight(XMFLOAT3 position)
{
	int x, z;
	XMFLOAT3 va, vb, hp;
	XMVECTOR p0, p1, p2, v01, v02, v, n, Position;

	float dp0n, dvn, dpn, t;			//d→内積結果　t→最後の結果

	v = XMLoadFloat3(&XMFLOAT3(0.0f, -10.0f, 0.0f));
	Position = XMLoadFloat3(&position);

	x = (position.x - m_Transform.Position.x + ((m_Cnt_X-1)) / 2);
	z = (position.z - m_Transform.Position.z + ((m_Cnt_X - 1)) / 2);

	va.x = m_pV[z  + (x + 1)].Position.x - m_pV[(z + 1)  + x].Position.x;
	va.y = m_pV[z  + (x + 1)].Position.y - m_pV[(z + 1)  + x].Position.y;
	va.z = m_pV[z  + (x + 1)].Position.z - m_pV[(z + 1)  + x].Position.z;

	vb.x = position.x - m_pV[(z + 1)  + x].Position.x;
	vb.y = position.y - m_pV[(z + 1)  + x].Position.y;
	vb.z = position.z - m_pV[(z + 1)  + x].Position.z;

	if (va.z * vb.x - va.x * vb.z > 0.0f)
	{
		p0 = XMLoadFloat3(&m_pV[(z + 1)*m_Cnt_X + (x+1)].Position);
		p1 = XMLoadFloat3(&m_pV[z * m_Cnt_X + (x + 1)].Position);
		p2 = XMLoadFloat3(&m_pV[(z + 1)*m_Cnt_X + x].Position);
		n = XMLoadFloat3(&m_pV[(z + 1)*m_Cnt_X + (x + 1)].Normal);

	}
	else
	{
		p0 = XMLoadFloat3(&m_pV[z*m_Cnt_X + x].Position);

		int cnt = (z - 1)*m_Cnt_X + x;

		if (cnt < 0)
		{
			cnt = (z + 1)*m_Cnt_X + x;
		}

		p1 = XMLoadFloat3(&m_pV[cnt].Position);
		p2 = XMLoadFloat3(&m_pV[z * m_Cnt_X + (x + 1)].Position);
		n = XMLoadFloat3(&m_pV[z*m_Cnt_X + x].Normal);

	}

	v01 = p1 - p0;
	v02 = p2 - p0;

	XMStoreFloat(&dvn, XMVector3Dot(v, n));

	XMStoreFloat(&dp0n, XMVector3Dot(p0, n));

	XMStoreFloat(&dpn, XMVector3Dot(Position, n));

	t = (dp0n - dpn) / dvn;

	XMStoreFloat3(&hp, Position + v*t);

	return hp.y;
}