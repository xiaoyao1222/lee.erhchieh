#pragma once

class CTexture
{

public:
	CTexture() {};
	CTexture(const char *FileName) { LoadSTB(FileName); }

	~CTexture() {}

	void Load( const char *FileName );
	void LoadSTB(const char *FileName);
	void Unload();

	ID3D11ShaderResourceView* GetShaderResourceView(){ return m_ShaderResourceView; }


private:

	ID3D11Texture2D*			m_Texture;
	ID3D11Resource*				m_Resource;
	ID3D11ShaderResourceView*	m_ShaderResourceView;
	ID3D11SamplerState*			m_pSampler;

};