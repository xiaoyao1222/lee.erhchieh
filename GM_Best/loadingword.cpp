#include "main.h"
#include "renderer.h"

#include "loadingword.h"

#include "texture.h"
#include "shader.h"

void CLoadingWord::Init(float dx, float dy, float width, float height)
{
	//=============================================================================
	//頂点データの準備
	VERTEX_3D vertex[4];

	//左上
	vertex[0].Position = XMFLOAT3(dx - width / 2, dy - height / 2, 0.0f);
	vertex[0].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[0].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[0].TexCoord = XMFLOAT2(0.0f, 0.0f);

	//右上
	vertex[1].Position = XMFLOAT3(dx + width / 2, dy - height / 2, 0.0f);
	vertex[1].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[1].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[1].TexCoord = XMFLOAT2(1.0f, 0.0f);

	//左下
	vertex[2].Position = XMFLOAT3(dx - width / 2, dy + height / 2, 0.0f);
	vertex[2].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[2].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[2].TexCoord = XMFLOAT2(0.0f, 1.0f);

	//右下
	vertex[3].Position = XMFLOAT3(dx + width / 2, dy + height / 2, 0.0f);
	vertex[3].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
	vertex[3].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	vertex[3].TexCoord = XMFLOAT2(1.0f, 1.0f);

	//頂点バッファ生成
	D3D11_BUFFER_DESC bd;
	ZeroMemory(&bd, sizeof(bd));
	bd.Usage = D3D11_USAGE_DEFAULT;
	bd.ByteWidth = sizeof(VERTEX_3D) * 4;
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
	bd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA sd;
	ZeroMemory(&sd, sizeof(sd));
	sd.pSysMem = vertex;

	CRenderer::GetDevice()->CreateBuffer(&bd, &sd, &m_VertexBuffer);
	//=============================================================================

	m_Texture = new CTexture("asset/loading.png");

	m_Shader = new CShader();
	m_Shader->Init("shader/LoadingVS.cso", "shader/LoadingPS.cso");
	m_Flame = 0.0f;
}

void CLoadingWord::Uninit()
{
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}

	if (m_Shader != NULL)
	{
		m_Shader->Uninit();
		delete m_Shader;
	}
}

void CLoadingWord::Update()
{
	m_Flame += 1.0f;

	if (m_Flame >= 360)
	{
		m_Flame = 0;
	}
}

void CLoadingWord::Draw()
{
	CRenderer::SetDepthEnable(false);
	UINT stride = sizeof(VERTEX_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定

	CRenderer::SetTexture(m_Texture);

	TIME t;
	t.frame = XMFLOAT4(0.0f, 0.0f, 0.0f, m_Flame);
	CRenderer::SetTime(t);

	m_Shader->Set();

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）
	CRenderer::GetDeviceContext()->Draw(4, 0);														//ポリゴン描画
	CRenderer::SetDepthEnable(true);
}

