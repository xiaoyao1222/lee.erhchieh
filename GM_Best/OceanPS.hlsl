//*****************************************************************************
// 定数バッファ
//*****************************************************************************

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

//カメラバッファ
struct CAMERA
{
	float4 pos;
	float4 front;
};

cbuffer CameraBuffer : register(b5)
{
	CAMERA Camera;
}

//フォグバッファ
struct FOG
{
	float4		Color;
	//x->fogStart y->fogEnd z->near w->far
	float4	DistanceFog;
	//x->density y->densityAttenuation
	float4	HeightFog;
};

cbuffer FogBuffer : register(b6)
{
	FOG Fog;
}

//*****************************************************************************
// グローバル変数
//*****************************************************************************
Texture2D		g_Texture_Color1  : register(t0);
Texture2D		g_Texture_Norm1   : register(t1);
Texture2D		g_Texture_Height1 : register(t2);
Texture2D		g_Texture_Rough   : register(t3);
Texture2D		g_Texture_Occ	  : register(t4);

Texture2D		g_ShadowMap		  : register(t5);

SamplerState	g_SamplerState    : register(s0);

SamplerState	g_ShadowMapState  : register(s1);

SamplerComparisonState g_ShadoMapSampler : register(s2);

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 posW	: POSITION1;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;

	float4 cameradir : POSITION2;
	float4 lightViewPos : TEXCOORD1;
};

float when_eq(float x, float y)
{
	return 1.0f - abs(sign(x - y));
}

float when_gt(float x, float y)
{
	return max(sign(x - y), 0.0f);
}

float and(float x, float y)
{
	return x * y;
}

float4 shadowcolor(float3 normal, float4 lightpos)
{
	float4 color;
	float shadowColor;

	{
		float2 depthTexCoord;
		// 深度テクスチャのUV計算
		depthTexCoord.x = 0.5f + (lightpos.x / lightpos.w * 0.5f);
		depthTexCoord.y = 0.5f - (lightpos.y / lightpos.w * 0.5f);

		float PercentLit = 0;

		// ライトビュー上でのピクセルの深度値
		float depthcompare = lightpos.z / lightpos.w;

		// バイアス
		float bias = 0.005f;
		depthcompare -= bias;

		float cnt = 0;
		for (int i = -2; i < 3; i += 2)
		{
			for (int j = -2; j < 3; j += 2)
			{
				// ミップマップ0限定で depthcompare >= 深度マップの深度値 の場合、0
				PercentLit += g_ShadowMap.SampleCmpLevelZero(g_ShadoMapSampler,
					float2(
						depthTexCoord.x + (float)i * (1.0f / 1920.0f),
						depthTexCoord.y + (float)j * (1.0f / 1080.0f)),
					depthcompare);
				cnt++;
			}
		}

		PercentLit = PercentLit / cnt;
		shadowColor = clamp(PercentLit, 0.3f, 1.0f);
	}

	float3 lightDir = normalize(-Light.Direction);
	// ハーフランバート
	float lambert = dot(lightDir, normal);
	lambert = lambert * 0.5f + 0.5f;

	shadowColor = min(lambert, shadowColor);

	color.xyz = float3(shadowColor, shadowColor, shadowColor);
	color.w = 1.0f;

	return color;
}

float3 normalmap(float3 norm, PixelInputType input)
{
	// Expand the range of the normal value from (0, +1) to (-1, +1).
	norm = (norm * 2.0f) - 1.0f;

	// Calculate the normal from the data in the bump map.
	norm = (norm.x * input.tangent) + (norm.y * input.binormal) + (norm.z * input.normal);

	// Normalize the resulting bump normal.
	norm = normalize(norm);

	return norm;
}

float4 lightcolor(float3 normal, float4 diffuse)
{
	float lightIntensity;
	float3 lightDir;
	float4 color;

	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);

	// Calculate the amount of light on this pixel based on the bump map normal value.
	lightIntensity = saturate(dot(normal, lightDir));

	// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
	color = saturate(diffuse * lightIntensity);

	color.w = 1.0f;

	return color;
}

float4 specularcolor(float3 normal, float4 pos)
{
	float3 lightDir;
	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);
	float light = (dot(normalize(lightDir), normal.xyz));

	float3 ref = reflect(-lightDir, normal.xyz);
	float3 toEye = Camera.pos.xyz - pos;
	ref = normalize(ref);
	toEye = normalize(toEye);

	float s = dot(ref, toEye);
	s = saturate(s);
	s = pow(s, 10);
	return float4(s, s, s, 0.0f);
}

float distancefog(float distance)
{
	float fogStart = Fog.DistanceFog.x;
	float fogEnd = Fog.DistanceFog.y;

	float near = Fog.DistanceFog.z;
	float far = Fog.DistanceFog.w;
	float linerDepth = 1.0 / (far - near);

	float depthValue = distance * linerDepth;

	float fogFactor = saturate((fogEnd - depthValue) / (fogEnd - fogStart));

	return fogFactor;
}

float calcFogHeightExp(float3 inPos, float3 camPos, float densityY0, float densityAttenuation)
{
	float3 v = camPos - inPos;
	float l = length(v);
	float ret;
	float tmp = l * densityY0 * exp(-densityAttenuation * inPos.y);
	if (v.y == 0.0) // 単純な均一フォグ
	{
		ret = exp(-tmp);
	}
	else
	{
		float kvy = densityAttenuation * v.y;
		ret = exp(tmp / kvy * (exp(-kvy) - 1.0));
	}
	return ret;
}
//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float4 blendcolor, tex, light, shadow, specular, rough, occ, ref;
	float3 normal;

	float hScale;
	float2 hTex;

	tex = g_Texture_Color1.Sample(g_SamplerState, input.tex);
	hScale = g_Texture_Height1.Sample(g_SamplerState, input.tex).x * 0.08f;
	hTex = input.tex - hScale * input.cameradir.xy;
	normal = normalmap(g_Texture_Norm1.Sample(g_SamplerState, hTex), input);

	occ = g_Texture_Occ.Sample(g_SamplerState, hTex);

	light = lightcolor(normal, input.diffuse) * occ;

	shadow = shadowcolor(normal, input.lightViewPos);

	rough = g_Texture_Rough.Sample(g_SamplerState, input.tex);

	specular = specularcolor(normal, input.posW) * rough;

	blendcolor = saturate(tex * light * shadow + specular);

	blendcolor.a *= 0.7f;

	//フォグ処理
	{
		float4 fogcolor = float4(0.7f, 0.7f, 0.7f, 1.0f);

		float dfog = distancefog(distance(Camera.pos, input.posW));

		float hfog = calcFogHeightExp(input.posW, Camera.pos, Fog.HeightFog.x, Fog.HeightFog.y);

		blendcolor = lerp(Fog.Color, blendcolor, dfog);

		blendcolor = lerp(Fog.Color, blendcolor, hfog);
	}

	return blendcolor;
}
