//=================================================================
//制作者　李爾捷
//フィールドベーステンプレクラス（CFieldBase）
//トランスフォーム持ち（座標、回転、大きさ）
//描画は複数パターン対応
//１.引数なし						→	アセット全部使わない、基本ImGuiまだはデバック用
//２.シェーダのみ					→	シェーダだけで描画できるオブジェクト使用
//３.シェーダ、テクスチャ			→	シェーダとテクスチャで描画する時使用
//=================================================================

#pragma once

class CTexture;
class CShader;
class CGameObject;
class CFieldManager;

class CFieldBase
{
public:
	CFieldBase() {};
	virtual ~CFieldBase() {};

	virtual void Init() {}
	virtual void Init(CGameObject* m) {}

	virtual void Uninit() {}
	virtual void Update() {}

	virtual void Draw() {}
	virtual void Draw(CShader* s) {}
	virtual void Draw(CShader* s, std::list<CTexture*> t) {}

	void SetTransform(Transform t) { m_Transform = t; }
	void SetPos(XMFLOAT3 p) { m_Transform.Position = p; }
	void SetRot(XMFLOAT3 r) { m_Transform.Rotation = r; }
	void SetScale(XMFLOAT3 s) { m_Transform.Scale = s; }

	Transform& GetTransform() { return m_Transform; }
	const XMFLOAT3& GetPos() const { return m_Transform.Position; }
	const XMFLOAT3& GetRot() const { return m_Transform.Rotation; }
	const XMFLOAT3& GetScale() const { return m_Transform.Scale; }


	void SetDestroy() { m_Destroy = true; }
	bool IsDestroy() { return m_Destroy; }
	bool Destroy() {
		if (m_Destroy)
		{
			delete this;
			return true;
		}
		else
		{
			return false;
		}
	}
protected:
	Transform m_Transform;
	CFieldManager* m_pFieldM;

	bool m_Destroy = false;

};

