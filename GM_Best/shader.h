#pragma once

#include "light.h"

class CShader
{
private:
	ID3D11VertexShader*     m_VertexShader;
	ID3D11PixelShader*      m_PixelShader;
	ID3D11InputLayout*      m_VertexLayout;

public:
	CShader(){}
	CShader(const char* VertexShader, const char* PixelShader) { Init(VertexShader, PixelShader); }
	~CShader() {}

	void Init( const char* VertexShader, const char* PixelShader);
	void Uninit();
	void Set();
};