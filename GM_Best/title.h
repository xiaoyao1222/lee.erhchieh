#pragma once

#include "Scene.h"

class CTextureManager;
class CShaderManager;

class CTimeManager;
class CTitleUIManager;

class CTitle : public CScene
{
private:
	CTextureManager*	m_pTexM;
	CShaderManager*		m_pShaderM;

	CTitleUIManager*	m_pTitleUIM;

public:
	void Init();
	void Uninit();
	void Update();
	void Draw();
};
