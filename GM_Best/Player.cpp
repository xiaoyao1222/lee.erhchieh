//=================================================================
//制作者　李爾捷
//プレイヤーアクタークラス（CPlayer）
//操作可能アクター
//飛行モードと移動制限フラグがある
//着地処理ある
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"
#include "input.h"

#include "player.h"

#include "actormanager.h"
#include "fieldmanager.h"


CPlayer::CPlayer(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;

	m_Speed = 0.5f;
	m_Front = XMFLOAT3(0.0f, 0.0f, 1.0f);

	m_FlyingMode = true;
	m_DebugMoveRelease = false;
}

void CPlayer::Init(CGameObject* m)
{
	m_pActorM = (CActorManager*)m;
}

void CPlayer::Uninit()
{
	
}

void CPlayer::Update()
{
	//上下左右回転運動
	{
		if (CInput::GetKeyPress(VK_LEFT))
		{
			m_Transform.Rotation.y -= m_Speed / 2.0f;
		}

		if (CInput::GetKeyPress(VK_RIGHT))
		{
			m_Transform.Rotation.y += m_Speed/2.0f;
		}

		if (CInput::GetKeyPress(VK_UP))
		{
			m_Transform.Rotation.x -= m_Speed / 2.0f;
		}

		if (CInput::GetKeyPress(VK_DOWN))
		{
			m_Transform.Rotation.x += m_Speed / 2.0f;
		}
	}

	XMFLOAT3 right;

	//rightとfront方向を算出
	{
		XMMATRIX rotMatrix;

		rotMatrix = XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);

		XMVECTOR vf = XMLoadFloat3(&XMFLOAT3(0.0f, 0.0f, 1.0f));

		XMVECTOR vr = XMLoadFloat3(&XMFLOAT3(1.0f, 0.0f, 0.0f));

		vr = XMVector3TransformCoord(vr, rotMatrix);
		vr = XMVector3Normalize(vr);

		vf = XMVector3TransformCoord(vf, rotMatrix);
		vf = XMVector3Normalize(vf);

		XMStoreFloat3(&right, vr);
		XMStoreFloat3(&m_Front, vf);
	}
	
	//左右前後移動
	{
		if (CInput::GetKeyPress('W'))
		{
			Move(m_Front, m_Speed);
		}

		if (CInput::GetKeyPress('S'))
		{
			Move(m_Front, -m_Speed);
		}

		if (CInput::GetKeyPress('A'))
		{
			Move(right, -m_Speed);
		}
		if (CInput::GetKeyPress('D'))
		{
			Move(right, +m_Speed);
		}
	}

	//上下移動
	{
		if (CInput::GetKeyPress(VK_SPACE))
		{
			m_Transform.Position.y += m_Speed;
		}

		if (CInput::GetKeyPress('B'))
		{
			m_Transform.Position.y -= m_Speed;
		}
	}

	//飛行と地面上処理
	{
		if (!m_FlyingMode)
		{
			m_Transform.Position.y -= VALUE_GRAVITY;
		}

		m_Transform.Position.y = std::max(m_Transform.Position.y, m_pActorM->GetFieldM()->GetHeight(m_Transform.Position) + 0.7f);
	}

	//エリア制限
	if (!m_DebugMoveRelease)
	{
		m_Transform.Position.x = std::min(m_Transform.Position.x, 200.0f);
		m_Transform.Position.z = std::min(m_Transform.Position.z, 200.0f);
		m_Transform.Position.x = std::max(m_Transform.Position.x, -200.0f);
		m_Transform.Position.z = std::max(m_Transform.Position.z, -200.0f);
	}

}

void CPlayer::Draw()
{
	//プレイヤーのImgui
	if (ImGui::BeginTabItem("Player")) {
		ImGui::Text("Pos = %.2f, %.2f, %.2f", m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);
		ImGui::Text("Rot = %.2f, %.2f, %.2f", m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
		ImGui::Text("Scale = %.2f, %.2f, %.2f", m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
		ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.5f);
		ImGui::SliderFloat("speed", &m_Speed, 0.1f, 1.0f);
		ImGui::Checkbox("move_release", &m_DebugMoveRelease);
		ImGui::SameLine();
		ImGui::Checkbox("flying_mode", &m_FlyingMode);
		ImGui::EndTabItem();
	}

}

//移動関連　dir->方向　speed->速度
void CPlayer::Move(XMFLOAT3 dir, float speed)
{
	m_Transform.Position.x += dir.x * speed;
	m_Transform.Position.y += dir.y * speed;
	m_Transform.Position.z += dir.z * speed;
}