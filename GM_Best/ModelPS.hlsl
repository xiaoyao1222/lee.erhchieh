//*****************************************************************************
// 定数バッファ
//*****************************************************************************
// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

//カメラバッファ
struct CAMERA
{
	float4 pos;
	float4 front;
};

cbuffer CameraBuffer : register(b5)
{
	CAMERA Camera;
}

//フォグバッファ
struct FOG
{
	float4		Color;
	//x->fogStart y->fogEnd z->near w->far
	float4	DistanceFog;
	//x->density y->densityAttenuation
	float4	HeightFog;
};

cbuffer FogBuffer : register(b6)
{
	FOG Fog;
}

//*****************************************************************************
// グローバル変数
//*****************************************************************************
Texture2D		g_Texture_Color1  : register(t0);

Texture2D		g_Texture_Color2  : register(t1);

Texture2D		g_ShadowMap		  : register(t2);

SamplerState	g_SamplerState    : register(s0);

SamplerState	g_ShadowMapState  : register(s1);

SamplerComparisonState g_ShadoMapSampler : register(s2);

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 posW	: POSITION1;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;

	float4 lightViewPos : TEXCOORD1;
};

float when_eq(float x, float y)
{
	return 1.0f - abs(sign(x - y));
}

float when_gt(float x, float y)
{
	return max(sign(x - y), 0.0f);
}

float and(float x, float y)
{
	return x * y;
}

float4 lightcolor(float3 normal, float4 diffuse)
{
	float lightIntensity;
	float3 lightDir;
	float4 color;

	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);

	// Calculate the amount of light on this pixel based on the bump map normal value.
	lightIntensity = saturate(dot(normal, lightDir));

	// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
	color = saturate(diffuse * lightIntensity);

	color.w = 1.0f;

	return color;
}

float4 shadowcolor(float3 normal, float4 lightpos)
{
	float4 color;
	float shadowColor;

	{
		float2 depthTexCoord;
		// 深度テクスチャのUV計算
		depthTexCoord.x = 0.5f + (lightpos.x / lightpos.w * 0.5f);
		depthTexCoord.y = 0.5f - (lightpos.y / lightpos.w * 0.5f);

		float PercentLit = 0;

		// ライトビュー上でのピクセルの深度値
		float depthcompare = lightpos.z / lightpos.w;

		// バイアス
		float bias = 0.005f;
		depthcompare -= bias;

		float cnt = 0;
		for (int i = -2; i < 3; i += 2)
		{
			for (int j = -2; j < 3; j += 2)
			{
				// ミップマップ0限定で depthcompare >= 深度マップの深度値 の場合、0
				PercentLit += g_ShadowMap.SampleCmpLevelZero(g_ShadoMapSampler,
					float2(
						depthTexCoord.x + (float)i * (1.0f / 1920.0f),
						depthTexCoord.y + (float)j * (1.0f / 1080.0f)),
					depthcompare);
				cnt++;
			}
		}

		PercentLit = PercentLit / cnt;
		shadowColor = clamp(PercentLit, 0.3f, 1.0f);
	}

	float3 lightDir = normalize(-Light.Direction);
	// ハーフランバート
	float lambert = dot(lightDir, normal);
	lambert = lambert * 0.5f + 0.5f;

	shadowColor = min(lambert, shadowColor);

	color.xyz = float3(shadowColor, shadowColor, shadowColor);
	color.w = 1.0f;

	return color;
}

float4 specularcolor(float3 normal, float4 pos)
{
	float3 lightDir;
	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);
	float light = (dot(normalize(lightDir), normal.xyz));

	float3 ref = reflect(-lightDir, normal.xyz);
	float3 toEye = Camera.pos.xyz - pos;
	ref = normalize(ref);
	toEye = normalize(toEye);

	float s = dot(ref, toEye);
	s = saturate(s);
	s = pow(s, 20);
	return float4(s, s, s, 0.0f);
}

//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float4 blendcolor, tex, light, shadow, specular, ref_tex;

	tex = g_Texture_Color1.Sample(g_SamplerState, input.tex);

	float3 lightdr = normalize(Light.Direction.xyz);

	light = lightcolor(input.normal.xyz, input.diffuse);

	shadow = shadowcolor(input.normal, input.lightViewPos);

	specular = specularcolor(input.normal.xyz, input.posW);

	blendcolor = saturate(tex * shadow + specular);

	{
		float3 toEye = input.posW.xyz - Camera.pos.xyz;

		toEye = normalize(toEye);

		float light = 0.5f - dot(input.normal, lightdr.xyz)*0.49f;

		float facing = dot(-toEye, input.normal) * 0.99f;

		float2 toonTex;
		toonTex.x = light;
		toonTex.y = facing;

		blendcolor *= g_Texture_Color2.Sample(g_SamplerState, toonTex.xy);
	}
	return blendcolor;
}
