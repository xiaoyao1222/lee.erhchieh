Texture2D		g_ShadowMap		  : register(t0);

SamplerState	g_SamplerState    : register(s0);


//*****************************************************************************
// インプットプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex : TEXCOORD0;
};

//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float4 blendColor, tex;

	tex = g_ShadowMap.Sample(g_SamplerState, input.tex);

	blendColor = tex * input.diffuse;

	return blendColor;
}