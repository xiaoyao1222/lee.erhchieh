//=================================================================
//制作者　李爾捷
//雲クラス（CCloud）
//2Dポリゴン
//描画時は深度チェックを解除
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "cloud.h"

#include "shader.h"

CCloud::CCloud(XMFLOAT2 pos, float width, float height)
{
	m_Pos = pos;
	m_Width = width;
	m_Height = height;
}

void CCloud::Init()
{
	//頂点データの準備
	VERTEX_3D vertex[4];
	{
		//左上
		vertex[0].Position = XMFLOAT3(m_Pos.x, m_Pos.y, 0.0f);
		vertex[0].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		vertex[0].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		vertex[0].TexCoord = XMFLOAT2(0.0f, 0.0f);

		//右上
		vertex[1].Position = XMFLOAT3(m_Pos.x + m_Width, m_Pos.y, 0.0f);
		vertex[1].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		vertex[1].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		vertex[1].TexCoord = XMFLOAT2(1.0f, 0.0f);

		//左下
		vertex[2].Position = XMFLOAT3(m_Pos.x, m_Pos.y + m_Height, 0.0f);
		vertex[2].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		vertex[2].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		vertex[2].TexCoord = XMFLOAT2(0.0f, 1.0f);

		//右下
		vertex[3].Position = XMFLOAT3(m_Pos.x + m_Width, m_Pos.y + m_Height, 0.0f);
		vertex[3].Normal = XMFLOAT3(0.0f, 1.0f, 0.0f);
		vertex[3].Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
		vertex[3].TexCoord = XMFLOAT2(1.0f, 1.0f);
	}

	//頂点バッファ生成
	{
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(VERTEX_3D) * 4;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.pSysMem = vertex;

		CRenderer::GetDevice()->CreateBuffer(&bd, &sd, &m_VertexBuffer);
	}
}

void CCloud::Uninit()
{
	//後片付け
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}
}

void CCloud::Update()
{

}

void CCloud::Draw()
{
	if (ImGui::BeginTabItem("cloud")) {
		ImGui::Checkbox("cloud_close", &m_debugdraw);
		ImGui::EndTabItem();
	}
}

void CCloud::Draw(CShader* s)
{
	if (m_debugdraw)
	{
		return;
	}

	UINT stride = sizeof(VERTEX_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::SetDepthEnable(false);
	s->Set();														//2Dマトリクス設定
	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）
	CRenderer::GetDeviceContext()->Draw(4, 0);

	CRenderer::SetDepthEnable(true);
}
