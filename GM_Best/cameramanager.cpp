//=================================================================
//制作者　李爾捷
//カメラ管理クラス（CCameraManager）
//=================================================================

#include "main.h"
#include "renderer.h"

#include "cameramanager.h"

void CCameraManager::Init()
{
	m_pCam = new CCamera(XMFLOAT3(0.0f, 1.5f, 0.0f), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f));
	m_pCam->Init(this);
}

void CCameraManager::Uninit()
{
	if (m_pCam != nullptr)
	{
		delete m_pCam;
	}
}

void CCameraManager::Update()
{
	m_pCam->Update();
}

void CCameraManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;

	case NORMAL_MODE:
		m_pCam->Draw();

		break;
	case IMGUI_MODE:


		break;
	}
}