#pragma once
//=================================================================
//制作者　李爾捷
//船ステートテンプレと何もないクラス（CBoatState CBoatNone）
//=================================================================

class CBoat;

class CBoatState
{
public:
	virtual ~CBoatState(){}
	virtual void Update(CBoat* boat) = 0;
	virtual void Draw() = 0;
};

class CBoatStateNone : public CBoatState
{
public:
	~CBoatStateNone() {}
	void Update(CBoat* boat) override {}
	void Draw() override {}
};
