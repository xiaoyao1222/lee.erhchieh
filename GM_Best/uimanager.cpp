//=================================================================
//����ҁ@������
//UI�Ǘ��N���X�iCUIManager�j
//=================================================================

#include "main.h"
#include "renderer.h"

#include "uimanager.h"
#include "texturemanager.h"
#include "shadermanager.h"
#include "depthmapui.h"

void CUIManager::Init()
{
	m_pMapUI = new CDepthMapUI(SCREEN_WIDTH - 300.0f, 0, 300.0f, 300.0f);
	m_pMapUI->Init();
}

void CUIManager::Uninit()
{
	if (m_pMapUI != nullptr)
	{
		delete m_pMapUI;
	}

}

void CUIManager::Update()
{
	m_pMapUI->Update();
}

void CUIManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;

	case NORMAL_MODE:

		m_pMapUI->Draw(m_pMapUIShader);
		break;

	case IMGUI_MODE:

		m_pMapUI->Draw();
		break;
	}
}

void CUIManager::Load(CTextureManager* m)
{

}

void CUIManager::Load(CShaderManager* m)
{
	m_pMapUIShader = m->SetShader("shader/2DVS.cso", "shader/2DPS.cso");
}