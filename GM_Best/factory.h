//=================================================================
//制作者　李爾捷
//ファクトリーテンプレクラス（CFactory）
//ファクトリーの機能↓
//１.各マネージャーのセット命令を実行
//２.アセットの生成と読み込み命令を実行
//３.オブジェクトの生成命令
//=================================================================

#pragma once

#include "modelmanager.h"
#include "texturemanager.h"
#include "shadermanager.h"

#include "timemanager.h"
#include "cameramanager.h"
#include "lightmanager.h"
#include "skymanager.h"
#include "fieldmanager.h"
#include "fogmanager.h"
#include "actormanager.h"
#include "uimanager.h"

class CFactory
{
protected:
	CModelManager* m_pModelM;
	CTextureManager* m_pTexM;
	CShaderManager* m_pShaderM;

	CTimeManager* m_pTimeM;
	CCameraManager* m_pCamM;
	CLightManager* m_pLightM;
	CSkyManager* m_pSkyM;
	CFieldManager* m_pFieldM;
	CFogManager* m_pFogM;
	CActorManager* m_pActorM;
	CUIManager* m_pUIM;
public:
	CFactory(){}
	~CFactory(){}

	virtual void LoadAssert() = 0;
	virtual void BuildGameObject() = 0;

	virtual void SetManager() = 0;

	void SetObject(CModelManager* m) { m_pModelM = m; }
	void SetObject(CTextureManager* m) { m_pTexM = m; }
	void SetObject(CShaderManager* m) { m_pShaderM = m; }

	void SetObject(CTimeManager* m) { m_pTimeM = m; }
	void SetObject(CCameraManager* m) { m_pCamM = m; }
	void SetObject(CLightManager* m) { m_pLightM = m; }
	void SetObject(CSkyManager* m) { m_pSkyM = m; }
	void SetObject(CFieldManager* m) { m_pFieldM = m; }
	void SetObject(CFogManager* m) { m_pFogM = m; }
	void SetObject(CActorManager* m) { m_pActorM = m; }
	void SetObject(CUIManager* m) { m_pUIM = m; }


};
