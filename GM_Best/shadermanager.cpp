//=================================================================
//制作者　李爾捷
//シェーダ管理クラス（CShaderManager）
//１.各シェーダのパスを使って管理する（二つ一セット）
//２.生成命令が来た時まずマップ内を確認する
//３.存在する	→	アドレスをコピー
//４.存在しない	→	ロードしてアドレスをコピー
//５.リターンアドレス
//=================================================================

#include "main.h"
#include "renderer.h"
#include "shadermanager.h"

void CShaderManager::Init()
{
	
}

void CShaderManager::Uninit()
{
	for (auto s : m_pShaders)
	{
		s.second->Uninit();
		delete s.second;
	}

	m_pShaders.clear();
}

CShader* CShaderManager::SetShader(std::string vs_path, std::string ps_path)
{
	std::pair<std::string, std::string> name = std::make_pair(vs_path, ps_path);
	

	auto iter = m_pShaders.find(name);

	if (iter == m_pShaders.end())
	{
		m_pShaders[name] = new CShader(vs_path.c_str(), ps_path.c_str());
	}

	CShader* s = new CShader();
	s = m_pShaders[name];

	return s;
}