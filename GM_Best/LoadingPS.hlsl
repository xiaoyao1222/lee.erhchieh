//タイムバッファ
struct TIME
{
	float4 frame;
};

cbuffer TimeBuffer : register(b7)
{
	TIME Time;
}

Texture2D		g_Texture_Color	  : register(t0);

SamplerState	g_SamplerState    : register(s0);

#define pi 3.14159265359

//*****************************************************************************
// インプットプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex : TEXCOORD0;
	float2 posL : TEXCOORD1;
};


float Noise2d(float2 x)
{
	float xhash = cos(x.x * 37.0);
	float yhash = cos(x.y * 57.0);
	return frac(415.92653 * (xhash + yhash));
}

//非整数ブラウン運動
float fbm(float2 p)
{
	float3x3 m = float3x3(0.00f, 1.60f, 1.20f, -1.60f, 0.72f, -0.96f, -1.20f, -0.96f, 1.28f);
	float f;
	f = 0.5f * Noise2d(p);
	p = mul(m, p);
	f += 0.25f * Noise2d(p);
	p = mul(m, p);
	f += 0.1666f * Noise2d(p);
	p = mul(m, p);
	f += 0.0834f * Noise2d(p);

	return f;
}

//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float4 blendColor, tex;

	tex = g_Texture_Color.Sample(g_SamplerState, input.tex);

	blendColor = tex * input.diffuse;

	float x = sin((Time.frame.w * 0.5f)* (pi / 180.0f));
	float y = sin((Time.frame.w * 0.5f)* (pi / 180.0f));

	float r = fbm(input.posL + float2(x, y));
	float g = fbm(input.posL + float2(x, -y));
	float b = fbm(input.posL + float2(-x, y));

	blendColor.x = r;
	blendColor.y = g;
	blendColor.z = b;

	return blendColor;
}