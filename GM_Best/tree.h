//=================================================================
//制作者　李爾捷
//木アクタークラス（CTree,CTree02）
//=================================================================

#pragma once

#include "actor.h"

class CModel;
class CShader;

class CTree : public CActor
{
private: 

public:
	CTree(){}
	CTree(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~CTree() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(CShader* s, CModel* m) override;
};

class CTree_02 : public CActor
{
private:

public:
	CTree_02(){}
	CTree_02(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~CTree_02() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(CShader* s, CModel* m) override;
};
