#include "main.h"
#include "noise.h"
#include <cmath>


//==============================================================================
//ハッシュ関連
//==============================================================================

//ハッシュセッティング
void CNoise::SetHash(unsigned int seed)
{
	//乱数ライブラリ初期化.
	srand(seed);

	//ハッシュコード初期化.
	memset(m_hashcode, 0, sizeof(unsigned int) * COUNTOF(m_hashcode));

	//ランダムテーブル生成.
	const int TABLE_NUM = HASH_CODE_MAX;
	unsigned int randomTable[TABLE_NUM] = {};
	for (int i = 0; i < COUNTOF(randomTable); ++i) {
		randomTable[i] = rand() % HASH_CODE_MAX;
	}

	//ハッシュコード生成.
	for (int i = 0; i < COUNTOF(m_hashcode); ++i) {
		m_hashcode[i] = randomTable[i % TABLE_NUM];
	}
}

//ハッシュゲット
unsigned int CNoise::GetHash(int x, int y)
{
	int abs_x = abs(x % HASH_CODE_MAX);
	int abs_y = abs(y % HASH_CODE_MAX);
	return m_hashcode[abs_x + m_hashcode[abs_y]];

	//return modf(sinf(x * 12.9898 + y * 78.233) * 43758.5453123, NULL);
}

//==============================================================================
////補正関連
//==============================================================================

//乱数を0.0f~1.0fに正規化したものを取得する.
float CNoise::GetValue(int x, int y)
{
	return (float)GetHash(x, y); // (float)(HASH_CODE_MAX - 1);
}

//五次補間関数.
float CNoise::Fade(float t)
{
	//Ken Perlin氏(パーリンノイズを作った人)が考えだした補間関数.
	//6x^5 - 15x^4 + 10x^3.
	return (6 * powf(t, 5) - 15 * powf(t, 4) + 10 * powf(t, 3));
}

//線形補間.
float CNoise::Lerp(float a, float b, float t)
{
	return (a + (b - a) * t);
}

//==============================================================================
//バリューノイズ計算
//==============================================================================

//バリューノイズ取得.
float CNoise::ValueNoise(float x, float y)
{
	//整数部と小数部に分ける.
	int xi = (int)floorf(x);
	int yi = (int)floorf(y);
	float xf = x - xi;
	float yf = y - yi;

	//格子点を取り出す.
	float a00 = GetValue(xi, yi);
	float a10 = GetValue(xi + 1, yi);
	float a01 = GetValue(xi, yi + 1);
	float a11 = GetValue(xi + 1, yi + 1);

	//小数部分を使ってそのまま線形補間してしまうと折れ線グラフになってしまうので.
	//線形補間する前に小数部分を五次補間関数で歪めちゃう.
	xf = Fade(xf);
	yf = Fade(yf);

	//位置を基準に，各格子点からの影響を考慮した値を算出する.
	return Lerp(Lerp(a00, a10, xf), Lerp(a01, a11, xf), yf);
}

//オクターブバリューノイズ
float CNoise::OctaveValueNoise(float x, float y)
{
	float a = 1.0f;
	float f = 1.0f;
	float maxValue = 0.0f;
	float totalValue = 0.0f;
	float per = 0.5f;
	for (int i = 0; i < 5; ++i) {
		totalValue += a * ValueNoise(x * f, y * f);
		maxValue += a;
		a *= per;
		f *= 2.0f;
	}
	return totalValue / maxValue;
}

//==============================================================================
//パーリンノイズ計算
//==============================================================================
float CNoise::Grad(unsigned int hash, float a, float b)
{
	unsigned int key = hash % 0x4;
	switch (key)
	{
	case 0x0:   return a;   //a * 1.0f + b * 0.0f.
	case 0x1:   return -a;  //a * -1.0f + b * 0.0f.
	case 0x2:   return -b;  //a * 0.0f + b * -1.0f.
	case 0x3:   return b;   //a * 0.0f + b * 1.0f.
	};
	return 0.0f;
}

//パーリンノイズ取得.
float CNoise::PerlinNoise(float x, float y)
{
	//整数部と小数部に分ける.
	int xi = (int)floorf(x);
	int yi = (int)floorf(y);
	float xf = x - xi;
	float yf = y - yi;

	//格子点からハッシュを取り出し，その値を基に勾配を取得する.
	float a00 = Grad(GetHash(xi, yi), xf, yf);
	float a10 = Grad(GetHash(xi + 1, yi), xf - 1.0f, yf);
	float a01 = Grad(GetHash(xi, yi + 1), xf, yf - 1.0f);
	float a11 = Grad(GetHash(xi + 1, yi + 1), xf - 1.0f, yf - 1.0f);

	//補間をかける.
	xf = Fade(xf);
	yf = Fade(yf);

	//位置に合わせて格子点のどの点から一番影響を受けるかを決める.
	//(勾配関数内で内積を取っているので，ベクトルの向きによっては負の値が出る．範囲は-1.0f~1.0f).
	//(なので，正の値にするために1.0fを足して2.0fで割っている).
	return (Lerp(Lerp(a00, a10, xf), Lerp(a01, a11, xf), yf) + 1.0f) / 2.0f;
}

//オクターブパーリンノイズ
float CNoise::OctavePerlinNoise(float x, float y)
{
	float a = 1.0f;
	float f = 1.0f;
	float maxValue = 0.0f;
	float totalValue = 0.0f;
	float per = 0.5f;
	for (int i = 0; i < 7; ++i) {
		totalValue += a * PerlinNoise(x * f, y * f);
		maxValue += a;
		a *= per;
		f *= 2.0f;
	}
	return totalValue / maxValue;
}

//==============================================================================
//中点変位法計算
//==============================================================================

//テーブル生成.
void CNoise::SetupMidpointDisplaceNoise(unsigned int seed)
{
	//乱数ライブラリ初期化.
	srand(seed);

	//ハッシュコード初期化.
	for (int i = 0; i < TABLE_SIZE; ++i) {
		for (int j = 0; j < TABLE_SIZE; ++j) {
			m_NoiseValue[i][j] = -1;
		}
	}

	//四隅に初期値を設定する.
	m_NoiseValue[0][0] = (rand() % HASH_CODE_MAX);
	m_NoiseValue[TABLE_SIZE - 1][0] = (rand() % HASH_CODE_MAX);
	m_NoiseValue[0][TABLE_SIZE - 1] = (rand() % HASH_CODE_MAX);
	m_NoiseValue[TABLE_SIZE - 1][TABLE_SIZE - 1] = (rand() % HASH_CODE_MAX);

	//残りの点を決定する.
	XMFLOAT2 topLeft; topLeft.x = topLeft.y = 0.0f;
	XMFLOAT2 rightBottom; rightBottom.x = rightBottom.y = (float)(TABLE_SIZE - 1);
	SetupMidpointDisplaceNoise(topLeft, rightBottom, HASH_CODE_MAX);
}

void CNoise::SetupMidpointDisplaceNoise(XMFLOAT2 topLeft, XMFLOAT2 rightBottom, int heightMax)
{
	static const int    SMOOTH_COEF = 4;

	//始点と終点の各成分.
	int nTop = (int)floorf(topLeft.y);
	int nLeft = (int)floorf(topLeft.x);
	int nBottom = (int)floorf(rightBottom.y);
	int nRight = (int)floorf(rightBottom.x);

	//正方形の四点の値.
	const int nTopLeft = m_NoiseValue[nLeft][nTop];
	const int nTopRight = m_NoiseValue[nRight][nTop];
	const int nBottomLeft = m_NoiseValue[nLeft][nBottom];
	const int nBottomRight = m_NoiseValue[nRight][nBottom];

	//中点の位置.
	int nX = (nLeft + nRight) / 2;
	int nY = (nTop + nBottom) / 2;

	if (heightMax <= 1) {
		int value = (nTopLeft + nTopRight + nBottomLeft + nBottomRight) / SMOOTH_COEF;
		value = CLIP(value, 0, HASH_CODE_MAX - 1);
		if (m_NoiseValue[nX][nY] < 0) {
			m_NoiseValue[nX][nY] = value;
		}
	}
	else {
		int value = (nTopLeft + nTopRight + nBottomLeft + nBottomRight) / SMOOTH_COEF;
		value += (rand() % heightMax) - (heightMax / 2);
		value = CLIP(value, 0, HASH_CODE_MAX - 1);
		if (m_NoiseValue[nX][nY] < 0) {
			m_NoiseValue[nX][nY] = value;
		}

		//上下左右の点も値を決める.
		{
			if (m_NoiseValue[nX][nTop] < 0) {
				m_NoiseValue[nX][nTop] = (nTopLeft + nTopRight) / 2;
			}
			if (m_NoiseValue[nX][nBottom] < 0) {
				m_NoiseValue[nX][nBottom] = (nBottomLeft + nBottomRight) / 2;
			}
			if (m_NoiseValue[nLeft][nY] < 0) {
				m_NoiseValue[nLeft][nY] = (nTopLeft + nBottomLeft) / 2;
			}
			if (m_NoiseValue[nRight][nY] < 0) {
				m_NoiseValue[nRight][nY] = (nTopRight + nBottomRight) / 2;
			}
		}

		//分割した正方形に関して，同じように中点の高さを決める.
		{
			XMFLOAT2 midPoint;        midPoint.x = (float)nX;        midPoint.y = (float)nY;
			XMFLOAT2 midUpEdge;       midUpEdge.x = (float)nX;        midUpEdge.y = (float)nTop;
			XMFLOAT2 midDownEdge;     midDownEdge.x = (float)nX;        midDownEdge.y = (float)nBottom;
			XMFLOAT2 midLeftEdge;     midLeftEdge.x = (float)nLeft;     midLeftEdge.y = (float)nY;
			XMFLOAT2 midRightEdge;    midRightEdge.x = (float)nRight;    midRightEdge.y = (float)nY;

			heightMax /= 2;
			SetupMidpointDisplaceNoise(topLeft, midPoint, heightMax);
			SetupMidpointDisplaceNoise(midUpEdge, midRightEdge, heightMax);
			SetupMidpointDisplaceNoise(midLeftEdge, midDownEdge, heightMax);
			SetupMidpointDisplaceNoise(midPoint, rightBottom, heightMax);
		}
	}
}

float CNoise::GetMidpointDisplaceNoise(float x, float y)
{
	int xi = abs((int)floorf(x));
	int yi = abs((int)floorf(y));
	return ((float)m_NoiseValue[xi][yi] / (float)(HASH_CODE_MAX - 1));
}

//==============================================================================
//ノイズコール
//==============================================================================
float CNoise::Call(float x, float y, float rect_size, float range_max)
{

	const float RECT_SIZE = rect_size;
	const int   RANGE_MAX = range_max;

	switch (m_Type)
	{
	case Value:
		return (float)RANGE_MAX * OctaveValueNoise((float)x / RECT_SIZE, (float)y / RECT_SIZE);

	case Perlin:
		return (float)RANGE_MAX * OctavePerlinNoise((float)x / RECT_SIZE, (float)y / RECT_SIZE);

	case MidpointDisplace:
		return (float)RANGE_MAX * GetMidpointDisplaceNoise(x, y);
	}
}