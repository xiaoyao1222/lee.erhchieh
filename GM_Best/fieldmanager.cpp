//=================================================================
//制作者　李爾捷
//フィールド管理クラス（CFieldManager）
//１.フィールド、海、ウェイポイントは順序生成
//２.毎フレームプレイヤー足元のフィールドのイテレーターを確認
//３.イテレーターの周りのフィールド存在を確認→生成
//４.ウェイポイントの取得はランダム、IDと同じじゃないならばいい
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "fieldmanager.h"

#include "modelmanager.h"
#include "texturemanager.h"
#include "shadermanager.h"
#include "actormanager.h"
#include "cameramanager.h"


#include "field.h"
#include "ocean.h"
#include "tree.h"
#include "player.h"
#include "waypoint.h"


//=============================================================================
//フィールド生成
//=============================================================================
void CFieldManager::SpawnField(float x, float z)
{
	auto field = new CField(XMFLOAT3(x, 0.0f, z), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f));
	field->Init(FIELD_WIDTH, FIELD_HEIGHT, this);
	m_pFields.push_back(field);

	if (m_Type != DESERT)
	{
		auto ocean = new COcean(XMFLOAT3(x, 0.0f, z), XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f));
		ocean->Init(FIELD_WIDTH, FIELD_HEIGHT);
		m_pOceans.push_back(ocean);
	}

	for (auto p : m_pPoints)
	{
		p->CheckLinkList(this);
	}
}

//=============================================================================
//周辺フィールドの存在確認
//=============================================================================
void CFieldManager::CheckArea(XMFLOAT3 pos)
{
	std::list <CField*>::iterator spawnField = m_pFields.end();

	for (int y = 0; y < 3; y++)
	{
		for (int x = 0; x < 3; x++)
		{
			XMFLOAT2 spawnpos = XMFLOAT2(pos.x + FIELD_WIDTH * (x - 1) , pos.z + FIELD_HEIGHT * (y - 1));

			spawnField = std::find_if(m_pFields.begin(), m_pFields.end(), [spawnpos](CField* field) {
				return (spawnpos.x == field->GetPos().x &&
					spawnpos.y == field->GetPos().z);
			});
			if (spawnField == m_pFields.end())
			{
				SpawnField(spawnpos.x, spawnpos.y);
			}
		}
	}
}

float CFieldManager::Distance(XMFLOAT3 p1, XMFLOAT3 p2)
{
	float dx = p1.x - p2.x, dz = p1.z - p2.z;
	return dx * dx + dz * dz;
}

void CFieldManager::Init()
{
	TreeDebugDraw = FieldDebugDraw = OceanDebugDraw = false;
	WayPointDebugDraw = true;
}

void CFieldManager::Uninit()
{

	m_pFieldTextures.clear();

	m_pOceanTextures.clear();

	for (auto p : m_pPoints)
	{
		delete p;
	}

	m_pPoints.clear();

	for (auto t : m_pTrees)
	{
		delete t;
	}
	m_pTrees.clear();

	for (auto t : m_pTree_02s)
	{
		delete t;
	}
	m_pTree_02s.clear();

	for (auto o : m_pOceans)
	{
		delete o;
	}
	m_pOceans.clear();

	for (auto f : m_pFields)
	{
		delete f;
	}
	m_pFields.clear();

	if (m_pNoise != nullptr)
	{
		delete m_pNoise;
	}
}

void CFieldManager::Update()
{
	XMFLOAT3 playerPos = m_pActorM->GetPlayer()->GetPos();

	auto findStandingfield = [&playerPos](CField* field) {
		return (field->GetPos().x - FIELD_WIDTH / 2 < playerPos.x &&
			field->GetPos().x + FIELD_WIDTH / 2 >= playerPos.x &&
			field->GetPos().z - FIELD_HEIGHT / 2 < playerPos.z &&
			field->GetPos().z + FIELD_HEIGHT / 2 >= playerPos.z);
	};

	m_pStandingField = std::find_if(m_pFields.begin(), m_pFields.end(), findStandingfield);

	if (m_pStandingField != m_pFields.end())
	{
		CheckArea((*m_pStandingField)->GetPos());
	}

	for (auto f : m_pFields)
	{
		f->Update();
	}

	for (auto o : m_pOceans)
	{
		o->Update();
	}

	for (auto t : m_pTrees)
	{
		t->Update();
	}

	for (auto t : m_pTree_02s)
	{
		t->Update();
	}
}

void CFieldManager::Draw(RenderMode mode)
{
	XMFLOAT3 playerPos = m_pActorM->GetPlayer()->GetPos();

	static float pos[2];

	switch (mode)
	{
	case SHADOW_MODE:

		if (!TreeDebugDraw)
		{
			switch (m_Type)
			{
			case NORMAL:
				for (auto t : m_pTrees)
				{
					if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

					t->Draw(m_pShadowShader, m_pTreeModel);
				}
				break;
			case MOUNTAIN:
				break;

			case DESERT:
				for (auto t : m_pTrees)
				{
					if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

					t->Draw(m_pShadowShader, m_pCactusModel);
				}
				break;
			}


			for (auto t : m_pTree_02s)
			{
				if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

				t->Draw(m_pShadowShader, m_pTreeModel);
			}
		}

		if (!OceanDebugDraw)
		{
			for (auto o : m_pOceans)
			{
				if (Distance(playerPos, o->GetPos()) >= 300 * 300)continue;

				o->Draw(m_pShadowShader);
			}
		}

		if (!FieldDebugDraw)
		{
			for (auto f : m_pFields)
			{
				if (Distance(playerPos, f->GetPos()) >= 300 * 300)continue;

				f->Draw(m_pShadowShader);
			}
		}

		break;

	case NORMAL_MODE:

		if (!TreeDebugDraw)
		{
			switch (m_Type)
			{
			case NORMAL:
				for (auto t : m_pTrees)
				{
					if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

					t->Draw(m_pTreeShader, m_pTreeModel);
				}
				break;
			case MOUNTAIN:
				break;

			case DESERT:
				for (auto t : m_pTrees)
				{
					if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

					t->Draw(m_pTreeShader, m_pCactusModel);
				}
				break;
			}


			for (auto t : m_pTree_02s)
			{
				if (!m_pCamM->GetCamera()->GetVisibility(t->GetPos()))continue;

				t->Draw(m_pTreeShader, m_pTree_02Model);
			}
		}

		if (!OceanDebugDraw)
		{
			for (auto o : m_pOceans)
			{
				if (Distance(playerPos, o->GetPos()) >= 300 * 300)continue;

				o->Draw(m_pOceanShader, m_pOceanTextures);
			}
		}

		if (!FieldDebugDraw)
		{
			for (auto f : m_pFields)
			{
				if (Distance(playerPos, f->GetPos()) >= 300 * 300)continue;

				f->Draw(m_pFieldShader, m_pFieldTextures);
			}
		}

		if (!WayPointDebugDraw)
		{
			CRenderer::SetRenderType(false);
			for (auto p : m_pPoints)
			{
				if (!m_pCamM->GetCamera()->GetVisibility(p->GetPos()))continue;

				p->Draw(m_WayPointDebugShader, m_WayPointDebugModel);
			}
			CRenderer::SetRenderType(true);
		}
		break;

	case IMGUI_MODE:
		if (ImGui::BeginTabItem("field")) {

			ImGui::Text("StandingField = %.2f, %,2f, %.2f", (*m_pStandingField)->GetPos().x, (*m_pStandingField)->GetPos().y, (*m_pStandingField)->GetPos().z);

			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.5f);

			ImGui::InputFloat2("SpawnPos", pos, "%.2f");
			if (ImGui::Button("Spawn")) {
				SpawnField(pos[0], pos[1]);
			}

			ImGui::SameLine();

			if (ImGui::Button("Clear")) {
				for (auto t : m_pTrees)
				{
					delete t;
				}
				m_pTrees.clear();

				for (auto t : m_pTree_02s)
				{
					delete t;
				}
				m_pTree_02s.clear();

				for (auto o : m_pOceans)
				{
					delete o;
				}
				m_pOceans.clear();

				for (auto f : m_pFields)
				{
					delete f;
				}
				m_pFields.clear();
			}
			ImGui::Checkbox("CloseTree", &TreeDebugDraw);

			ImGui::SameLine();

			ImGui::Checkbox("CloseOcean", &OceanDebugDraw);



			ImGui::Checkbox("CloseField", &FieldDebugDraw);

			ImGui::SameLine();

			ImGui::Checkbox("CloseWayPoint", &WayPointDebugDraw);

			if (m_pStandingField != m_pFields.end())
			{
				(*m_pStandingField)->Draw();
			}

			ImGui::EndTabItem();
		}

		break;
	}
}

void CFieldManager::Load(CModelManager* m)
{
	m_pTreeModel = m->SetModel("asset/Tree/Tree.obj");
	m_pTree_02Model = m->SetModel("asset/Palm_01//Palm_01.obj");
	m_pCactusModel = m->SetModel("asset/Cactus/10436_Cactus_v1_max2010_it2.obj");
	m_WayPointDebugModel = m->SetModel("asset/sphere.obj");
}

void CFieldManager::Load(CTextureManager* m)
{
	switch (m_Type)
	{
	case NORMAL:
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_baseColor.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/ground/Ground_Grass_001_COLOR.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_COLOR.png"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_normal.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/ground/Ground_Grass_001_NORM.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_Normal.png"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_height.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/ground/Ground_Grass_001_DISP.PNG"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_Height.png"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_roughness.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/ground/Ground_Grass_001_ROUGH.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_ROUGH.png"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_ambientOcclusion.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/ground/Ground_Grass_001_OCC.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_OCC.png"));

		break;
	case MOUNTAIN:
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/snow/Snow_003_COLOR.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_baseColor.jpg"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/snow/Snow_003_NORM.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_normal.jpg"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/snow/Snow_003_DISP.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_height.png"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/snow/Snow_003_ROUGH.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_roughness.jpg"));

		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/snow/Snow_003_OCC.jpg"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/rock/Rock_033_ambientOcclusion.jpg"));


		break;

	case DESERT:
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_COLOR.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_Normal.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_Height.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_ROUGH.png"));
		m_pFieldTextures.push_back(m->SetTexture("asset/fieldtexture/sand/Sand_004_OCC.png"));

		break;
	}

	m_pOceanTextures.push_back(m->SetTexture("asset/fieldtexture/water/Water_002_COLOR.jpg"));
	m_pOceanTextures.push_back(m->SetTexture("asset/fieldtexture/water/Water_002_NORM.jpg"));
	m_pOceanTextures.push_back(m->SetTexture("asset/fieldtexture/water/Water_002_DISP.png"));
	m_pOceanTextures.push_back(m->SetTexture("asset/fieldtexture/water/Water_002_ROUGH.jpg"));
	m_pOceanTextures.push_back(m->SetTexture("asset/fieldtexture/water/Water_002_OCC.jpg"));
}

void CFieldManager::Load(CShaderManager* m)
{
	switch (m_Type)
	{
	case NORMAL:
		m_pFieldShader = m->SetShader("shader/FieldVS.cso", "shader/FieldPS.cso");
		break;
	case MOUNTAIN:
		m_pFieldShader = m->SetShader("shader/FieldVS.cso", "shader/MountainPS.cso");
		break;

	case DESERT:
		m_pFieldShader = m->SetShader("shader/FieldVS.cso", "shader/DesertPS.cso");
		break;
	}


	m_pOceanShader = m->SetShader("shader/OceanVS.cso", "shader/OceanPS.cso");
	m_pTreeShader = m->SetShader("shader/TreeVS.cso", "shader/TreePS.cso");
	m_WayPointDebugShader = m->SetShader("shader/TreeVS.cso", "shader/WayPointDebugPS.cso");
	m_pShadowShader = m->SetShader("shader/SoftShadowVS.cso", "shader/SoftShadowPS.cso");
}

void CFieldManager::Build(FIELD_TYPE fType, CNoise::NoiseType ntype)
{
	m_Type = fType;

	m_pNoise = new CNoise(ntype);

	SpawnField(0.0f, 0.0f);

	CheckArea(XMFLOAT3(0.0f, 0.0f, 0.0f));
}

float CFieldManager::GetHeight(XMFLOAT3 position)
{
	auto findStandingfield = [&position](CField* field) {
		return (field->GetPos().x - FIELD_WIDTH / 2 < position.x &&
			field->GetPos().x + FIELD_WIDTH / 2 >= position.x &&
			field->GetPos().z - FIELD_HEIGHT / 2 < position.z &&
			field->GetPos().z + FIELD_HEIGHT / 2 >= position.z);
	};

	std::list<CField*>::iterator pStandingField = std::find_if(m_pFields.begin(), m_pFields.end(), findStandingfield);

	if (pStandingField != m_pFields.end())
	{
		return (*pStandingField)->GetHeight(position);
	}
	else
	{
		return position.y - 1.0f;
	}

}

void CFieldManager::SpawnTree(XMFLOAT3 pos, int type)
{
	if (type == 0)
	{
		switch (m_Type)
		{
		case NORMAL:
			m_pTrees.push_back(new CTree(pos, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.75f, 0.75f, 0.75f)));
			break;
		case MOUNTAIN:
			break;

		case DESERT:
			m_pTrees.push_back(new CTree(pos, XMFLOAT3(-1.5f, 0.0f, 0.0f), XMFLOAT3(0.05f, 0.05f, 0.05f)));
			break;
		}
	}
	else if (type == 1)
	{
		m_pTree_02s.push_back(new CTree_02(pos, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.25f, 0.25f, 0.25f)));
	}
}

void CFieldManager::SpawnWayPoint(XMFLOAT3 pos)
{
	int id = m_pPoints.size();
	CWayPoint* p = new CWayPoint(pos, id);

	std::list<CWayPoint*>* points = p->GetLinkPoints();

	for (auto point : m_pPoints)
	{
		if (point->GetDistance_sq(pos) <= POINT_RADIUS)
		{
			points->push_back(point);
			point->GetLinkPoints()->push_back(p);
		}
	}

	m_pPoints.push_back(p);
}

CWayPoint* CFieldManager::GetWayPoint(int id) {
	int cnt = (rand() % m_pPoints.size());

	std::list<CWayPoint*>::iterator startiter = std::find_if(m_pPoints.begin(), m_pPoints.end(), [id](CWayPoint* p) {return p->GetID() == id; });
	std::list<CWayPoint*>::iterator goaliter;
	while (true)
	{
		float d;
		if (cnt != id)
		{
			goaliter = std::find_if(m_pPoints.begin(), m_pPoints.end(), [&cnt](CWayPoint* p) {return p->GetID() == cnt; });

			d = (*goaliter)->GetDistance_sq((*startiter)->GetPos());

			if (d <= 1000.0f && d >=100.0f)
			{
				break;
			}
		}

		cnt = rand() % m_pPoints.size();
	}
	return *goaliter;
}