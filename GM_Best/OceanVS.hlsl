
//*****************************************************************************
// 定数バッファ
//*****************************************************************************

// マトリクスバッファ
cbuffer WorldBuffer : register(b0)
{
	matrix World;
}
cbuffer ViewBuffer : register(b1)
{
	matrix View;
}
cbuffer ProjectionBuffer : register(b2)
{
	matrix Projection;
}

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

//カメラバッファ
struct CAMERA
{
	float4 pos;
	float4 front;
};

cbuffer CameraBuffer : register(b5)
{
	CAMERA Camera;
}

//タイムバッファ
struct TIME
{
	float4 frame;
};

cbuffer TimeBuffer : register(b7)
{
	TIME Time;
}

//*****************************************************************************
// インプット構造体
//*****************************************************************************
struct VertexInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex		: TEXCOORD0;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;
};

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : SV_POSITION;
	float4 posW	: POSITION1;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;
	float3 tangent : TANGENT;
	float3 binormal : BINORMAL;

	float4 cameradir : POSITION2;
	float4 lightViewPos : TEXCOORD1;
};

//=============================================================================
// 頂点シェーダ
//=============================================================================
PixelInputType main(in VertexInputType input)
{
	PixelInputType output;

	//pos
	{
		matrix wvp, lightwvp;
		wvp = mul(World, View);
		wvp = mul(wvp, Projection);

		lightwvp = mul(World, Light.ViewMatrix);
		lightwvp = mul(lightwvp, Light.OrthoMatrix);

		output.pos = mul(input.pos, wvp);
		output.posW = mul(input.pos, World);
		output.lightViewPos = mul(input.pos, lightwvp);
	}

	//UV
	{
		float2 ShiftUV = float2(Time.frame.w * 0.1f % 10, 0.0f);
		output.tex = input.tex + ShiftUV;
	}

	//Normal
	{
		float4 worldNormal, normal;
		normal = float4(input.normal.xyz, 0.0);
		worldNormal = mul(normal, World);
		worldNormal = normalize(worldNormal);
		output.normal = worldNormal;
	}

	//diffuse
	{
		//float light = 0.5 - 0.5 * dot(Light.Direction.xyz, worldNormal.xyz);
		output.diffuse = input.diffuse * Light.Diffuse;
		output.diffuse += input.diffuse * Light.Ambient;
		output.diffuse.a = input.diffuse.a;
	}

	output.tangent = mul(input.tangent, (float3x3)World);
	output.tangent = normalize(output.tangent);

	output.binormal = mul(input.binormal, (float3x3)World);
	output.binormal = normalize(output.binormal);

	// 鏡面反射用のベクトル
	float3 E = Camera.pos.xyz - output.posW;   // 視線ベクトル
	output.cameradir.x = dot(E, output.tangent);
	output.cameradir.y = dot(E, output.binormal);
	output.cameradir.z = dot(E, output.normal);

	return output;

}


