//=================================================================
//制作者　李爾捷
//船の移動ステートクラス（CBoatStateGo）
//基本は回転ステートから移動ルートのパス（アドレス）を取得
//１.パスの先頭ポイントを次のポイント使ってフロントベクトルを作成
//２.次のポイントまでの距離を一定数以下になったら次のステートに遷移
//３.そしてポイントの状態をNONEに戻す
//パス内のポイント数がゼロになったら→サーチステート
//まだゼロではないと				→ターンステート
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "boatstatego.h"

#include "boat.h"
#include "boatstatesearch.h"
#include "boatstateturn.h"
#include "waypoint.h"

CBoatStateGo::CBoatStateGo(CBoat* boat, std::list<CWayPoint*>* p)
	:m_pPoints(*p)
{
	CWayPoint* point = *p->begin();
	XMFLOAT3 pos, front;
	pos = boat->GetPos();

	front.x = point->GetPos().x - pos.x;
	front.y = point->GetPos().y - pos.y;
	front.z = point->GetPos().z - pos.z;

	XMVECTOR vf = XMLoadFloat3(&front);

	vf = XMVector3Normalize(vf);

	XMStoreFloat3(&front, vf);

	boat->SetFront(front);
}

void CBoatStateGo::Update(CBoat* boat)
{
	CWayPoint* point = *m_pPoints.begin();

	XMFLOAT3 pos = boat->GetPos(), front = boat->GetFront();
	float speed = boat->GetSpeed();

	pos.x += front.x * speed;
	pos.y += front.y * speed;
	pos.z += front.z * speed;

	boat->SetPos(pos);

	//距離判断
	float d = point->GetDistance_sq(pos);
	if (d <= 1.0f)
	{
		point->SetStatus(NONE);
		m_pPoints.pop_front();
		//リスト内ポイント確認
		if (m_pPoints.size() <= 0)
		{
			boat->ChangeState(new CBoatStateSearch(boat, point));
		}
		else
		{
			boat->ChangeState(new CBoatStateTurn(boat, &m_pPoints));
		}
	}
}

void CBoatStateGo::Draw()
{
	CWayPoint* point = *m_pPoints.begin();

	ImGui::Text("Go Now");
	ImGui::Text("TargetPointID: %d", point->GetID());
	ImGui::Text("TargetPointPos: %.2f, %.2f, %.2f", point->GetPos().x, point->GetPos().y, point->GetPos().z);

}