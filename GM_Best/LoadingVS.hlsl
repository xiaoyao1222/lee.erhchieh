//*****************************************************************************
// 定数バッファ
//*****************************************************************************

//タイムバッファ
struct TIME
{
	float4 frame;
};

cbuffer TimeBuffer : register(b7)
{
	TIME Time;
}

cbuffer ProjectionBuffer2D : register(b8)
{
	matrix Projection2D;
}

//*****************************************************************************
// インプット構造体
//*****************************************************************************
struct VertexInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex		: TEXCOORD0;

};

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : SV_POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex : TEXCOORD0;
	float2 posL : TEXCOORD1;
};

//=============================================================================
// 頂点シェーダ
//=============================================================================
PixelInputType main(in VertexInputType input)
{
	PixelInputType output;

	output.pos = mul(input.pos, Projection2D);
	output.normal = input.normal;
	output.diffuse = input.diffuse;
	output.tex = input.tex;

	output.posL = input.pos.xy;

	return output;
}

