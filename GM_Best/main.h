#pragma once

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <assert.h>
#include <string>
#include <map>
#include <unordered_map>
#include <vector>
#include <list>
#include <algorithm>
#include <functional>
#include <cpplinq.hpp>
#include <iostream>
#include <thread>
#include <chrono>
#include <mutex>

#include <d3d11.h>
#include <DirectXMath.h>
#include "cereal\cereal.hpp"
#include "cereal\archives\json.hpp"
#include <fstream>

using namespace DirectX;
using namespace cpplinq;

#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "winmm.lib")
#pragma comment (lib, "xaudio2.lib")
#pragma comment(lib, "assimp.lib")



#define SCREEN_WIDTH	(1920)			// ウインドウの幅
#define SCREEN_HEIGHT	(1080)			// ウインドウの高さ

struct Transform
{
	XMFLOAT3 Position;
	XMFLOAT3 Rotation;
	XMFLOAT3 Scale;
};

HWND GetWindow();

struct VECTOR3
{
	float x, y, z;
	template<class Archive>
	void serialize(Archive& archive)
	{
		archive(CEREAL_NVP(x),
				CEREAL_NVP(y),
				CEREAL_NVP(z));
	}
};
