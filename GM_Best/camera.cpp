//=================================================================
//制作者　李爾捷
//カメラアクタークラス（CCamera）
//プレイヤーからトランスフォームを取得
//FPS形式
//=================================================================

#include "main.h"
#include "renderer.h"

#include "camera.h"

#include "cameramanager.h"
#include "actormanager.h"
#include "player.h"

CCamera::CCamera(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale):
	m_Front(XMFLOAT3(0.0f, 0.0f, 1.0f)),m_pCamM(nullptr)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;

	m_Viewport.left = 0;
	m_Viewport.top = 0;
	m_Viewport.right = SCREEN_WIDTH;
	m_Viewport.bottom = SCREEN_HEIGHT;
}

void CCamera::Init(CGameObject* m)
{
	m_pCamM = (CCameraManager*)m;
}


void CCamera::Uninit()
{


}


void CCamera::Update()
{
	Transform playerTransform = m_pCamM->GetActorM()->GetPlayer()->GetTransform();
	XMFLOAT3 front = m_pCamM->GetActorM()->GetPlayer()->GetFrontVector();

	SetTransform(playerTransform);
	SetFrontVector(front);
}



void CCamera::Draw()
{
	XMMATRIX viewMatrix, projectionMatrix;

	// ビューポート設定
	D3D11_VIEWPORT dxViewport;
	dxViewport.Width = (float)(m_Viewport.right - m_Viewport.left);
	dxViewport.Height = (float)(m_Viewport.bottom - m_Viewport.top);
	dxViewport.MinDepth = 0.0f;
	dxViewport.MaxDepth = 1.0f;
	dxViewport.TopLeftX = (float)m_Viewport.left;
	dxViewport.TopLeftY = (float)m_Viewport.top;

	CRenderer::GetDeviceContext()->RSSetViewports(1, &dxViewport);

	// ビューマトリクス設定
	m_InvViewMatrix = XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	m_InvViewMatrix *= XMMatrixTranslation(m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);

	XMVECTOR det;
	viewMatrix = XMMatrixInverse(&det, m_InvViewMatrix);

	// プロジェクションマトリクス設定
	projectionMatrix = XMMatrixPerspectiveFovLH(1.0f, dxViewport.Width / dxViewport.Height, 1.0f, 250.0f);

	XMStoreFloat4x4(&m_ViewMatrix, viewMatrix);
	XMStoreFloat4x4(&m_ProjectionMatrix, projectionMatrix);

	CRenderer::SetViewMatrix(&m_ViewMatrix);
	CRenderer::SetProjectionMatrix(&m_ProjectionMatrix);
	CRenderer::SetCamera(this);
}

//可視範囲確認関数
bool CCamera::GetVisibility(XMFLOAT3 Pos)
{
	XMVECTOR worldPos, viewPos, projPos;
	XMFLOAT3 projPosF;
	worldPos = XMLoadFloat3(&Pos);

	XMMATRIX viewMatrix, projectionMatrix;

	viewMatrix = XMLoadFloat4x4(&m_ViewMatrix);
	projectionMatrix = XMLoadFloat4x4(&m_ProjectionMatrix);

	//座標変換
	viewPos = XMVector3TransformCoord(worldPos, viewMatrix);
	projPos = XMVector3TransformCoord(viewPos, projectionMatrix);

	XMStoreFloat3(&projPosF, projPos);

	if (-1.0f < projPosF.x && projPosF.x < 1.0f&& -1.0f < projPosF.y && projPosF.y < 1.0f &&  -1.0f < projPosF.z && projPosF.z < 1.0f) return true;

	return false;
}