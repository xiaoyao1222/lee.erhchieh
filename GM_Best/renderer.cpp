#include "main.h"
#include "renderer.h"
#include "texture.h"
#include "light.h"
#include "camera.h"
#include <io.h>


D3D_FEATURE_LEVEL       CRenderer::m_FeatureLevel = D3D_FEATURE_LEVEL_11_0;

ID3D11Device*           CRenderer::m_D3DDevice = NULL;
ID3D11DeviceContext*    CRenderer::m_ImmediateContext = NULL;
ID3D11DeviceContext*    CRenderer::m_DeferredContext = NULL;
IDXGISwapChain*         CRenderer::m_SwapChain = NULL;
ID3D11RenderTargetView* CRenderer::m_RenderTargetView = NULL;
ID3D11DepthStencilView* CRenderer::m_DepthStencilView = NULL;

ID3D11Texture2D*			CRenderer::m_DepthRenderTarget = NULL;
ID3D11RenderTargetView*		CRenderer::m_DepthRenderTargetView = NULL;
ID3D11ShaderResourceView*	CRenderer::m_DepthResourceView = NULL;

ID3D11DepthStencilState* CRenderer::m_DepthStateEnable = NULL;
ID3D11DepthStencilState* CRenderer::m_DepthStateDisable = NULL;

ID3D11RasterizerState* CRenderer::m_RasterizerStateSolid = NULL;
ID3D11RasterizerState* CRenderer::m_RasterizerStateWireframe = NULL;

//定数バッファ群
ID3D11Buffer*	CRenderer::m_pWorldBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pViewBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pProjectionBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pLightBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pMaterialBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pCameraBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pFogBuffer = NULL;
ID3D11Buffer*	CRenderer::m_pTimeBuffer = NULL;

ID3D11Buffer*	CRenderer::m_pWorldBuffer2D = NULL;
ID3D11Buffer*	CRenderer::m_pViewBuffer2D = NULL;
ID3D11Buffer*	CRenderer::m_pProjectionBuffer2D = NULL;



void CRenderer::Init()
{
	HRESULT hr = S_OK;

	DXGI_SWAP_CHAIN_DESC sd;
	// デバイス、スワップチェーン、コンテキスト生成
	{
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = SCREEN_WIDTH;
		sd.BufferDesc.Height = SCREEN_HEIGHT;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = GetWindow();
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = TRUE;

		hr = D3D11CreateDeviceAndSwapChain(NULL,
			D3D_DRIVER_TYPE_HARDWARE,
			NULL,
			0,
			NULL,
			0,
			D3D11_SDK_VERSION,
			&sd,
			&m_SwapChain,
			&m_D3DDevice,
			&m_FeatureLevel,
			&m_ImmediateContext);
	}

	// レンダーターゲットビュー生成、設定
	{
		ID3D11Texture2D* pBackBuffer = NULL;
		m_SwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer);
		m_D3DDevice->CreateRenderTargetView(pBackBuffer, NULL, &m_RenderTargetView);
		pBackBuffer->Release();
	}

	//ステンシル用テクスチャー作成
	{
		ID3D11Texture2D* depthTexture = NULL;
		D3D11_TEXTURE2D_DESC td;
		ZeroMemory(&td, sizeof(td));
		td.Width = sd.BufferDesc.Width;
		td.Height = sd.BufferDesc.Height;
		td.MipLevels = 1;
		td.ArraySize = 1;
		td.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		td.SampleDesc = sd.SampleDesc;
		td.Usage = D3D11_USAGE_DEFAULT;
		td.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		td.CPUAccessFlags = 0;
		td.MiscFlags = 0;
		m_D3DDevice->CreateTexture2D(&td, NULL, &depthTexture);


		//ステンシルターゲット作成
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvd;
		ZeroMemory(&dsvd, sizeof(dsvd));
		dsvd.Format = td.Format;
		dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		dsvd.Flags = 0;
		m_D3DDevice->CreateDepthStencilView(depthTexture, &dsvd, &m_DepthStencilView);


		m_ImmediateContext->OMSetRenderTargets(1, &m_RenderTargetView, m_DepthStencilView);
	}

	//Gバッファ作成↓4回
	//{
	//	ID3D11Texture2D* texture = NULL;
	//	D3D11_TEXTURE2D_DESC td;
	//	ZeroMemory(&td, sizeof(td));
	//	td.Width = sd.BufferDesc.Width;											//2の累乗の正方形がいい
	//	td.Height = sd.BufferDesc.Height;
	//	td.MipLevels = 1;
	//	td.ArraySize = 1;
	//	td.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;								//RGBA各32を使う(float)
	//	td.SampleDesc = sd.SampleDesc;
	//	td.Usage = D3D11_USAGE_DEFAULT;
	//	td.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	//	td.CPUAccessFlags = 0;
	//	td.MiscFlags = 0;
	//	m_D3DDevice->CreateTexture2D(&td, NULL, &texture);

	//	D3D11_RENDER_TARGET_VIEW_DESC rtvd;
	//	ZeroMemory(&rtvd, sizeof(rtvd));
	//	rtvd.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//	rtvd.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;

	//	//m_D3DDevice->CreateRenderTargetView(texture, &rtvd, &m_GRenderTargetView[i]);

	//	D3D11_SHADER_RESOURCE_VIEW_DESC SRVDesc = {};
	//	SRVDesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	//	SRVDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
	//	SRVDesc.Texture2D.MipLevels = 1;

	//	//m_D3DDevice->CreateShaderResourceView(texture, &SRVDesc, &m_GRenderTargetView[i]);
	//}

	// ビューポート設定
	{
		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)SCREEN_WIDTH;
		vp.Height = (FLOAT)SCREEN_HEIGHT;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		m_ImmediateContext->RSSetViewports(1, &vp);
	}

	// ラスタライザステート設定
	{
		D3D11_RASTERIZER_DESC rd;
		ZeroMemory(&rd, sizeof(rd));
		rd.FillMode = D3D11_FILL_SOLID;
		rd.CullMode = D3D11_CULL_BACK;
		rd.DepthClipEnable = TRUE;
		rd.MultisampleEnable = FALSE;

		m_D3DDevice->CreateRasterizerState(&rd, &m_RasterizerStateSolid);

		rd.FillMode = D3D11_FILL_WIREFRAME;
		m_D3DDevice->CreateRasterizerState(&rd, &m_RasterizerStateWireframe);

		m_ImmediateContext->RSSetState(m_RasterizerStateSolid);
	}

	// ブレンドステート設定
	{
		D3D11_BLEND_DESC blendDesc;
		ZeroMemory(&blendDesc, sizeof(blendDesc));
		blendDesc.AlphaToCoverageEnable = TRUE;
		blendDesc.IndependentBlendEnable = FALSE;
		blendDesc.RenderTarget[0].BlendEnable = TRUE;
		blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
		blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
		blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
		blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
		blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
		blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

		float blendFactor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
		ID3D11BlendState* blendState = NULL;
		m_D3DDevice->CreateBlendState(&blendDesc, &blendState);
		m_ImmediateContext->OMSetBlendState(blendState, blendFactor, 0xffffffff);
	}

	// 深度ステンシルステート設定
	{
		D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
		ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));
		depthStencilDesc.DepthEnable = TRUE;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
		depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
		depthStencilDesc.StencilEnable = FALSE;

		m_D3DDevice->CreateDepthStencilState(&depthStencilDesc, &m_DepthStateEnable);//深度有効ステート

		depthStencilDesc.DepthEnable = FALSE;
		depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
		m_D3DDevice->CreateDepthStencilState(&depthStencilDesc, &m_DepthStateDisable);//深度無効ステート

		m_ImmediateContext->OMSetDepthStencilState(m_DepthStateEnable, NULL);
		SetDepthEnable(true);
	}

	// サンプラーステート設定
	{

		D3D11_SAMPLER_DESC samplerDesc;
		ZeroMemory(&samplerDesc, sizeof(samplerDesc));
		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0;
		samplerDesc.MaxAnisotropy = 16;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		ID3D11SamplerState* samplerState = NULL;
		hr = m_D3DDevice->CreateSamplerState(&samplerDesc, &samplerState);

		m_ImmediateContext->PSSetSamplers(0, 1, &samplerState);


		//シャドウマップ用サンプラー
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;

		// サンプラーステートを生成.
		hr = m_D3DDevice->CreateSamplerState(&samplerDesc, &samplerState);

		m_ImmediateContext->PSSetSamplers(1, 1, &samplerState);

		//深度比較サンプラー
		// サンプラーステートを作成する
		samplerDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;                           // サンプリング時に使用するフィルタ。
		samplerDesc.MipLODBias = 0;                            // 計算されたミップマップ レベルからのバイアス
		samplerDesc.MaxAnisotropy = 1;                         // サンプリングに異方性補間を使用している場合の限界値。有効な値は 1 〜 16 。
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_LESS;           // 比較オプション。
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;             // アクセス可能なミップマップの上限値
		hr = m_D3DDevice->CreateSamplerState(&samplerDesc, &samplerState);

		m_ImmediateContext->PSSetSamplers(2, 1, &samplerState);
	}

	//定数バッファ設定
	{
		D3D11_BUFFER_DESC hBufferDesc;
		hBufferDesc.Usage = D3D11_USAGE_DEFAULT;
		hBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		hBufferDesc.CPUAccessFlags = 0;
		hBufferDesc.MiscFlags = 0;
		hBufferDesc.StructureByteStride = sizeof(float);

		hBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);

		//world
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pWorldBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(0, 1, &m_pWorldBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(0, 1, &m_pWorldBuffer);

		//view
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pViewBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(1, 1, &m_pViewBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(1, 1, &m_pViewBuffer);

		//projection
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pProjectionBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(2, 1, &m_pProjectionBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(2, 1, &m_pProjectionBuffer);

		//light
		hBufferDesc.ByteWidth = sizeof(LIGHT);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pLightBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(3, 1, &m_pLightBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(3, 1, &m_pLightBuffer);

		//material
		hBufferDesc.ByteWidth = sizeof(MATERIAL);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pMaterialBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(4, 1, &m_pMaterialBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(4, 1, &m_pMaterialBuffer);

		//camera
		hBufferDesc.ByteWidth = sizeof(CAMERA);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pCameraBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(5, 1, &m_pCameraBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(5, 1, &m_pCameraBuffer);

		//fog
		hBufferDesc.ByteWidth = sizeof(FOG);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pFogBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(6, 1, &m_pFogBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(6, 1, &m_pFogBuffer);

		//time
		hBufferDesc.ByteWidth = sizeof(TIME);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pTimeBuffer);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(7, 1, &m_pTimeBuffer);
		CRenderer::GetDeviceContext()->PSSetConstantBuffers(7, 1, &m_pTimeBuffer);

		//projection2D
		hBufferDesc.ByteWidth = sizeof(XMFLOAT4X4);
		CRenderer::GetDevice()->CreateBuffer(&hBufferDesc, NULL, &m_pProjectionBuffer2D);
		CRenderer::GetDeviceContext()->VSSetConstantBuffers(8, 1, &m_pProjectionBuffer2D);
		XMMATRIX projection_matrix;
		XMFLOAT4X4 projection;
		projection_matrix = XMMatrixOrthographicOffCenterLH(0.0f, SCREEN_WIDTH, SCREEN_HEIGHT, 0.0f, 0.0f, 1.0f);
		XMStoreFloat4x4(&projection, projection_matrix);
		projection = Transpose(&projection);
		m_ImmediateContext->UpdateSubresource(m_pProjectionBuffer2D, 0, NULL, &projection, 0, 0);
	}

	CreateDepthTexture();

}

void CRenderer::Uninit()
{
	if (m_pTimeBuffer)	m_pTimeBuffer->Release();
	if (m_pFogBuffer)	m_pFogBuffer->Release();
	if (m_pCameraBuffer)	m_pCameraBuffer->Release();
	if (m_pMaterialBuffer)	m_pMaterialBuffer->Release();
	if (m_pLightBuffer)	m_pLightBuffer->Release();
	if (m_pProjectionBuffer)	m_pProjectionBuffer->Release();
	if (m_pViewBuffer)	m_pViewBuffer->Release();
	if (m_pWorldBuffer)	m_pWorldBuffer->Release();

	if (m_DepthResourceView)	m_DepthResourceView->Release();
	if (m_DepthRenderTargetView)m_DepthRenderTargetView->Release();
	if (m_DepthRenderTarget)	m_DepthRenderTarget->Release();

	if (m_RasterizerStateWireframe)	m_RasterizerStateWireframe->Release();
	if (m_RasterizerStateSolid)		m_RasterizerStateSolid->Release();

	if (m_DepthStateDisable)	m_DepthStateDisable->Release();
	if (m_DepthStateEnable)		m_DepthStateEnable->Release();

	// オブジェクト解放

	if( m_ImmediateContext )	m_ImmediateContext->ClearState();
	if( m_RenderTargetView )	m_RenderTargetView->Release();
	if( m_SwapChain )			m_SwapChain->Release();
	if (m_DeferredContext)		m_DeferredContext->Release();
	if( m_ImmediateContext )	m_ImmediateContext->Release();
	if( m_D3DDevice )			m_D3DDevice->Release();

}

void CRenderer::Begin()
{
	// バックバッファクリア
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };
	m_ImmediateContext->ClearRenderTargetView( m_RenderTargetView, ClearColor );
	m_ImmediateContext->ClearDepthStencilView( m_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

}

void CRenderer::End()
{
	m_SwapChain->Present( 1, 0 );
}

void CRenderer::CreateDepthTexture()
{
	D3D11_TEXTURE2D_DESC renderTextureDesc;

	ZeroMemory(&renderTextureDesc, sizeof(renderTextureDesc));

	renderTextureDesc.Width = SCREEN_WIDTH;
	renderTextureDesc.Height = SCREEN_HEIGHT;
	renderTextureDesc.MipLevels = 1;
	renderTextureDesc.ArraySize = 1;
	renderTextureDesc.Format = DXGI_FORMAT_R32_FLOAT;
	renderTextureDesc.SampleDesc.Count = 1;
	renderTextureDesc.SampleDesc.Quality = 0;
	renderTextureDesc.Usage = D3D11_USAGE_DEFAULT;
	renderTextureDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET;
	renderTextureDesc.CPUAccessFlags = 0;

	//Z値用のテクスチャを生成
	auto hr = m_D3DDevice->CreateTexture2D(&renderTextureDesc, NULL, &m_DepthRenderTarget);

	if (FAILED(hr))
	{
		assert(false);
	}

	//Z値を書き込む必要なターゲットビューを生成
	hr = m_D3DDevice->CreateRenderTargetView(m_DepthRenderTarget, NULL, &m_DepthRenderTargetView);

	if (FAILED(hr))
	{
		assert(false);
	}

	//Z値を参照する必要なリソースビューを生成
	hr = m_D3DDevice->CreateShaderResourceView(m_DepthRenderTarget, NULL, &m_DepthResourceView);

	if (FAILED(hr))
	{
		assert(false);
	}
}

void CRenderer::SetRenderTarget(RenderMode mode)
{
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 1.0f };

	switch (mode)
	{
	case SHADOW_MODE:
		m_ImmediateContext->OMSetRenderTargets(1, &m_DepthRenderTargetView, m_DepthStencilView);

		// バックバッファクリア
		m_ImmediateContext->ClearRenderTargetView(m_DepthRenderTargetView, ClearColor);

		m_ImmediateContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
		break;

	case NORMAL_MODE:
		m_ImmediateContext->OMSetRenderTargets(1, &m_RenderTargetView, m_DepthStencilView);

		Begin();
		break;
	}



}

void CRenderer::SetDepthEnable( bool Enable )
{
	if( Enable )
		m_ImmediateContext->OMSetDepthStencilState( m_DepthStateEnable, NULL );
	else
		m_ImmediateContext->OMSetDepthStencilState( m_DepthStateDisable, NULL );

}

void CRenderer::SetRenderType(bool Enable)
{
	if (Enable)
		m_ImmediateContext->RSSetState(m_RasterizerStateSolid);
	else
		m_ImmediateContext->RSSetState(m_RasterizerStateWireframe);
}


void CRenderer::SetVertexBuffers( ID3D11Buffer* VertexBuffer )
{

	UINT stride = sizeof( VERTEX_3D );
	UINT offset = 0;
	ID3D11Buffer* vb[1] = { VertexBuffer };
	m_ImmediateContext->IASetVertexBuffers( 0, 1, vb, &stride, &offset );

}

void CRenderer::SetIndexBuffer( ID3D11Buffer* IndexBuffer )
{

	m_ImmediateContext->IASetIndexBuffer( IndexBuffer, DXGI_FORMAT_R16_UINT, 0 );

}

void CRenderer::SetTexture( CTexture* Texture, int index)
{

	ID3D11ShaderResourceView* srv[1] = { Texture->GetShaderResourceView() };
	m_ImmediateContext->PSSetShaderResources(index, 1, srv );

}

void CRenderer::SetDepthTexture(int slot)
{
	ID3D11ShaderResourceView* srv[1] = { m_DepthResourceView };
	m_ImmediateContext->PSSetShaderResources(slot, 1, srv);
}

void CRenderer::DrawIndexed( unsigned int IndexCount, unsigned int StartIndexLocation, int BaseVertexLocation )
{

	m_ImmediateContext->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
	m_ImmediateContext->DrawIndexed( IndexCount, StartIndexLocation, BaseVertexLocation );

}

void CRenderer::DrawIndexedInstanced(unsigned int IndexCount, int InstancedCount, unsigned int StartIndexLocation, int BaseVertexLocation)
{
	m_ImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_ImmediateContext->DrawIndexedInstanced(IndexCount, InstancedCount, StartIndexLocation, BaseVertexLocation, 0);
}

//定数バッファ群更新
void CRenderer::SetWorldMatrix(XMFLOAT4X4* m)
{
	XMFLOAT4X4 world = Transpose(m);
	m_ImmediateContext->UpdateSubresource(m_pWorldBuffer, 0, NULL, &world, 0, 0);

}

void CRenderer::SetViewMatrix(XMFLOAT4X4* m)
{
	XMFLOAT4X4 view = Transpose(m);
	m_ImmediateContext->UpdateSubresource(m_pViewBuffer, 0, NULL, &view, 0, 0);
}

void CRenderer::SetProjectionMatrix(XMFLOAT4X4* m)
{
	XMFLOAT4X4 projection = Transpose(m);
	m_ImmediateContext->UpdateSubresource(m_pProjectionBuffer, 0, NULL, &projection, 0, 0);

}

void CRenderer::SetLight(CLight* l)
{
	LIGHT light;

	light.Direction = l->GetDirection();
	light.Ambient = l->GetAmbient();
	light.Diffuse = l->GetDiffuse();

	light.Position = l->GetPosition();
	light.LookAt = l->GetLookAt();
	light.Near_Far = XMFLOAT4(l->GetNear(), l->GetFar(), 0.0f, 0.0f);

	light.ViewMatrix = Transpose(&l->GetViewMatrix());
	light.OrthoMatrix = Transpose(&l->GetOrthoMatrix());
	light.ProjectionMatrix = Transpose(&l->GetProjectionMatrix());

	m_ImmediateContext->UpdateSubresource(m_pLightBuffer, 0, NULL, &light, 0, 0);
}

void CRenderer::SetMaterial(MATERIAL m)
{
	MATERIAL material = m;

	m_ImmediateContext->UpdateSubresource(m_pMaterialBuffer, 0, NULL, &material, 0, 0);

}

void CRenderer::SetCamera(CCamera* c)
{
	CAMERA camera;
	camera.pos = XMFLOAT4(c->GetPos().x, c->GetPos().y, c->GetPos().z, 0.0f);
	camera.front = XMFLOAT4(c->GetFrontVector().x, c->GetFrontVector().y, c->GetFrontVector().z, 0.0f);

	m_ImmediateContext->UpdateSubresource(m_pCameraBuffer, 0, NULL, &camera, 0, 0);

}

void CRenderer::SetFog(FOG f)
{
	FOG fog = f;
	m_ImmediateContext->UpdateSubresource(m_pFogBuffer, 0, NULL, &fog, 0, 0);

}

void CRenderer::SetTime(TIME t)
{
	TIME time = t;
	m_ImmediateContext->UpdateSubresource(m_pTimeBuffer, 0, NULL, &time, 0, 0);

}
