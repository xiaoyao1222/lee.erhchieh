//=================================================================
//ง์า@ขท
//V[เิวNXiCTimeManagerj
//t[ฬ๐ชS
//	จ	HR
//	จ	MIN
//	จ	SEC
//	จ	FRAME
//=================================================================

#include "main.h"
#include "timemanager.h"

void CTimeManager::Init()
{
	m_Time.frame = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
}

void CTimeManager::Uninit()
{

}

void CTimeManager::Update()
{
	float hr, min, sec, flame;

	flame = m_Time.frame.w;
	sec = m_Time.frame.z;
	min = m_Time.frame.y;
	hr = m_Time.frame.x;

	flame += (m_Speed / 10.0f);

	if (!m_bFreeze)
	{
		sec += m_Speed;
	}

	min += (int)sec / 60;
	sec = (int)sec % 60;

	hr += (int)min / 60;
	min = (int)min % 60;

	if (flame >= MAX_TIME)
	{
		flame = 0.0f;
	}

	if (hr >= 12.0f)
	{
		hr = 0.0f;
	}

	SetTime(hr, min, sec, flame);

}

void CTimeManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;
	case NORMAL_MODE:
		CRenderer::SetTime(m_Time);

		break;
	case IMGUI_MODE:

		break;
	}
}