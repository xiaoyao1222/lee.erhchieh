//=================================================================
//制作者　李爾捷
//ライトクラス（CLight）
//影を生成するためライトからビューとプロジェクションを生成
//=================================================================

#include "main.h"
#include "renderer.h"
#include "light.h"

void CLight::Init(float depth_near, float depth_far)
{
	m_Direction = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_Diffuse = COLOR(0.0f, 0.0f, 0.0f, 0.0f);
	m_Ambient = COLOR(0.0f, 0.0f, 0.0f, 0.0f);

	m_Position = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	m_LookAt = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);

	m_Near = depth_near;
	m_Far = depth_far;

	m_Radius = 0.0f;
}

void CLight::GenerateViewMatrix(void)
{
	XMVECTOR pos = XMLoadFloat4(&m_Position);
	XMVECTOR focus = XMLoadFloat4(&m_LookAt);
	XMVECTOR up;
	XMVECTOR right = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);

	up = XMVector3Cross(focus, right);

	XMMATRIX viewMatrix;

	viewMatrix = XMMatrixLookAtLH(pos, focus, up);

	XMStoreFloat4x4(&m_ViewMatrix, viewMatrix);

	CRenderer::SetViewMatrix(&m_ViewMatrix);

	return;
}

void CLight::GenerateOrthoMatrix(float width)
{
	XMMATRIX orthoMatrix;

	orthoMatrix = XMMatrixOrthographicLH(width, width, m_Near, m_Far);

	XMStoreFloat4x4(&m_OrthoMatrix, orthoMatrix);

	CRenderer::SetProjectionMatrix(&m_OrthoMatrix);

	return;
}

void CLight::GenerateProjectionMatrix()
{
	float fieldOfView, screenAspect;

	XMMATRIX ProjectionMatrix;

	// Setup field of view and screen aspect for a square light source.
	fieldOfView = (float)XM_PIDIV2 / 2.0f;
	screenAspect = 1.0f;

	// Create the projection matrix for the light.

	ProjectionMatrix = XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, m_Near, m_Far);

	XMStoreFloat4x4(&m_ProjectionMatrix, ProjectionMatrix);

	//CRenderer::SetProjectionMatrix(&m_ProjectionMatrix);

	return;
}