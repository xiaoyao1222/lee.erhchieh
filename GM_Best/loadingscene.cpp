#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "loadingscene.h"

#include "loadingword.h"

void CLoadingScene::Init()
{
	m_pLoadingWord = new CLoadingWord();
	m_pLoadingWord->Init(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 320.0f, 160.0f);
}

void CLoadingScene::Uninit()
{
	if (m_pLoadingWord != nullptr)
	{
		delete m_pLoadingWord;
	}
}

void CLoadingScene::Update()
{
	m_pLoadingWord->Update();
}

void CLoadingScene::Draw()
{
	m_pLoadingWord->Draw();
}
