#pragma once

#include "polygon.h"

class CTitleWord : public CPolygon
{
private:
	//頂点バッファ
	ID3D11Buffer* m_VertexBuffer = NULL;

public:
	CTitleWord() {}
	CTitleWord(float dx, float dy, float width, float height);
	~CTitleWord() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;

	void Draw(CShader* s, std::list<CTexture*> t) override;
};
