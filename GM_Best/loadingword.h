#pragma once

class CTexture;
class CShader;

class CLoadingWord
{
private:
	//頂点バッファ
	ID3D11Buffer* m_VertexBuffer = NULL;

	//テクスチャ
	CTexture* m_Texture = NULL;
	CShader* m_Shader = NULL;

	float m_Flame;

public:
	CLoadingWord() {}
	~CLoadingWord() { Uninit(); }

	void Init(float dx, float dy, float width, float height);
	void Uninit();
	void Update();
	void Draw();
};

