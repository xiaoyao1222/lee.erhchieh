//=================================================================
//制作者　李爾捷
//空管理クラス（CSkyManager）
//=================================================================

#include "main.h"
#include "renderer.h"

#include "skymanager.h"
#include "shadermanager.h"
#include "timemanager.h"
#include "skysphere.h"
#include "cloud.h"

void CSkyManager::Init()
{
	m_pCloud = new CCloud(XMFLOAT2(0.0f, 0.0f), SCREEN_WIDTH, SCREEN_HEIGHT);
	m_pCloud->Init();

	m_pSkyDome = new CSkySphere(200.0f, XMFLOAT2(1.0f, 1.0f));
	m_pSkyDome->Init(this);
}

void CSkyManager::Uninit()
{
	if (m_pCloud != nullptr)
	{
		delete m_pCloud;
	}

	if (m_pSkyDome != nullptr)
	{
		delete m_pSkyDome;
	}

}

void CSkyManager::Update()
{
	m_pSkyDome->Update();

	m_pCloud->Update();
}

void CSkyManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;

	case NORMAL_MODE:

		m_pSkyDome->Draw(m_pSkyDomeShader);
		m_pCloud->Draw(m_pCloudShader);
		break;
	case IMGUI_MODE:

		m_pCloud->Draw();
		break;
	}
}

void CSkyManager::Load(CShaderManager* m)
{
	m_pCloudShader = m->SetShader("shader/CloudVS.cso", "shader/CloudPS.cso");

	m_pSkyDomeShader = m->SetShader("shader/vertexShader.cso", "shader/pixelShader.cso");
}