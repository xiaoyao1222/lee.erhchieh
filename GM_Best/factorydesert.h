//=================================================================
//制作者　李爾捷
//砂漠ファクトリークラス（CFactoryDesert）
//=================================================================

#pragma once

#include "factory.h"

class CFactoryDesert : public CFactory
{
public:
	CFactoryDesert();
	~CFactoryDesert() {}

	void LoadAssert() override;
	void BuildGameObject() override;

	void SetManager() override;
};

