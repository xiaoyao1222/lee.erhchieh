//=================================================================
//制作者　李爾捷
//山ファクトリークラス（CFactoryMountain）
//フィールド生成時バリューノイズを使用
//=================================================================

#include "main.h"
#include "renderer.h"

#include "factorymountain.h"

CFactoryMountain::CFactoryMountain()
{
	m_pModelM = nullptr;
	m_pTexM = nullptr;
	m_pShaderM = nullptr;

	m_pTimeM = nullptr;
	m_pCamM = nullptr;
	m_pLightM = nullptr;
	m_pSkyM = nullptr;
	m_pFieldM = nullptr;
	m_pFogM = nullptr;
	m_pActorM = nullptr;
	m_pUIM = nullptr;
}

void CFactoryMountain::LoadAssert()
{
	if (m_pModelM != nullptr)
	{
		m_pFieldM->Load(m_pModelM);
		m_pActorM->Load(m_pModelM);
	}

	if (m_pTexM != nullptr)
	{
		m_pFieldM->Load(m_pTexM);
		m_pActorM->Load(m_pTexM);
		m_pUIM->Load(m_pTexM);
	}

	if (m_pShaderM != nullptr)
	{
		m_pSkyM->Load(m_pShaderM);
		m_pFieldM->Load(m_pShaderM);
		m_pActorM->Load(m_pShaderM);
		m_pUIM->Load(m_pShaderM);
	}
}

void CFactoryMountain::BuildGameObject()
{
	if (m_pFieldM != nullptr)
	{
		m_pFieldM->Build(CFieldManager::MOUNTAIN, CNoise::Value);
	}

	if (m_pActorM != nullptr)
	{
		m_pActorM->Build(CActorManager::MOUNTAIN, XMFLOAT3(0.0f, 50.0f, 0.0f));
	}
}

void CFactoryMountain::SetManager()
{
	if (m_pTimeM != nullptr)
	{
		m_pLightM->Load(m_pTimeM);
		m_pFogM->Load(m_pTimeM);
	}

	if (m_pCamM != nullptr)
	{
		m_pFieldM->Load(m_pCamM);
		m_pActorM->Load(m_pCamM);
	}

	if (m_pFieldM != nullptr)
	{
		m_pActorM->Load(m_pFieldM);
	}

	if (m_pActorM != nullptr)
	{
		m_pCamM->Load(m_pActorM);
		m_pLightM->Load(m_pActorM);
		m_pSkyM->Load(m_pActorM);
		m_pFieldM->Load(m_pActorM);
	}
}