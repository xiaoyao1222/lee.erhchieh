//=================================================================
//制作者　李爾捷
//船のルート検索ステートクラス（CBoatStateSearch）
//=================================================================

#pragma once

#include "boatstate.h"

class CBoat;
class CWayPoint;

class CBoatStateSearch : public CBoatState
{
private:
	CBoatStateSearch();

	CWayPoint* m_pStartPoint;
	CWayPoint* m_pGoalPoint;
public:
	CBoatStateSearch(CBoat* boat, CWayPoint* startp);
	~CBoatStateSearch() { }

	void Update(CBoat* boat) override;
	void Draw() override;
};
