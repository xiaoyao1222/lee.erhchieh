#pragma once

class CGameObject;
class CShader;

class CPolygon
{
protected:
	XMFLOAT2 m_Pos;
	float m_Width;
	float m_Height;

public:
	virtual ~CPolygon(){ Uninit(); }

	virtual void Init(){}
	virtual void Init(CGameObject* m) {}
	virtual void Uninit(){}
	virtual void Update(){}

	virtual void Draw() {}
	virtual void Draw(CShader* s) {}
	virtual void Draw(CShader* s, std::list<CTexture*> t) {}
};
