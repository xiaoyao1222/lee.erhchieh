//*****************************************************************************
// 定数バッファ
//*****************************************************************************

// マトリクスバッファ
cbuffer WorldBuffer : register(b0)
{
	matrix World;
}
cbuffer ViewBuffer : register(b1)
{
	matrix View;
}
cbuffer ProjectionBuffer : register(b2)
{
	matrix Projection;
}

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

// 2Dマトリクスバッファ
cbuffer ProjectionBuffer2D : register(b8)
{
	matrix Projection2D;
}

//*****************************************************************************
// インプット構造体
//*****************************************************************************
struct VertexInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex		: TEXCOORD0;
};

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : SV_POSITION;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;
};



//=============================================================================
// 頂点シェーダ
//=============================================================================
PixelInputType main( in VertexInputType input)
{
	PixelInputType output;

	matrix wvp;
	wvp = mul(World, View);
	wvp = mul(wvp, Projection);

	output.pos = mul( input.pos, wvp);
	output.normal = input.normal;
	output.tex = input.tex;
	
	float4 worldNormal, normal;
	normal = float4(input.normal.xyz, 0.0);
	worldNormal = mul(normal, World);
	worldNormal = normalize(worldNormal);

	float light = 0.5 - 0.5 * dot(Light.Direction.xyz, worldNormal.xyz);

	output.diffuse = input.diffuse * light * Light.Diffuse;
	output.diffuse += input.diffuse * Light.Ambient;
	output.diffuse.a = input.diffuse.a;

	return output;

}


