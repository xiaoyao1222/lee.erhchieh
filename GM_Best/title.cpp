#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "title.h"

#include "texturemanager.h"
#include "shadermanager.h"

#include "titleuimanager.h"

#include "input.h"
#include "manager.h"
#include "island.h"
#include "desert.h"
#include "mountain.h"
#include "dungeon.h"

void CTitle::Init()
{

	m_pTexM = AddGameObject<CTextureManager>(Layer_System);
	m_pShaderM = AddGameObject<CShaderManager>(Layer_System);

	m_pTitleUIM = AddGameObject<CTitleUIManager>(Layer_UI);

	m_pTitleUIM->Load(m_pTexM);
	m_pTitleUIM->Load(m_pShaderM);

	CManager::SetLockFlag(true);
}

void CTitle::Uninit()
{
	CScene::Uninit();
}

void CTitle::Update()
{
	CScene::Update();

	if (CInput::GetKeyTrigger('P'))
	{
		std::thread T1(CManager::SetScene<CIsland>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('O'))
	{
		std::thread T1(CManager::SetScene<CDesert>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('I'))
	{
		std::thread T1(CManager::SetScene<CMountain>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('U'))
	{
		std::thread T1(CManager::SetScene<CDungeon>);

		T1.detach();
		return;
	}
}


void CTitle::Draw()
{
	ImGui::SetNextWindowSize(ImVec2(200, 150), ImGuiCond_Always);
	ImGui::Begin("Lobby");

	ImGui::Text("Here is Lobby");
	ImGui::Text("Press P to Island");
	ImGui::Text("Press O to Desert");
	ImGui::Text("Press I to Mountain");
	ImGui::Text("Press U to Dungeon");
	ImGui::Text("Press ESC to Finish ");

	ImGui::End();

	CScene::Draw();
}

