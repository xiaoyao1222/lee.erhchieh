//=================================================================
//制作者　李爾捷
//モデル管理クラス（CModelManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "model.h"

class CModelManager : public CGameObject
{
private:
	std::unordered_map<std::string, CModel*> m_pModels;

	;
public:
	CModelManager()
	{
		m_pModels.clear();
	}
	~CModelManager() { Uninit(); }

	void Init();
	void Uninit();

	CModel* SetModel(std::string model_path);

};

