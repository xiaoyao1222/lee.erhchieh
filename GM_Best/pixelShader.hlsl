//*****************************************************************************
// グローバル変数
//*****************************************************************************
Texture2D		g_Texture_Color		 : register( t0 );
SamplerState	g_SamplerState		 : register( s0 );

//=============================================================================
// ピクセルシェーダ
//=============================================================================
void main( in  float4 inPosition		: POSITION0,
						 in  float4 inNormal		: NORMAL0,
						 in  float2 inTexCoord		: TEXCOORD0,
						 in  float4 inDiffuse		: COLOR0,

						 out float4 outDiffuse		: SV_Target )
{
	float4 color1;

	color1 = g_Texture_Color.Sample( g_SamplerState, inTexCoord );

	outDiffuse = color1 * inDiffuse;

	outDiffuse = float4(1.0f, 1.0f, 1.0f, 1.0f);

}
