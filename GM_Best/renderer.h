#pragma once

class CLight;
class CCamera;

// 頂点構造体
struct VERTEX_3D
{
    XMFLOAT3 Position;
    XMFLOAT3 Normal;
    XMFLOAT4 Diffuse;
    XMFLOAT2 TexCoord;
};

// 頂点構造体
struct VERTEX_FIELD_3D
{
	XMFLOAT3 Position;
	XMFLOAT3 Normal;
	XMFLOAT4 Diffuse;
	XMFLOAT2 TexCoord;
	XMFLOAT3 Tangent;
	XMFLOAT3 Binormal;
};

// 色構造体
struct COLOR
{
	COLOR(){}
	COLOR( float _r, float _g, float _b, float _a )
	{
		r = _r;
		g = _g;
		b = _b;
		a = _a;
	}

	float r;
	float g;
	float b;
	float a;
};

struct LIGHT
{
	XMFLOAT4	Direction;
	COLOR		Diffuse;
	COLOR		Ambient;

	XMFLOAT4	Position;
	XMFLOAT4	LookAt;
	XMFLOAT4	Near_Far;

	XMFLOAT4X4	ViewMatrix;
	XMFLOAT4X4	OrthoMatrix;
	XMFLOAT4X4	ProjectionMatrix;
};

// マテリアル構造体
struct MATERIAL
{
	COLOR		Ambient;
	COLOR		Diffuse;
	COLOR		Specular;
	COLOR		Emission;
	float		Shininess;
	float		Dummy[3];//16bit境界用
};

// マテリアル構造体
struct DX11_MODEL_MATERIAL
{
	MATERIAL		Material;
	class CTexture*	Texture;
};

// 描画サブセット構造体
struct DX11_SUBSET
{
	unsigned short	StartIndex;
	unsigned short	IndexNum;
	DX11_MODEL_MATERIAL	Material;
};

struct CAMERA
{
	XMFLOAT4 pos;
	XMFLOAT4 front;
};

struct FOG
{
	COLOR		Color;

	//x->fogStart y->fogEnd z->near w->far
	XMFLOAT4	DistanceFog;

	//x->density y->densityAttenuation
	XMFLOAT4	HeightFog;
};

struct TIME
{
	XMFLOAT4 frame;
};

class CVertexBuffer;
class CIndexBuffer;
class CTexture;

enum RenderMode
{
	SHADOW_MODE = 0,
	NORMAL_MODE,
	IMGUI_MODE
};

class CRenderer
{
private:
	static D3D_FEATURE_LEVEL       m_FeatureLevel;

	static ID3D11Device*           m_D3DDevice;
	static ID3D11DeviceContext*    m_ImmediateContext;
	static ID3D11DeviceContext*	   m_DeferredContext;
	static IDXGISwapChain*         m_SwapChain;
	static ID3D11RenderTargetView* m_RenderTargetView;
	static ID3D11DepthStencilView* m_DepthStencilView;

	static ID3D11DepthStencilState* m_DepthStateEnable;
	static ID3D11DepthStencilState* m_DepthStateDisable;

	static ID3D11RasterizerState* m_RasterizerStateSolid;
	static ID3D11RasterizerState* m_RasterizerStateWireframe;

	static ID3D11Texture2D*				m_DepthRenderTarget;
	static ID3D11RenderTargetView*		m_DepthRenderTargetView;
	static ID3D11ShaderResourceView*	m_DepthResourceView;

	//定数バッファ群
	static ID3D11Buffer*	m_pWorldBuffer;
	static ID3D11Buffer*	m_pViewBuffer;
	static ID3D11Buffer*	m_pProjectionBuffer;
	static ID3D11Buffer*	m_pLightBuffer;
	static ID3D11Buffer*	m_pMaterialBuffer;
	static ID3D11Buffer*	m_pCameraBuffer;
	static ID3D11Buffer*	m_pFogBuffer;
	static ID3D11Buffer*	m_pTimeBuffer;

	static ID3D11Buffer*	m_pWorldBuffer2D;
	static ID3D11Buffer*	m_pViewBuffer2D;
	static ID3D11Buffer*	m_pProjectionBuffer2D;

	static XMFLOAT4X4 Transpose(XMFLOAT4X4* Matrix)
	{
		XMFLOAT4X4 outMatrix;

		outMatrix._11 = Matrix->_11;
		outMatrix._12 = Matrix->_21;
		outMatrix._13 = Matrix->_31;
		outMatrix._14 = Matrix->_41;

		outMatrix._21 = Matrix->_12;
		outMatrix._22 = Matrix->_22;
		outMatrix._23 = Matrix->_32;
		outMatrix._24 = Matrix->_42;

		outMatrix._31 = Matrix->_13;
		outMatrix._32 = Matrix->_23;
		outMatrix._33 = Matrix->_33;
		outMatrix._34 = Matrix->_43;

		outMatrix._41 = Matrix->_14;
		outMatrix._42 = Matrix->_24;
		outMatrix._43 = Matrix->_34;
		outMatrix._44 = Matrix->_44;

		return outMatrix;
	}

public:
	static void Init();
	static void Uninit();
	static void Begin();
	static void End();

	static void CreateDepthTexture();
	static void SetRenderTarget(RenderMode mode);

	static void SetDepthEnable(bool Enable);
	static void SetRenderType(bool Enable);

	static void SetVertexBuffers( ID3D11Buffer* VertexBuffer );
	static void SetIndexBuffer( ID3D11Buffer* IndexBuffer );
	static void SetTexture( CTexture* Texture, int numview = 0);
	static void SetDepthTexture(int slot);
	static void DrawIndexed( unsigned int IndexCount, unsigned int StartIndexLocation, int BaseVertexLocation );
	static void DrawIndexedInstanced(unsigned int IndexCount, int InstancedCount, unsigned int StartIndexLocation, int BaseVertexLocation);

	static ID3D11Device* GetDevice( void ){ return m_D3DDevice; }
	static ID3D11DeviceContext* GetDeviceContext( void ){ return m_ImmediateContext; }
	static IDXGISwapChain* GetSwapChain(void) { return m_SwapChain; }
	static ID3D11RenderTargetView* GetRenderTargetView(void) { return m_RenderTargetView; }
	static ID3D11DepthStencilView* GetDepthStencilView(void) { return m_DepthStencilView; }

	static ID3D11RenderTargetView*	 GetDepthRenderTargetView() { return m_DepthRenderTargetView; }
	static ID3D11ShaderResourceView* GetDepthShaderResourceView() { return m_DepthResourceView; }

	static void SetWorldMatrix(XMFLOAT4X4* m);
	static void SetViewMatrix(XMFLOAT4X4* m);
	static void SetProjectionMatrix(XMFLOAT4X4* m);

	static void SetLight(CLight* l);
	static void SetMaterial(MATERIAL m);
	static void SetCamera(CCamera* c);
	static void SetFog(FOG f);
	static void SetTime(TIME t);

};
