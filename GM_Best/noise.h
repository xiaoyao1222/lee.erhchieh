#pragma once

//指定範囲でクリップする.
#define CLIP(e,l,h) (std::min(std::max(e,l),h))

//配列の要素数取得.
#define COUNTOF(a) ( sizeof( a ) / sizeof( a[0] ) )

#define HASH_CODE_MAX       (256)
#define HASH_CODE_TABLE_NUM     (HASH_CODE_MAX*2)

#define TABLE_SIZE 200

class CNoise
{
public:
	enum NoiseType
	{
		Value = 0,
		Perlin,
		MidpointDisplace
	};

private:
	int Hash_Seed;
	NoiseType m_Type;

	int m_hashcode[HASH_CODE_TABLE_NUM] = {};
	int m_NoiseValue[TABLE_SIZE][TABLE_SIZE];

	//ハッシュ関連
	void SetHash(unsigned int seed);
	unsigned int GetHash(int x, int y);

	//補正関連
	float GetValue(int x, int y);
	float Fade(float t);
	float Lerp(float a, float b, float t);

	//バリューノイズ計算
	float ValueNoise(float x, float y);
	float OctaveValueNoise(float x, float y);

	//パーリンノイズ計算
	float Grad(unsigned int hash, float a, float b);
	float PerlinNoise(float x, float y);
	float OctavePerlinNoise(float x, float y);

	void SetupMidpointDisplaceNoise(unsigned int seed);
	void SetupMidpointDisplaceNoise(XMFLOAT2 topLeft, XMFLOAT2 rightBottom, int heightMax);
	float GetMidpointDisplaceNoise(float x, float y);

public:

	CNoise() : m_Type(Perlin) {
		SetHash(0);

		SetupMidpointDisplaceNoise(0);
	}

	CNoise(NoiseType type) : m_Type(type) {
		SetHash(0);

		SetupMidpointDisplaceNoise(0);
	}

	CNoise(int seed) : Hash_Seed(seed), m_Type(Perlin) {
		SetHash(Hash_Seed);

		SetupMidpointDisplaceNoise(Hash_Seed);
	}

	CNoise(int seed, NoiseType type) : Hash_Seed(seed), m_Type(type) {
		SetHash(Hash_Seed);

		SetupMidpointDisplaceNoise(Hash_Seed);
	}

	//ノイズコール
	float Call(float x, float y, float rect_size, float range_max);
};
