//=================================================================
//制作者　李爾捷
//アクター管理クラス（CActorManager）
//アクターたちを管理するマネージャーです
//今現在はプレイヤーと船だけ存在している
//=================================================================

#include "main.h"
#include "renderer.h"

#include "actormanager.h"

#include "modelmanager.h"
#include "texturemanager.h"
#include "shadermanager.h"
#include "cameramanager.h"
#include "player.h"
#include "boat.h"

void CActorManager::Init()
{

}

void CActorManager::Build(ACTOR_TYPE type, XMFLOAT3 player_pos)
{
	m_pPlayer = new CPlayer(player_pos, XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f));
	m_pPlayer->Init(this);
	switch (type)
	{
	case NORMAL:

		for (int cnt = 0; cnt < MAX_BOAT; cnt++)
		{
			CBoat* b = new CBoat(cnt, this);
			b->Init();
			m_pBoats.push_back(b);
		}
		break;
	case DESERT:
		break;
	case MOUNTAIN:
		break;
	}
}

void CActorManager::Uninit()
{

	m_pPlayerTextures.clear();

	for (auto b : m_pBoats)
	{
		delete b;
	}

	m_pBoats.clear();

	if (m_pPlayer != nullptr)
	{
		delete m_pPlayer;
	}

}

void CActorManager::Update()
{
	m_pPlayer->Update();

	for (auto b : m_pBoats)
	{
		b->Update();
	}
}

void CActorManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:

		for (auto b : m_pBoats)
		{
			if (!m_pCamM->GetCamera()->GetVisibility(b->GetPos()))continue;

			b->Draw(m_pShadowShader, m_pBoatModel);
		}

		break;

	case NORMAL_MODE:

		for (auto b : m_pBoats)
		{
			if (!m_pCamM->GetCamera()->GetVisibility(b->GetPos()))continue;

			b->Draw(m_pBoatShader, m_pEnemyTextures, m_pBoatModel);
		}

		break;
	case IMGUI_MODE:

		m_pPlayer->Draw();

		if (m_pBoats.size() > 0)
		{
			(*m_pBoats.begin())->Draw();
		}

		break;
	}
}

void CActorManager::Load(CModelManager* m)
{
	m_pBoatModel = m->SetModel("asset/Boat/Boat.obj");
}

void CActorManager::Load(CTextureManager* m)
{
	m_pEnemyTextures.push_back(m->SetTexture("asset/toon.tga"));
}

void CActorManager::Load(CShaderManager* m)
{
	m_pBoatShader = m->SetShader("shader/ModelVS.cso", "shader/ModelPS.cso");
	m_pShadowShader = m->SetShader("shader/SoftShadowVS.cso", "shader/SoftShadowPS.cso");
}