//=================================================================
//制作者　李爾捷
//カメラアクタークラス（CCamera）
//=================================================================

#pragma once

#include "actor.h"

class CCameraManager;

class CCamera : public CActor
{
private:
	//XMMATRIXに変換必要
	XMFLOAT4X4	m_ViewMatrix;
	XMFLOAT4X4	m_ProjectionMatrix;

	RECT		m_Viewport;
	XMMATRIX	m_InvViewMatrix;

	XMFLOAT3 m_Front;

	CCameraManager* m_pCamM;

public:
	CCamera(){}
	CCamera(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~CCamera(){ Uninit();}

	void Init(CGameObject* m);
	void Uninit();
	void Update();
	void Draw();

	XMMATRIX getInView() { return m_InvViewMatrix; }

	XMFLOAT4X4 GetView(void) { return m_ViewMatrix; }
	XMFLOAT4X4 GetProjection(void) { return m_ProjectionMatrix; }

	XMFLOAT3 GetFrontVector() { return m_Front; }
	void SetFrontVector(XMFLOAT3 v) { m_Front = v; }

	bool GetVisibility(XMFLOAT3 Pos);
};