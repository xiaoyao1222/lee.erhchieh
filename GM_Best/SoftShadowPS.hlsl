//*****************************************************************************
// 定数バッファ
//*****************************************************************************

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}


//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;

	float4 posWV : TEXCOORD0;
};

float4 main(in  PixelInputType input) : SV_Target
{
	float depthValue;
	float4 color;

	//depthValue = input.posWVP.z / input.posWVP.w;

	depthValue = input.posWV.z / (Light.Near_Far.y - Light.Near_Far.x);

	color = float4(depthValue, 0.0f, 0.0f, 1.0f);

	// Z値算出
	return color;
}