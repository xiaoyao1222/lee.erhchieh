//=================================================================
//制作者　李爾捷
//テクスチャ管理クラス（CTextureManager）
//=================================================================

#pragma once

#include "texture.h"
#include "gameobject.h"

class CTextureManager : public CGameObject
{
private:
	std::unordered_map<std::string, CTexture*> m_pTextures;


public:
	CTextureManager()
	{
		m_pTextures.clear();
	}
	~CTextureManager() { Uninit(); }

	void Init();
	void Uninit();

	CTexture* SetTexture(std::string texture_path);
};
