//=================================================================
//制作者　李爾捷
//ダンジョンファクトリークラス（CFactoryDungeon）
//=================================================================

#pragma once

#include "factory.h"

class CFactoryDungeon : public CFactory
{
public:
	CFactoryDungeon();
	~CFactoryDungeon() {}

	void LoadAssert() override;
	void BuildGameObject() override;

	void SetManager() override;
};
