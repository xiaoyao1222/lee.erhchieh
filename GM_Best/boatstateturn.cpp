//=================================================================
//制作者　李爾捷
//船の回転ステートクラス（CBoatStateTurn）
//１.次のポイントから目標ベクトルを作成
//２.現在のクォータニオンを記録
//３.目標ベクトルと初期ベクトルでアングルを計算する
//４.アングルを使って回転先のクォータニオンを計算する
//５.現在のクォータニオンと回転先のクォータニオンに球面補間
//６.補間済みのクォータニオンからアングルを取得
//７.船のローテイションにセット
//８.補間の時間が1になったら→ゴーステート
//
//斜め回転たまにバグがある→修正必須項目
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "boatstateturn.h"

#include "boat.h"
#include "boatstatego.h"
#include "waypoint.h"

CBoatStateTurn::CBoatStateTurn(CBoat* boat, std::list<CWayPoint*>* p)
	:m_pPoints(*p), m_Time(0.0f), m_TurnSpeed(0.1f), m_AngleR(0.0f), m_AngleSlerp(0.0f)
{
	CWayPoint* point = *p->begin();
	XMFLOAT3 pos;
	pos = boat->GetPos();

	m_pTargetFront.x = point->GetPos().x - pos.x;
	m_pTargetFront.y = point->GetPos().y - pos.y;
	m_pTargetFront.z = point->GetPos().z - pos.z;

	XMVECTOR vf = XMLoadFloat3(&m_pTargetFront);

	vf = XMVector3Normalize(vf);

	XMStoreFloat3(&m_pTargetFront, vf);

	m_pStartRot.x = boat->GetRot().x;
	m_pStartRot.y = boat->GetRot().y;
	m_pStartRot.z = boat->GetRot().z;
}

void CBoatStateTurn::Update(CBoat* boat)
{
	//今のクォータニオン
	XMVECTOR qn = XMQuaternionRotationRollPitchYaw(m_pStartRot.x, m_pStartRot.y, m_pStartRot.z);

	//角度計算
	XMVECTOR vta = XMLoadFloat3(&m_pTargetFront);
	XMVECTOR f = XMLoadFloat3(&XMFLOAT3(-1.0f, 0.0f, 0.0f));
	XMVECTOR va = XMVector3AngleBetweenNormals(f, vta);
	float a;
	XMStoreFloat(&a, va);

	if (XMConvertToDegrees(a) <= 5.0f)
	{
		boat->SetFront(m_pTargetFront);
		boat->ChangeState(new CBoatStateGo(boat, &m_pPoints));
		return;
	}
	
	XMVECTOR vc = XMVector3Cross(f, vta);
	float c;
	XMStoreFloat(&c, vc);

	if (m_pTargetFront.z <= -0.1)
	{
		a = XMConvertToRadians(360) - a;
	}

	m_AngleR = a;
	//目標のクォータニオン
	XMVECTOR qt = XMQuaternionRotationRollPitchYaw(0.0f, a, 0.0f);

	//補間
	XMVECTOR q = XMQuaternionSlerp(qn, qt, m_Time);

	//補間後のアングルを取得
	XMQuaternionToAxisAngle(&XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f), &m_AngleSlerp, q);

	XMFLOAT3 r = XMFLOAT3(0.0f, m_AngleSlerp, 0.0f);

	boat->SetRot(r);

	if (m_Time >= 1.0f)
	{
		boat->ChangeState(new CBoatStateGo(boat, &m_pPoints));
		return;
	}

	m_Time += m_TurnSpeed;
}

void CBoatStateTurn::Draw()
{
	ImGui::Text("Turn Now");
	ImGui::Text("AngleD = %2f", XMConvertToDegrees(m_AngleR));
	ImGui::Text("AngleSlerpD = %2f", XMConvertToDegrees(m_AngleSlerp));
}