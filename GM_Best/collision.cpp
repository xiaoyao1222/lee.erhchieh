///////////////////////////////////////////////////////
//				shooting game
//				距離判断処理　collision.cpp
//				製作者：李爾捷
///////////////////////////////////////////////////////

//インクルードファイル
//=============================================================================
// インクルードファイル
//=============================================================================
#include "main.h"
#include "renderer.h"
#include "collision.h"

//当たり判定関数
//
//引数	:pA　→　物Aのアドレス
//		:pB	 →　物Bのアドレス
//
//戻り値:成功／失敗
//


bool CCollisionSphere::IsHit(CCollisionSphere* pB)
{
	float lenSqV;;
	XMStoreFloat(&lenSqV, XMVector3LengthSq(m_centerpos - pB->GetCenterPos()));
	return lenSqV < ((m_radius + pB->GetRadius()) * (m_radius + pB->GetRadius()));
}

bool CCollisionSphere::IsHit(CCollisionCube* pB)
{
	float SqLen = 0;
	XMFLOAT3 pa, pb;
	XMStoreFloat3(&pa, m_centerpos);
	XMStoreFloat3(&pb, pB->GetCenterPos());

	//x判断
	if (pa.x < pb.x - pB->GetRadius().x)
	{
		SqLen += (pa.x - (pb.x - pB->GetRadius().x)) * (pa.x - (pb.x - pB->GetRadius().x));
	}

	if (pa.x > pb.x + pB->GetRadius().x)
	{
		SqLen += (pa.x - (pb.x + pB->GetRadius().x)) * (pa.x - (pb.x + pB->GetRadius().x));
	}

	//y判断
	if (pa.y < pb.y - pB->GetRadius().y)
	{
		SqLen += (pa.y - (pb.y - pB->GetRadius().y)) * (pa.y - (pb.y - pB->GetRadius().y));
	}

	if (pa.y > pb.y + pB->GetRadius().y)
	{
		SqLen += (pa.y - (pb.y + pB->GetRadius().y)) * (pa.y - (pb.y + pB->GetRadius().y));
	}

	//z判断
	if (pa.z < pb.z - pB->GetRadius().z)
	{
		SqLen += (pa.z - (pb.z - pB->GetRadius().z)) * (pa.z - (pb.y - pB->GetRadius().z));
	}

	if (pa.z > pb.z + pB->GetRadius().z)
	{
		SqLen += (pa.z - (pb.z + pB->GetRadius().z)) * (pa.z - (pb.z + pB->GetRadius().z));
	}

	return sqrt(SqLen) < m_radius;
}

bool CCollisionCapsule::IsHit(CCollisionSphere* pB)
{
	XMVECTOR p1, p2;
	float t1, t2;
	// S2の始点とS1の最短問題に帰着
	float len = calcPointSegmentDist3D(pB->GetCenterPos(), *m_line, p1, t1);
	p2 = pB->GetCenterPos();
	clamp01(t1);
	t2 = 0.0f;

	return (len <= m_radius + pB->GetRadius());
}

bool CCollisionCapsule::IsHit(CCollisionCapsule* pB)
{
	XMVECTOR p1, p2;
	float t1, t2;

	float d = calcSegmentSegmentDist3D(*m_line, *pB->GetLine(), p1, p2, t1, t2);


	return (d <= m_radius + pB->GetRadius());
}

// 0〜1の間にクランプ
void CCollision::clamp01(float &v)
{
	if (v < 0.0f)
		v = 0.0f;
	else if (v > 1.0f)
		v = 1.0f;
}

float CCollision::calcPointLineDist3D(XMVECTOR &p, CCollisionLine &l, XMVECTOR &h, float &t)
{
	float lenSqV;
	XMStoreFloat(&lenSqV, XMVector3LengthSq(l.GetVec()));
	t = 0.0f;
	if (lenSqV > 0.0f)
	{
		XMStoreFloat(&t, XMVector3Dot(l.GetVec(), (p - l.GetPos()) / lenSqV));
	}
	h = l.GetPos() + t * l.GetVec();

	float end;
	XMStoreFloat(&end, XMVector3Length(h - p));
	return end;
}

float CCollision::calcPointSegmentDist3D(XMVECTOR &p, CCollisionLine &seg, XMVECTOR &h, float &t)
{

	const XMVECTOR e = seg.GetPos() + seg.GetVec();

	// 垂線の長さ、垂線の足の座標及びtを算出
	float len = calcPointLineDist3D(p, seg, h, t);

	float d1, d2;
	XMStoreFloat(&d1, XMVector3Dot((p - seg.GetPos()), (e - seg.GetPos())));
	XMStoreFloat(&d2, XMVector3Dot((p - e), (seg.GetPos() - e)));
	if (d2 < 0.0f) {
		// 始点側の外側
		h = seg.GetPos();
		XMStoreFloat(&len, XMVector3Length(seg.GetPos() - p));
		return len;
	}
	else if (d2 < 0.0f) {
		// 終点側の外側
		h = e;
		XMStoreFloat(&len, XMVector3Length(e - p));
		return len;
	}

	return len;
}

float CCollision::calcLineLineDist3D(CCollisionLine &l1, CCollisionLine &l2, XMVECTOR &p1, XMVECTOR &p2, float &t1, float &t2)
{
	XMVECTOR vec;
	XMFLOAT3 fvec;
	vec = XMVector3Cross(l1.GetVec(), l2.GetVec());
	XMStoreFloat3(&fvec, vec);
	// 2直線が平行？
	if (fvec.x == 0.0f && fvec.y == 0.0f && fvec.z == 0.0f) {

		// 点P11と直線L2の最短距離の問題に帰着
		float len = calcPointLineDist3D(l1.GetPos(), l2, p2, t2);
		p1 = l1.GetPos();
		t1 = 0.0f;

		return len;
	}
	float DV1V2, DV1V1, DV2V2;
	// 2直線はねじれ関係

	XMStoreFloat(&DV1V2, XMVector3Dot(l1.GetVec(), l2.GetVec()));
	XMStoreFloat(&DV1V1, XMVector3LengthSq(l1.GetVec()));
	XMStoreFloat(&DV2V2, XMVector3LengthSq(l2.GetVec()));

	XMVECTOR P21P11 = l1.GetPos() - l2.GetPos();
	XMStoreFloat(&t1,(DV1V2 * XMVector3Dot(l2.GetVec(), P21P11) - DV2V2 * XMVector3Dot(l1.GetVec(), P21P11)) / (DV1V1 * DV2V2 - DV1V2 * DV1V2));
	p1 = l1.GetPos() + l1.GetVec();
	XMStoreFloat(&t2, XMVector3Dot(l2.GetVec(), (p1 - l2.GetPos())) / DV2V2);
	p2 = l2.GetPos() + l2.GetVec();

	float end;
	XMStoreFloat(&end, XMVector3Length(p2 - p1));
	return end;
}

float CCollision::calcSegmentSegmentDist3D(CCollisionLine &s1, CCollisionLine &s2, XMVECTOR &p1, XMVECTOR &p2, float &t1, float &t2)
{
	float s1l, s2l;
	XMStoreFloat(&s1l, XMVector3LengthSq(s1.GetVec()));
	XMStoreFloat(&s2l, XMVector3LengthSq(s2.GetVec()));
	// S1が縮退している？
	if (s1l < _OX_EPSILON_) {
		// S2も縮退？
		if (s2l < _OX_EPSILON_) {
			// 点と点の距離の問題に帰着
			float len;
			XMStoreFloat(&len, XMVector3Length(s2.GetPos() - s1.GetPos()));
			p1 = s1.GetPos();
			p2 = s2.GetPos();
			t1 = t2 = 0.0f;
			return len;
		}
		else {
			// S1の始点とS2の最短問題に帰着
			float len = calcPointSegmentDist3D(s1.GetPos(), s2, p2, t2);
			p1 = s1.GetPos();
			t1 = 0.0f;
			clamp01(t2);
			return len;
		}
	}

	// S2が縮退している？
	else if (s2l < _OX_EPSILON_) {
		// S2の始点とS1の最短問題に帰着
		float len = calcPointSegmentDist3D(s2.GetPos(), s1, p1, t1);
		p2 = s2.GetPos();
		clamp01(t1);
		t2 = 0.0f;
		return len;
	}

	/* 線分同士 */

	XMVECTOR vec;
	XMFLOAT3 fvec;
	vec = XMVector3Cross(s1.GetVec(), s2.GetVec());
	XMStoreFloat3(&fvec, vec);

	// 2線分が平行だったら垂線の端点の一つをP1に仮決定
	if (fvec.x == 0.0f && fvec.y == 0.0f && fvec.z == 0.0f) {
		t1 = 0.0f;
		p1 = s1.GetPos();
		float len = calcPointSegmentDist3D(s1.GetPos(), s2, p2, t2);
		if (0.0f <= t2 && t2 <= 1.0f)
			return len;
	}
	else {
		// 線分はねじれの関係
		// 2直線間の最短距離を求めて仮のt1,t2を求める
		float len = calcLineLineDist3D(s1, s2, p1, p2, t1, t2);
		if (
			0.0f <= t1 && t1 <= 1.0f &&
			0.0f <= t2 && t2 <= 1.0f
			) {
			return len;
		}
	}

	// 垂線の足が外にある事が判明
	// S1側のt1を0〜1の間にクランプして垂線を降ろす
	clamp01(t1);
	p1 = s1.GetPos() + s1.GetVec();
	float len = calcPointSegmentDist3D(p1, s2, p2, t2);
	if (0.0f <= t2 && t2 <= 1.0f)
		return len;

	// S2側が外だったのでS2側をクランプ、S1に垂線を降ろす
	clamp01(t2);
	p2 = s2.GetPos() + s2.GetVec();
	len = calcPointSegmentDist3D(p2, s1, p1, t1);
	if (0.0f <= t1 && t1 <= 1.0f)
		return len;

	// 双方の端点が最短と判明
	clamp01(t1);
	p1 = s1.GetPos() + s1.GetVec();

	float end;
	XMStoreFloat(&end, XMVector3Length(p2 - p1));
	return end;
}

void CCollisionLine::Init()
{
}

void CCollisionLine::Draw()
{
}


void CCollisionCube::Init()
{
}

void CCollisionCube::Draw()
{
}

void CCollisionSphere::Init()
{
	float angle = XMConvertToRadians(360.0f / CIRCLE_VERTEX_COUNT);
	int VertexCnt = 24;

	VERTEX_3D* pV = new VERTEX_3D[VertexCnt];

	for (int i = 0; i < CIRCLE_VERTEX_COUNT; i++)
	{
		pV[i].Position.x = cosf(angle * i) * m_radius;
		pV[i].Position.y = sinf(angle * i) * m_radius;
		pV[i].Position.z = 0.0f			   * m_radius;
		pV[i].Diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	for (int i = 0; i < CIRCLE_VERTEX_COUNT; i++)
	{
		pV[i + CIRCLE_VERTEX_COUNT].Position.x = 0.0f			 * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT].Position.y = sinf(angle * i) * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT].Position.z = cosf(angle * i) * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT].Diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	for (int i = 0; i < CIRCLE_VERTEX_COUNT; i++)
	{
		pV[i + CIRCLE_VERTEX_COUNT * 2].Position.x = cosf(angle * i) * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT * 2].Position.y = 0.0f			 * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT * 2].Position.z = sinf(angle * i) * m_radius;
		pV[i + CIRCLE_VERTEX_COUNT * 2].Diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
	}

	int indexcnt = CIRCLE_VERTEX_COUNT * 2 * 3;
	WORD* pIndex = new WORD[indexcnt];

	for (int j = 0; j < 3; j++)
	{
		for (int i = 0; i < CIRCLE_VERTEX_COUNT; i++)
		{
			pIndex[i * 2 + CIRCLE_VERTEX_COUNT * 2 * j] = i + CIRCLE_VERTEX_COUNT * j;
			pIndex[i * 2 + 1 + CIRCLE_VERTEX_COUNT * 2 * j] = (i + 1) % CIRCLE_VERTEX_COUNT + CIRCLE_VERTEX_COUNT * j;
		}
	}



	//=============================================================================
	//頂点バッファ生成
	D3D11_BUFFER_DESC vbd;
	ZeroMemory(&vbd, sizeof(vbd));
	vbd.Usage = D3D11_USAGE_DEFAULT;
	vbd.ByteWidth = sizeof(VERTEX_3D) * VertexCnt;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
	vbd.CPUAccessFlags = 0;

	D3D11_SUBRESOURCE_DATA vsd;
	ZeroMemory(&vsd, sizeof(vsd));
	vsd.pSysMem = pV;

	CRenderer::GetDevice()->CreateBuffer(&vbd, &vsd, &m_VertexBuffer);

	delete pV;
	//=============================================================================
	//=============================================================================
	//インデクスバッファ生成
	D3D11_BUFFER_DESC ibd;
	ZeroMemory(&ibd, sizeof(ibd));
	ibd.Usage = D3D11_USAGE_DEFAULT;
	ibd.ByteWidth = sizeof(WORD) * indexcnt;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;					//インデクスバッファ設定
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;

	D3D11_SUBRESOURCE_DATA isd;
	ZeroMemory(&isd, sizeof(isd));
	isd.pSysMem = pIndex;

	CRenderer::GetDevice()->CreateBuffer(&ibd, &isd, &m_IndexBuffer);

	delete pIndex;
	//=============================================================================
}

void CCollisionSphere::Draw()
{
#if defined(_DEBUG) || defined(DEBUG)



	UINT stride = sizeof(VERTEX_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;																					//ワールドマトリクス
	world = XMMatrixScaling(1.0f, 1.0f, 1.0f);														//拡大縮小
	world *= XMMatrixRotationRollPitchYaw(m_Rotation.x, m_Rotation.y, m_Rotation.z);										//回転
	world *= XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);													//移動

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);		//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexed(CIRCLE_VERTEX_COUNT * 2 * 3, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)

#endif


}

void CCollisionCapsule::Init()
{
}

void CCollisionCapsule::Draw()
{

}