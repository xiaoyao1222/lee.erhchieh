//=================================================================
//制作者　李爾捷
//シェーダ管理クラス（CShaderManager）
//=================================================================

#pragma once

#include "shader.h"
#include "gameobject.h"

class CShaderManager : public CGameObject
{
private:

	std::map<std::pair<std::string, std::string>, CShader*> m_pShaders;

public:
	CShaderManager()
	{
		m_pShaders.clear();
	}
	~CShaderManager() { Uninit(); }

	void Init();
	void Uninit();

	CShader* SetShader(std::string vs_path, std::string ps_path);
};

