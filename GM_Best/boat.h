//=================================================================
//制作者　李爾捷
//船アクタークラス（CBoat）
//=================================================================

#pragma once

#include "actor.h"

class CActorManager;
class CBoatState;

class CModel;
class CShader;

class CBoat : public CActor
{
private:
	int		 m_ID;
	bool	 m_Stop;
	XMFLOAT3 m_Front;
	float	 m_Speed;

	CBoatState* m_pBoatState;

public:
	CBoat() {}
	CBoat(int id, CActorManager* m);
	~CBoat() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	void Draw(CShader* s, CModel* m) override;
	void Draw(CShader* s, std::list<CTexture*> t, CModel* m) override;

	void ChangeState(CBoatState* state);
	CActorManager* GetActorM() { return m_pActorM; }

	void SetFront(XMFLOAT3 f) { m_Front = f; }
	void SetSpeed(float s)	  { m_Speed = s; }

	XMFLOAT3 GetFront() { return m_Front; }
	float	 GetSpeed() { return m_Speed; }
};
