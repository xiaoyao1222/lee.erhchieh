//=================================================================
//制作者　李爾捷
//ウェイポイントクラス（CWayPoint）
//フィールド作成時同時作成、同時に現在リスト内のポイント（一定距離内）と連結
//フィールド作成完了時連結したポイントの障害物チェック
//ウエイポイントステータス
//１.NONE	→	初期状態
//２.OPEN	→	検索可能、同時にスタートとゴールポイントまでの距離を計算する
//３.CLOSED	→	検索済み、連結したポイントもしNONEならばOPENする、そしてOPENのポイントを検索する
//検索成功	→	return true
//検索失敗	→	return false
//=================================================================

#include "main.h"
#include "renderer.h"

#include "waypoint.h"
#include "fieldmanager.h"
#include "shader.h"
#include "model.h"

void CWayPoint::Draw(CShader* s, CModel* m)
{
	// マトリクス設定
	XMMATRIX world;
	world = XMMatrixScaling(1.0f, 1.0f, 1.0f);
	world *= XMMatrixRotationRollPitchYaw(0.0f, 0.0f, 0.0f);
	world *= XMMatrixTranslation(m_Position.x, m_Position.y, m_Position.z);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	m->Draw();
}

float CWayPoint::CalculateDistance(XMFLOAT3 pos)
{
	XMFLOAT3 v;
	XMVECTOR V, D;
	float d;
	v.x = m_Position.x - pos.x;
	v.y = m_Position.x - pos.x;
	v.z = m_Position.z - pos.z;

	V = XMLoadFloat3(&v);

	D = XMVector3Length(V);

	XMStoreFloat(&d, D);

	return d;
}

void CWayPoint::CheckLinkList(CFieldManager* m)
{
	auto search = [&m, Position = m_Position](CWayPoint* point) {
		XMFLOAT3 v, pos;
		XMVECTOR V;

		pos = Position;

		v.x = point->GetPos().x - pos.x;
		v.y = point->GetPos().y - pos.y;
		v.z = point->GetPos().z - pos.z;

		V = XMLoadFloat3(&v);
		V = XMVector3Normalize(V);

		XMStoreFloat3(&v, V);

		while (true)
		{
			pos.x += v.x;
			pos.y += v.y;
			pos.z += v.z;

			if (m->GetHeight(pos) >= 4.0f)
			{
				return true;
			}
			else if (point->GetDistance_sq(pos) <= 1.0f)
			{
				return false;
			}
		}
	};

	m_LinkPoints.erase(std::remove_if(m_LinkPoints.begin(), m_LinkPoints.end(), search), m_LinkPoints.end());
}

bool CWayPoint::SetStatus(WAYPOINT_STATUS s, CWayPoint* startp, CWayPoint* goalp, std::list<CWayPoint*>* path)
{
	m_Status = s;

	switch (m_Status)
	{
	case NONE:
		return true;
	case OPEN:

		if (startp != nullptr)
		{
			m_Distance = CalculateDistance(startp->GetPos());
		}
		if (goalp != nullptr)
		{
			m_Heuristic = CalculateDistance(goalp->GetPos());
		}
		return true;
	case CLOSED:

		if (m_Heuristic <= 0)
		{
			path->push_front(this);
			return true;
		}

		for (auto p : m_LinkPoints)
		{
			if (p->GetStatus() == NONE)
			{
				p->SetStatus(OPEN, startp, goalp);
			}
		}

		auto search = [](CWayPoint* p1, CWayPoint* p2)
		{
			return p1->GetScore() < p2->GetScore();
		};

		m_LinkPoints.sort(search);

		for (auto p : m_LinkPoints)
		{
			if (p->GetStatus() == OPEN)
			{
				if (p->SetStatus(CLOSED, startp, goalp, path))
				{
					path->push_front(this);
					return true;
				}
			}
		}
		return false;
	}

	return false;
}