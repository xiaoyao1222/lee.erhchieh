//=================================================================
//制作者　李爾捷
//ライトクラス（CLight）
//=================================================================

#pragma once

class CLight
{
private:
	XMFLOAT4	m_Direction;
	COLOR		m_Diffuse;
	COLOR		m_Ambient;

	XMFLOAT4	m_Position;
	XMFLOAT4	m_LookAt;
	float		m_Near;
	float		m_Far;
	float		m_Radius;

	XMFLOAT4X4	m_ViewMatrix;
	XMFLOAT4X4	m_OrthoMatrix;
	XMFLOAT4X4	m_ProjectionMatrix;

public:

	CLight(){}
	~CLight(){}

	void Init(float depth_near, float depth_far);

	void SetDirection(XMFLOAT4 d)			{ m_Direction = d; }
	void SetDiffuse(COLOR c)				{ m_Diffuse = c; }
	void SetAmbient(COLOR c)				{ m_Ambient = c; }
	void SetPosition(XMFLOAT4 p)			{ m_Position = p; }
	void SetLookAt(XMFLOAT4 p)				{ m_LookAt = p; }
	void SetNear(float n)					{ m_Near = n; }
	void SetFar(float f)					{ m_Far = f; }
	void SetRadius(float r)					{ m_Radius = r; }
	void SetViewMatrix(XMFLOAT4X4 m)		{ m_ViewMatrix = m; }
	void SetOrthoMatrix(XMFLOAT4X4 m)		{ m_OrthoMatrix = m; }
	void SetProjectionMatrix(XMFLOAT4X4 m)	{ m_ProjectionMatrix = m; }

	void GenerateViewMatrix(void);
	void GenerateOrthoMatrix(float width);
	void GenerateProjectionMatrix();

	XMFLOAT4	GetDirection(void)			{ return m_Direction; }
	COLOR		GetDiffuse(void)			{ return m_Diffuse; }
	COLOR		GetAmbient(void)			{ return m_Ambient; }
	XMFLOAT4	GetPosition(void)			{ return m_Position; }
	XMFLOAT4	GetLookAt(void)				{ return m_LookAt; }
	float		GetNear(void)				{ return m_Near; }
	float		GetFar(void)				{ return m_Far; }
	float		GetRadius(void)				{ return m_Radius; }
	XMFLOAT4X4	GetViewMatrix(void)			{ return m_ViewMatrix; }
	XMFLOAT4X4	GetOrthoMatrix(void)		{ return m_OrthoMatrix; }
	XMFLOAT4X4	GetProjectionMatrix(void)	{ return m_ProjectionMatrix; }



};
