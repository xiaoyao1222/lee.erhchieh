//=================================================================
//制作者　李爾捷
//海フィールドクラス（COcean）
//フィールド完成した後で生成
//基本高さは4.0f
//フィールドよりプロシージャルでテクスチャ生成する可能性が高い
//=================================================================

#include "main.h"
#include "renderer.h"

#include "Ocean.h"

#include "texture.h"
#include "shader.h"

COcean::COcean(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale):
	m_Indexcnt(0)
{
	m_Transform.Position = pos;
	m_Transform.Rotation = rot;
	m_Transform.Scale = scale;
}

//cnt_x→Xの枚数	cnt_z→Zの枚数
void COcean::Init(int cnt_x, int cnt_z)
{
	XMFLOAT3 c1 = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMFLOAT3 c2 = XMFLOAT3(0.0f, 0.0f, 1.0f);
	XMVECTOR vC1 = XMLoadFloat3(&c1);
	XMVECTOR vC2 = XMLoadFloat3(&c2);

	//頂点データの準備
	int VertexCnt = (cnt_x + 1) * (cnt_z + 1);
	m_pV = new VERTEX_FIELD_3D[VertexCnt];

	{
		for (int iCntz = 0; iCntz <= cnt_z; iCntz++)
		{
			for (int iCntx = 0; iCntx <= cnt_x; iCntx++)
			{
				float x = m_Transform.Position.x - cnt_x / 2 + iCntx;
				float z = m_Transform.Position.z - cnt_z / 2 + iCntz;
				float y = 0.0f;

				m_pV[iCntx + (cnt_x + 1) * iCntz] = { XMFLOAT3(x, y, z),	//pos
					XMFLOAT3(0.0f,  1.0f, 0.0f),							//normal
					XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f),						//diffuse
					XMFLOAT2(0.0f + iCntx * 0.1f, cnt_z - iCntz * 0.1f),	//tex
					XMFLOAT3(1.0f, 0.0f, 0.0f),								//tan
					XMFLOAT3(0.0f, 0.0f, 1.0f) };							//binormal
			}
		}
	}

	//インデクスデータの準備
	m_Indexcnt = (2 + 2 * cnt_x) * cnt_z + (cnt_z - 1) * 2;
	WORD* pIndex = new WORD[m_Indexcnt];
	{
		int edge = (cnt_x + 2) * cnt_z - 1;

		for (int iCnt = 0; iCnt < edge; iCnt++)
		{
			if (iCnt % (cnt_x + 2) == cnt_x + 1 && iCnt != 0)
			{
				pIndex[iCnt * 2] = pIndex[(iCnt - 1) * 2 + 1];
				pIndex[iCnt * 2 + 1] = iCnt - (iCnt / (cnt_x + 2));
			}
			else
			{
				pIndex[iCnt * 2] = iCnt - (iCnt / (cnt_x + 2));
				pIndex[iCnt * 2 + 1] = cnt_x + 1 + iCnt - (iCnt / (cnt_x + 2));
			}
		}
	}

	//頂点バッファ生成
	{
		D3D11_BUFFER_DESC vbd;
		ZeroMemory(&vbd, sizeof(vbd));
		vbd.Usage = D3D11_USAGE_DEFAULT;
		vbd.ByteWidth = sizeof(VERTEX_FIELD_3D) * VertexCnt;
		vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;					//頂点バッファ設定
		vbd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA vsd;
		ZeroMemory(&vsd, sizeof(vsd));
		vsd.pSysMem = m_pV;

		CRenderer::GetDevice()->CreateBuffer(&vbd, &vsd, &m_VertexBuffer);
	}

	//インデクスバッファ生成
	{
		D3D11_BUFFER_DESC ibd;
		ZeroMemory(&ibd, sizeof(ibd));
		ibd.Usage = D3D11_USAGE_DEFAULT;
		ibd.ByteWidth = sizeof(WORD) * m_Indexcnt;
		ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;					//インデクスバッファ設定
		ibd.CPUAccessFlags = 0;
		ibd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA isd;
		ZeroMemory(&isd, sizeof(isd));
		isd.pSysMem = pIndex;

		CRenderer::GetDevice()->CreateBuffer(&ibd, &isd, &m_IndexBuffer);

		delete pIndex;
	}

}

void COcean::Uninit()
{
	//後片付け
	if (m_VertexBuffer != NULL)
	{
		m_VertexBuffer->Release();
	}

	if (m_IndexBuffer != NULL)
	{
		m_IndexBuffer->Release();
	}

	delete m_pV;
}

void COcean::Update()
{

}

void COcean::Draw(CShader* s)
{
	//フィールド描画

	UINT stride = sizeof(VERTEX_FIELD_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(0.0f, OCEAN_HEI, 0.0f);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexedInstanced(m_Indexcnt, 1, 0, 0, 0);

	//CRenderer::GetDeviceContext()->DrawIndexed(m_indexcnt, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(m_VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)
}

void COcean::Draw(CShader* s, std::list<CTexture*> t)
{
	//フィールド描画

	UINT stride = sizeof(VERTEX_FIELD_3D);
	UINT offset = 0;
	CRenderer::GetDeviceContext()->IASetVertexBuffers(0, 1, &m_VertexBuffer, &stride, &offset);		//頂点バッファ設定
	CRenderer::GetDeviceContext()->IASetIndexBuffer(m_IndexBuffer, DXGI_FORMAT_R16_UINT, 0);		//インデックスバッファ設定

	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(0.0f, OCEAN_HEI, 0.0f);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	int texcnt = 0;

	for (auto tex : t)
	{
		CRenderer::SetTexture(tex, texcnt);
		texcnt++;
	}

	CRenderer::SetDepthTexture(texcnt);

	CRenderer::GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP);	//トポロジー設定（描画ルール）

	CRenderer::GetDeviceContext()->DrawIndexedInstanced(m_Indexcnt, 1, 0, 0, 0);

	//CRenderer::GetDeviceContext()->DrawIndexed(m_indexcnt, 0, 0);									//ポリゴン描画(インデックス使用)
	//CRenderer::GetDeviceContext()->Draw(m_VertexCnt, 0);											//ポリゴン描画(頂点のみ使用)
}