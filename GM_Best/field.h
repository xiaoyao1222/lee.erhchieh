//=================================================================
//制作者　李爾捷
//フィールドクラス（CField）
//=================================================================

#pragma once

#include "fieldbase.h"

class CFieldManager;
class CTexture;

class CField : public CFieldBase
{
private:
	ID3D11Buffer*	 m_VertexBuffer = NULL;
	ID3D11Buffer*	 m_IndexBuffer = NULL;

	int				 m_Indexcnt;
	int				 m_Cnt_X;
	int				 m_Cnt_Z;

	VERTEX_FIELD_3D* m_pV;


public:
	CField() {}
	CField(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~CField(){ Uninit(); }

	void Init(int cnt_x, int cnt_z, CFieldManager* manager);
	void Uninit() override;
	void Update() override;
	void Draw() override;
	void Draw(CShader* s) override;
	void Draw(CShader* s, std::list<CTexture*> t) override;

	float GetHeight(XMFLOAT3 position);
};