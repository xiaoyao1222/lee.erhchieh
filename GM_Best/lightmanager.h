//=================================================================
//制作者　李爾捷
//ライト管理クラス（CLightManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "light.h"

class CTimeManager;
class CActorManager;

class CLightManager : public CGameObject
{
private:
	CLight*			m_Light;

	CTimeManager*	m_pTimeM;
	CActorManager*	m_pActorM;

public:
	CLightManager() :
		m_Light(new CLight()), m_pTimeM(nullptr), m_pActorM(nullptr) {}
	~CLightManager() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(RenderMode mode) override;

	void SetLight(CLight* light) { m_Light = light; }
	CLight* GetLight() { return m_Light; }

	void Load(CTimeManager* m) { m_pTimeM = m; }
	void Load(CActorManager* m) { m_pActorM = m; }
	CTimeManager* GetTimeM() { return m_pTimeM; }
};
