//=================================================================
//制作者　李爾捷
//シーンテンプレクラス（CScene）
//ゲームオブジェクトリストを持ち
//レイヤー五つに分けてる
//１.System	→	各アセット、時間
//２.Camera	→	カメラのみ
//３.Field	→	ライト、フィールド、スカイ
//４.Actor	→	アクターのみ
//５.UI		→	UIのみ
//
//描画は三回をやります
//１.影用描画
//２.通常描画
//３.ImGui描画
//=================================================================
#pragma once

#include <list>
#include <typeinfo>
#include <vector>

#include "renderer.h"
#include "imgui\imgui.h"
#include "gameobject.h"

#define MAX_LAYER 5

class CScene
{
public:
	enum Layer
	{
		Layer_System = 0,
		Layer_Camera,
		Layer_Field,
		Layer_Actor,
		Layer_UI
	};

	std::list<CGameObject*> m_GameObject[MAX_LAYER];

	template<typename Type>
	Type* AddGameObject(int Layer)
	{
		Type* object = new Type();
		object->Init();
		m_GameObject[Layer].push_back(object);
		return object;
	}

	//==================================封印↓==================================
	////指定されたレイヤー内、最初にあった又はデフォルトのオブジェクトをreturnする
	//template<typename Type>
	//Type* GetGameObject(int Layer)
	//{
	//	auto obj = from(m_GameObject[Layer])
	//		>> first_or_default([](CGameObject* object) {return (typeid(*object) == typeid(Type)); });

	//	 return (Type*)obj;
	//}

	//template<typename Type>
	//std::vector<Type*> GetGameObjects(int Layer)
	//{

	//	std::vector<Type*> objects;
	//	for (CGameObject* object : m_GameObject[Layer])
	//	{
	//		if (typeid(*object) == typeid(Type))
	//		{
	//			objects.push_back((Type*)object);
	//		}
	//	}
	//	return objects;
	//	
	//}
	//=========================================================================

	CScene() {}
	virtual ~CScene(){}

	virtual void Init() = 0;

	virtual void Uninit()
	{
		for (int Layercnt = 0; Layercnt < MAX_LAYER; Layercnt++)
		{
			for (CGameObject* object : m_GameObject[Layercnt])
			{
				delete object;

			}
			m_GameObject[Layercnt].clear();
		}
	}

	virtual void Update()
	{
		for (int Layercnt = 0; Layercnt < MAX_LAYER; Layercnt++)
		{
			for (CGameObject* object : m_GameObject[Layercnt])
			{
				object->Update();
			
			}
		}
	}

	virtual void Draw()
	{
		CRenderer::SetRenderTarget(SHADOW_MODE);
		for (int Layercnt = 0; Layercnt < MAX_LAYER; Layercnt++)
		{
			for (CGameObject* object : m_GameObject[Layercnt])
			{
				object->Draw(SHADOW_MODE);
			}
		}

		CRenderer::SetRenderTarget(NORMAL_MODE);
		for (int Layercnt = 0; Layercnt < MAX_LAYER; Layercnt++)
		{
			for (CGameObject* object : m_GameObject[Layercnt])
			{
				object->Draw(NORMAL_MODE);
			}

			//m_GameObject[Layercnt].remove_if([](CGameObject* object)
			//{
			//	return object->Destroy();
			//});
		}

		static int rendermode = 0;
		ImGui::SetNextWindowSize(ImVec2(350, 400));
		if (ImGui::Begin("System"))
		{
			enum {
				Solid,
				Wireframe,
			};

			ImGui::RadioButton("Solid", &rendermode, Solid);
			ImGui::SameLine();
			ImGui::RadioButton("Wireframe", &rendermode, Wireframe);

			if (rendermode == Solid) {
				CRenderer::SetRenderType(true);
			}
			else if (rendermode == Wireframe) {
				CRenderer::SetRenderType(false);
			}

			ImGui::BeginTabBar("Object");

			for (int Layercnt = 0; Layercnt < MAX_LAYER; Layercnt++)
			{
				for (CGameObject* object : m_GameObject[Layercnt])
				{
					object->Draw(IMGUI_MODE);
				}
			}

			ImGui::EndTabBar();

			ImGui::End();

			return;
		}

		ImGui::End();
	}

	//void DestroyGameObject(CGameObject* GameObject)
	//{
	//	GameObject->SetDestroy();
	//}

};
