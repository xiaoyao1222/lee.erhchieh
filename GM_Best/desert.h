#pragma once

#include "Scene.h"

class CAudioClip;
class CModelManager;
class CTextureManager;
class CShaderManager;

class CCameraManager;

class CTimeManager;
class CLightManager;
class CSkyManager;
class CFieldManager;
class CFogManager;
class CActorManager;
class CUIManager;

class CDesert : public CScene
{
private:
	CAudioClip* m_BGM = nullptr;

	CModelManager*		m_pModelM;
	CTextureManager*	m_pTexM;
	CShaderManager*		m_pShaderM;

	CTimeManager*		m_pTimeM;
	CCameraManager*		m_pCamM;
	CLightManager*		m_pLightM;
	CSkyManager*		m_pSkyM;
	CFieldManager*		m_pFieldM;
	CFogManager*		m_pFogM;
	CActorManager*		m_pActorM;
	CUIManager*			m_pUIM;

public:
	void Init();
	void Uninit();
	void Update();
	void Draw();
};
