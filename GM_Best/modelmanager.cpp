//=================================================================
//制作者　李爾捷
//モデル管理クラス（CModelManager）
//１.各モデルのパスを使って管理する（モデル用のテクスチャはここで管理）
//２.生成命令が来た時まずマップ内を確認する
//３.存在する	→	アドレスをコピー
//４.存在しない	→	ロードしてアドレスをコピー
//５.リターンアドレス
//=================================================================

#include "main.h"
#include "renderer.h"
#include "modelmanager.h"

void CModelManager::Init()
{

}

void CModelManager::Uninit()
{
	for (auto m : m_pModels)
	{
		m.second->Unload();
		delete m.second;
	}

	m_pModels.clear();
}

CModel* CModelManager::SetModel(std::string model_path)
{
	auto iter = m_pModels.find(model_path);

	if (iter == m_pModels.end())
	{
		m_pModels[model_path] = new CModel(model_path.c_str());
	}

	CModel* m = new CModel();
	m = m_pModels[model_path];

	return m;
}