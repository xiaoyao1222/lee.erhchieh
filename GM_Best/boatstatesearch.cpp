//=================================================================
//制作者　李爾捷
//船のルート検索ステートクラス（CBoatStateSearch）
//スタートポイントから検索して
//ルート検索成功→ターンステート
//		検索失敗→サーチステート
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "boatstatesearch.h"

#include "actormanager.h"
#include "fieldmanager.h"

#include "boat.h"
#include "boatstateturn.h"
#include "waypoint.h"

CBoatStateSearch::CBoatStateSearch(CBoat* boat, CWayPoint* startp)
	:m_pStartPoint(startp)
{

	m_pGoalPoint = boat->GetActorM()->GetFieldM()->GetWayPoint(startp->GetID());
	m_pGoalPoint->SetStatus(NONE);
	m_pStartPoint->SetStatus(OPEN, nullptr, m_pGoalPoint);
}

void CBoatStateSearch::Update(CBoat* boat)
{
	std::list<CWayPoint*> newPoints;

	if (m_pStartPoint->SetStatus(CLOSED, m_pStartPoint, m_pGoalPoint, &newPoints))
	{
		boat->ChangeState(new CBoatStateTurn(boat, &newPoints));
	}
	else
	{
		boat->ChangeState(new CBoatStateSearch(boat, m_pStartPoint));
	}

}

void CBoatStateSearch::Draw()
{
	ImGui::Text("Search Now");
}