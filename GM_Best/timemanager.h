//=================================================================
//制作者　李爾捷
//シーン内時間管理クラス（CTimeManager）
//=================================================================

#pragma once

#define MAX_TIME 3600

#include "gameobject.h"

class CTimeManager : public CGameObject
{
private:
	TIME	m_Time;

	float	m_Speed;
	bool	m_bFreeze;

public:
	CTimeManager():m_Speed(1.0f), m_bFreeze(false){}
	~CTimeManager() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;
	void Draw(RenderMode mode) override;

	TIME GetTime() { return m_Time; }
	void SetTime(float hr, float min, float sec, float flame) { m_Time.frame = XMFLOAT4(hr, min, sec, flame); }

	float GetSpeed() { return m_Speed; }
	void SetSpeed(float s) { m_Speed = s; }

	bool GetFreezeCheck() { return m_bFreeze; }
	void SwitchFreezeCheck() { m_bFreeze = !m_bFreeze; }
};