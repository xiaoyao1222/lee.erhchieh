//=================================================================
//����ҁ@������
//UI�Ǘ��N���X�iCUIManager�j
//=================================================================

#pragma once

#include "gameobject.h"
#include "polygon.h"

class CTextureManager;
class CShaderManager;

class CTexture;
class CShader;

class CUIManager : public CGameObject
{
private:
	CPolygon* m_pMapUI;

	CShader* m_pMapUIShader;
public:
	CUIManager():m_pMapUI(nullptr), m_pMapUIShader(nullptr){}
	~CUIManager() { Uninit(); }

	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CTextureManager* m);
	void Load(CShaderManager* m);
};