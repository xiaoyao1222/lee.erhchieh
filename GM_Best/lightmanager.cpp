//=================================================================
//制作者　李爾捷
//ライト管理クラス（CLightManager）
//タイムマネージャーから時間を取得して
//サンライズとサンセットを計算する
//プレイヤーの座標に追跡する
//
//課題：ライトの色と大気散乱シンクロする
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "lightmanager.h"

#include "timemanager.h"
#include "actormanager.h"
#include "player.h"


void CLightManager::Init()
{
	XMFLOAT4 lightp = XMFLOAT4(-0.0f, 3.0f, 0.0f, 0.0f);
	XMFLOAT4 lightat = XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f);
	XMFLOAT4 lightd = XMFLOAT4(-lightp.x, 1.0f - lightp.y, 0.0f, 0.0f);

	m_Light->Init(0.1f, 250.0f);

	m_Light->SetDirection(lightd);
	m_Light->SetDiffuse(COLOR(0.9f, 0.9f, 0.9f, 1.0f));
	m_Light->SetAmbient(COLOR(0.5f, 0.5f, 0.5f, 1.0f));

	m_Light->SetPosition(lightp);
	m_Light->SetLookAt(lightat);

	m_Light->SetRadius(125.0f);
}

void CLightManager::Uninit()
{
	if (m_Light != nullptr)
	{
		delete m_Light;
	}
}

void CLightManager::Update()
{
	float frame = m_pTimeM->GetTime().frame.z + m_pTimeM->GetTime().frame.y * 60.0f;
	XMFLOAT3 playerpos = m_pActorM->GetPlayer()->GetPos();
	float radius = m_Light->GetRadius();

	float m_Angle;
	m_Angle = 1.0f * frame;

	XMFLOAT4 lightp, lightd, lightat;

	XMVECTOR vlightd;

	lightp.x = -radius * cosf(XMConvertToRadians(m_Angle)) + playerpos.x;
	lightp.y =  radius * sinf(XMConvertToRadians(m_Angle));
	lightp.z =  playerpos.z;

	lightd.x =  - (lightp.x - playerpos.x);
	lightd.y =  - lightp.y;
	lightd.z =  0.0f;

	vlightd = XMLoadFloat4(&lightd);
	vlightd = XMVector3Normalize(vlightd);

	XMStoreFloat4(&lightd, vlightd);

	lightat.x = lightp.x + lightd.x;
	lightat.y = lightp.y + lightd.y;
	lightat.z = lightp.z + lightd.z;

	m_Light->SetPosition(lightp);
	m_Light->SetDirection(lightd);
	m_Light->SetLookAt(lightat);
}

void CLightManager::Draw(RenderMode mode)
{
	static float lightfar = m_Light->GetFar();
	static float lightr = m_Light->GetRadius();
	switch (mode)
	{
	case SHADOW_MODE:
		m_Light->GenerateViewMatrix();
		m_Light->GenerateOrthoMatrix(250.0f);
		m_Light->GenerateProjectionMatrix();
		break;

	case NORMAL_MODE:
		CRenderer::SetLight(m_Light);

		break;
	case IMGUI_MODE:
		if (ImGui::BeginTabItem("light")) {
			ImGui::Text("Dir = %.2f, %.2f, %.2f", m_Light->GetDirection().x, m_Light->GetDirection().y, m_Light->GetDirection().z);
			ImGui::Text("Pos = %.2f, %.2f, %.2f", m_Light->GetPosition().x, m_Light->GetPosition().y, m_Light->GetPosition().z);
			ImGui::Text("LookAt = %.2f, %.2f, %.2f", m_Light->GetLookAt().x, m_Light->GetLookAt().y, m_Light->GetLookAt().z);
			ImGui::SliderFloat("LightFar", &lightfar, 10.0f, 250.0f);
			ImGui::SliderFloat("LightRadius", &lightr, 1.0f, 250.0f);
			ImGui::EndTabItem();

			m_Light->SetFar(lightfar);
			m_Light->SetRadius(lightr);
		}
		break;
	}
}