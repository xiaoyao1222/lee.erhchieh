#pragma once

#include "polygon.h"

class CDepthMapUI : public CPolygon
{
private:
	//頂点バッファ
	ID3D11Buffer* m_VertexBuffer = NULL;

	bool m_debugdraw = true;

public:
	CDepthMapUI() {}
	CDepthMapUI(float dx, float dy, float width, float height);
	~CDepthMapUI() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;

	void Draw() override;
	void Draw(CShader* s) override;
};
