//=================================================================
//制作者　李爾捷
//タイトルUI管理クラス（CTitleUIManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "polygon.h"

class CTextureManager;
class CShaderManager;

class CTexture;
class CShader;

class CTitleUIManager : public CGameObject
{
private:
	CPolygon* m_pBackGround;
	CPolygon* m_pUI;

	std::list<CTexture*> m_pBackGroundTextures;
	std::list<CTexture*> m_pWordTextures;

	CShader* m_pBackGroundShader;
	CShader* m_pUIShader;
public:
	CTitleUIManager():
		m_pBackGround(nullptr), m_pUI(nullptr), m_pBackGroundShader(nullptr), m_pUIShader(nullptr)
	{
		m_pBackGroundTextures.clear();
		m_pWordTextures.clear();
	}
	~CTitleUIManager() { Uninit(); }

	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CTextureManager* m);
	void Load(CShaderManager* m);
};
