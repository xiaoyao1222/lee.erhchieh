//=================================================================
//制作者　李爾捷
//山ファクトリークラス（CFactoryMountain）
//=================================================================

#pragma once

#include "factory.h"

class CFactoryMountain : public CFactory
{
public:
	CFactoryMountain();
	~CFactoryMountain() {}

	void LoadAssert() override;
	void BuildGameObject() override;

	void SetManager() override;
};
