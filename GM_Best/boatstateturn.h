//=================================================================
//制作者　李爾捷
//船の回転ステートクラス（CBoatStateTurn）
//=================================================================

#pragma once

#include "boatstate.h"

class CBoat;
class CWayPoint;

class CBoatStateTurn : public CBoatState
{
private:
	CBoatStateTurn();

	std::list<CWayPoint*> m_pPoints;

	XMFLOAT3 m_pTargetFront;
	XMFLOAT3 m_pStartRot;

	float m_Time;
	float m_TurnSpeed;
	float m_AngleR;
	float m_AngleSlerp;

public:
	CBoatStateTurn(CBoat* boat, std::list<CWayPoint*>* p);
	~CBoatStateTurn() { m_pPoints.clear(); }

	void Update(CBoat* boat) override;
	void Draw() override;
};
