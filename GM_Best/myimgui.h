#pragma once

#include "imgui\imgui.h"
#include "imgui\imgui_impl_win32.h"
#include "imgui\imgui_impl_dx11.h"

class CMyImGui
{

public:

	static void Init(HWND hWnd);
	static void Uninit();
	static void Update();
	static void Draw();

};
