//=================================================================
//制作者　李爾捷
//プレイヤーアクタークラス（CPlayer）
//=================================================================

#pragma once

#include "actor.h"

class CActorManager;

class CPlayer : public CActor
{
private:

	float	 m_Speed;
	XMFLOAT3 m_Front;

	bool	 m_FlyingMode;
	bool	 m_DebugMoveRelease;

	void Move(XMFLOAT3 dir, float speed);

public:
	CPlayer() {};
	CPlayer(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~CPlayer() { Uninit(); }

	void Init(CGameObject* m) override;
	void Uninit() override;
	void Update() override;
	void Draw() override;

	XMFLOAT3 GetFrontVector() { return m_Front; }
};
