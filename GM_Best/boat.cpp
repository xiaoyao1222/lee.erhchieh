//=================================================================
//制作者　李爾捷
//船アクタークラス（CBoat）
//１.クォータニオンを使用
//２.ステートパターンを使用
//３.自動移動型AI（A*）
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"
#include "boat.h"

#include "shader.h"
#include "model.h"
#include "actormanager.h"
#include "fieldmanager.h"
#include "boatstate.h"
#include "boatstatesearch.h"
#include "waypoint.h"

CBoat::CBoat(int id, CActorManager* m)
	: m_ID(id), m_Stop(false), m_Front(XMFLOAT3(-1.0f, 0.0f, 0.0f)), m_Speed(0.05f), m_pBoatState(new CBoatStateNone())
{
	m_pActorM = m;
	m_Transform.Rotation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	m_Transform.Scale = XMFLOAT3(0.005f, 0.005f, 0.005f);
}

void CBoat::Init()
{
	CWayPoint* point = m_pActorM->GetFieldM()->GetWayPoint(m_ID * 10);
	SetPos(point->GetPos());
	ChangeState(new CBoatStateSearch(this, point));
}

void CBoat::Uninit()
{
	delete m_pBoatState;
}

void CBoat::Update()
{
	if (!m_Stop)
	{
		m_pBoatState->Update(this);
	}
}

void CBoat::Draw()
{
	//船のImgui
	if (ImGui::BeginTabItem("Boat")) {
		ImGui::Checkbox("StopMoving", &m_Stop);
		ImGui::Text("Pos = %.2f, %.2f, %.2f", m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);
		ImGui::Text("Rot = %.2f, %.2f, %.2f", m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
		ImGui::Text("Scale = %.2f, %.2f, %.2f", m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
		ImGui::Text("Front = %.2f, %.2f, %.2f", m_Front.x, m_Front.y, m_Front.z);
		ImGui::SliderFloat("Speed", &m_Speed, 0.01f, 1.0f);
		m_pBoatState->Draw();
		ImGui::EndTabItem();
	}
}

void CBoat::Draw(CShader* s, CModel* m)
{
	// マトリクス設定
	XMMATRIX world;
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world *= XMMatrixTranslation(m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);

	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	CRenderer::SetDepthTexture(1);

	m->Draw();
}

void CBoat::Draw(CShader* s, std::list<CTexture*> t, CModel* m)
{
	// マトリクス設定
	XMMATRIX world;
	XMVECTOR q = XMQuaternionRotationRollPitchYaw(m_Transform.Rotation.x, m_Transform.Rotation.y, m_Transform.Rotation.z);
	world = XMMatrixScaling(m_Transform.Scale.x, m_Transform.Scale.y, m_Transform.Scale.z);
	world *= XMMatrixRotationQuaternion(q);
	world *= XMMatrixTranslation(m_Transform.Position.x, m_Transform.Position.y, m_Transform.Position.z);

	XMFLOAT4X4 world_f;
	XMStoreFloat4x4(&world_f, world);


	CRenderer::SetWorldMatrix(&world_f);
	s->Set();

	int texcnt = 1;

	for (auto tex : t)
	{
		CRenderer::SetTexture(tex, texcnt);
		texcnt++;
	}

	CRenderer::SetDepthTexture(texcnt);

	m->Draw();
}

void CBoat::ChangeState(CBoatState* state)
{
	delete m_pBoatState;
	m_pBoatState = state;
}