#pragma once

#include "Scene.h"

class CLoading;
class CLoadingWord;

class CLoadingScene : public CScene
{
private:
	CLoading* m_pLoading;
	CLoadingWord* m_pLoadingWord;
	
public:
	void Init();
	void Uninit();
	void Update();
	void Draw();
};