//=================================================================
//制作者　李爾捷
//船の移動ステートクラス（CBoatStateGo）
//=================================================================

#pragma once

#include "boatstate.h"

class CBoat;
class CWayPoint;

class CBoatStateGo : public CBoatState
{
private:
	CBoatStateGo();

	std::list<CWayPoint*> m_pPoints;

public:
	CBoatStateGo(CBoat* boat, std::list<CWayPoint*>* p);
	~CBoatStateGo() { m_pPoints.clear(); }

	void Update(CBoat* boat) override;
	void Draw() override;
};
