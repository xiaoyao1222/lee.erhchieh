//*****************************************************************************
// 定数バッファ
//*****************************************************************************

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

// マテリアルバッファ
struct MATERIAL
{
	float4		Ambient;
	float4		Diffuse;
	float4		Specular;
	float4		Emission;
	float		Shininess;
	float3		Dummy;//16bit境界用
};

struct CAMERA
{
	float4 pos;
	float4 front;
};


// マトリクスバッファ
cbuffer ConstantBuffer : register(b0)
{
	matrix World;
	matrix View;
	matrix Projection;

	LIGHT		Light;

	MATERIAL	Material;

	CAMERA		Camera;
}

//*****************************************************************************
// グローバル変数
//*****************************************************************************
Texture2D		g_Texture_Color1  : register(t0);

Texture2D		g_ShadowMap		  : register(t2);

SamplerState	g_SamplerState    : register(s0);

SamplerState	g_ShadowMapState  : register(s1);

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 posW	: POSITION1;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;

	float4 lightViewPos : TEXCOORD1;
};

float when_eq(float x, float y)
{
	return 1.0f - abs(sign(x - y));
}

float when_gt(float x, float y)
{
	return max(sign(x - y), 0.0f);
}

float and(float x, float y)
{
	return x * y;
}

float4 shadowcolor(float3 normal, float4 lightpos)
{
	float4 color = Light.Ambient;

	float lightIntensity, depthValue, lightDepthValue, bias;
	float2 depthTexCoord;
	float3 lightDir;

	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);

	// Calculate the amount of light on this pixel.
	lightIntensity = saturate(dot(normal, lightDir));

	// Set the bias value for fixing the floating point precision issues.
	bias = 0.001f;

	// 深度テクスチャのUV計算
	depthTexCoord.x = 0.5f + (lightpos.x / lightpos.w * 0.5f);
	depthTexCoord.y = 0.5f - (lightpos.y / lightpos.w * 0.5f);

	float and_out = and(when_eq(saturate(depthTexCoord.x), depthTexCoord.x), when_eq(saturate(depthTexCoord.y), depthTexCoord.y));

	// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.

	// 深度テクスチャのロード
	depthValue = g_ShadowMap.Sample(g_ShadowMapState, depthTexCoord).r;

	// ライトからの深度値計算
	lightDepthValue = lightpos.z / lightpos.w;

	// Subtract the bias from the lightDepthValue.
	lightDepthValue = lightDepthValue - bias;


	// ライトからの深度値と深度テクスチャ記録した深度値を比較、色を追加する
	color += (Light.Diffuse * lightIntensity) * when_gt(lightIntensity, 0) * when_gt(depthValue, lightDepthValue) * and_out;



	color = saturate(color);

	return color;

}

float4 specularcolor(float3 normal, float4 pos)
{
	float3 lightDir;
	// Invert the light direction for calculations.
	lightDir = -Light.Direction;

	lightDir = normalize(lightDir);
	float light = (dot(normalize(lightDir), normal.xyz));

	float3 ref = reflect(-lightDir, normal.xyz);
	float3 toEye = Camera.pos.xyz - pos;
	ref = normalize(ref);
	toEye = normalize(toEye);

	float s = dot(ref, toEye);
	s = saturate(s);
	s = pow(s, 2);
	return float4(s, s, s, 1.0f);
}
//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float4 blendcolor, tex, light, shadow, specular;

tex = g_Texture_Color1.Sample(g_SamplerState, input.tex);

light = input.diffuse * (0.5 - 0.5 * dot(Light.Direction.xyz, input.normal.xyz));
light.w = 1.0f;

shadow = shadowcolor(input.normal, input.lightViewPos);

specular = specularcolor(input.normal.xyz, input.posW);

blendcolor = saturate(tex * light * shadow + specular);

return blendcolor;
}
