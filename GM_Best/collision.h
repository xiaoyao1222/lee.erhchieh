#pragma once

#define _OX_EPSILON_	0.000001f	// 誤差
#define CIRCLE_VERTEX_COUNT (8)		//八角形
#define CIRCLE_DRAW_MAX (2048)		//丸の最大数

//const→アドレス先を書き込み防止策（見るだけ）

class CCollisionLine;
class CCollisionSphere;
class CCollisionCube;
class CCollisionCapsule;

class CCollision
{
	//当たり判定関数
	//
	//引数	:pA　→　物Aのアドレス
	//		:pB	 →　物Bのアドレス
	//
	//戻り値:成功／失敗
	//

protected:
	XMFLOAT3 m_Position;
	XMFLOAT3 m_Rotation;
	XMFLOAT3 m_Scale;

	//頂点バッファ
	ID3D11Buffer* m_VertexBuffer = NULL;
	//インデクスバッファ
	ID3D11Buffer* m_IndexBuffer = NULL;

	void clamp01(float &v);

	float calcPointLineDist3D(XMVECTOR &p, CCollisionLine &l, XMVECTOR &h, float &t);

	float calcPointSegmentDist3D(XMVECTOR &p, CCollisionLine &seg, XMVECTOR &h, float &t);

	float calcLineLineDist3D(CCollisionLine &l1, CCollisionLine &l2, XMVECTOR &p1, XMVECTOR &p2, float &t1, float &t2);

	float calcSegmentSegmentDist3D(CCollisionLine &s1, CCollisionLine &s2, XMVECTOR &p1, XMVECTOR &p2, float &t1, float &t2);

public:

	virtual void Init() = 0;
	virtual void Draw() = 0;
	virtual void SetPos(XMFLOAT3 pos) = 0;
	virtual void SetRot(XMFLOAT3 Rot) = 0;

	virtual bool IsHit(CCollisionSphere* pB) = 0;

	virtual bool IsHit(CCollisionCube* pB) = 0;

	virtual bool IsHit(CCollisionCapsule* pB) = 0;

	template<typename Type>
	bool Hit(Type* pB)
	{
		return IsHit(pB);
	}
};

class CCollisionCube : public CCollision
{
private:
	XMVECTOR m_centerpos;
	XMFLOAT3 m_radius;

public:
	CCollisionCube(XMVECTOR centerpos, XMFLOAT3 radius) : m_centerpos(centerpos), m_radius(radius) {}

	void Init() override;
	void Draw() override;
	XMVECTOR GetCenterPos() { return m_centerpos; }
	XMFLOAT3 GetRadius() { return m_radius; }

	void SetPos(XMFLOAT3 pos) override { m_Position = pos; }
	void SetRot(XMFLOAT3 Rot) override { m_Rotation = Rot; }
};

class CCollisionLine : public CCollision
{
private:
	XMVECTOR m_pos;
	XMVECTOR m_vec;

public:
	CCollisionLine(XMVECTOR pos, XMVECTOR vec) : m_pos(pos), m_vec(vec) {}

	void Init() override;
	void Draw() override;
	XMVECTOR GetPos() { return m_pos; }
	XMVECTOR GetVec() { return m_vec; }

	void SetPos(XMFLOAT3 pos) override { m_Position = pos; }
	void SetRot(XMFLOAT3 Rot) override { m_Rotation = Rot; }

	bool IsHit(CCollisionSphere* pB) { return NULL; }

	bool IsHit(CCollisionCube* pB) { return NULL; }

	bool IsHit(CCollisionCapsule* pB) { return NULL; }
};

class CCollisionSphere : public CCollision
{
private:
	XMVECTOR m_centerpos;
	float m_radius;

public:
	CCollisionSphere(XMVECTOR centerpos, float radius) : m_centerpos(centerpos), m_radius(radius) {}

	void Init() override;
	void Draw() override;
	XMVECTOR GetCenterPos() { return m_centerpos; }
	float GetRadius() { return m_radius; }

	void SetPos(XMFLOAT3 pos) override {
		m_Position = pos;
		m_centerpos = XMLoadFloat3(&pos);
	}
	void SetRot(XMFLOAT3 Rot) override { m_Rotation = Rot; }

	bool IsHit(CCollisionSphere* pB);

	bool IsHit(CCollisionCube* pB);

	bool IsHit(CCollisionCapsule* pB) { return NULL; }
};

class CCollisionCapsule : public CCollision
{
private:
	CCollisionLine* m_line;
	float m_radius;

public:
	CCollisionCapsule(CCollisionLine* line, float radius) {
		m_line = new CCollisionLine(line->GetPos(), line->GetVec());
		m_radius = radius;
	}

	void Init() override;
	void Draw() override;
	CCollisionLine* GetLine() { return m_line; }
	float GetRadius() { return m_radius; }

	void SetPos(XMFLOAT3 pos) override { m_Position = pos; }
	void SetRot(XMFLOAT3 Rot) override { m_Rotation = Rot; }

	bool IsHit(CCollisionSphere* pB);

	bool IsHit(CCollisionCapsule* pB);

	bool IsHit(CCollisionCube* pB) { return NULL; }
};