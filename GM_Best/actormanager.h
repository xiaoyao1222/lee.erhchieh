//=================================================================
//制作者　李爾捷
//アクター管理クラス（CActorManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "actor.h"

#define MAX_BOAT 10

class CModelManager;
class CTextureManager;
class CShaderManager;
class CFieldManager;
class CCameraManager;

class CModel;
class CTexture;
class CShader;

class CPlayer;

class CActorManager : public CGameObject
{
public:
	enum ACTOR_TYPE
	{
		NORMAL = 0,
		MOUNTAIN,
		DESERT
	};
private:
	CActor*				m_pPlayer;
	std::list<CActor*>	m_pBoats;

	CModel* m_pPlayerModel;
	CModel* m_pBoatModel;

	std::list<CTexture*> m_pEnemyTextures;
	std::list<CTexture*> m_pPlayerTextures;

	CShader* m_pPlayerShader;
	CShader* m_pBoatShader;
	CShader* m_pShadowShader;

	CFieldManager*	m_pFieldM;
	CCameraManager* m_pCamM;

public:
	CActorManager() :
		m_pPlayer(nullptr), m_pPlayerModel(nullptr), m_pBoatModel(nullptr), m_pPlayerShader(nullptr),
		m_pBoatShader(nullptr), m_pShadowShader(nullptr), m_pFieldM(nullptr), m_pCamM(nullptr)
	{
		m_pEnemyTextures.clear();
		m_pPlayerTextures.clear();
	}
	~CActorManager() { Uninit(); }

	void Build(ACTOR_TYPE type = NORMAL, XMFLOAT3 player_pos = XMFLOAT3(0.0f, 0.0f, 0.0f));
	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CModelManager* m);
	void Load(CTextureManager* m);
	void Load(CShaderManager* m);
	void Load(CFieldManager* m)		{ m_pFieldM = m; }
	void Load(CCameraManager* m)	{ m_pCamM = m; }

	CPlayer* GetPlayer()		{ return (CPlayer*)m_pPlayer; }
	CCameraManager* GetCamM()	{ return m_pCamM; }
	CFieldManager* GetFieldM()	{ return m_pFieldM; }
};
