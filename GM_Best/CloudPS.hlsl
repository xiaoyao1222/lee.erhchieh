//*****************************************************************************
// 定数バッファ
//*****************************************************************************
// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

//カメラバッファ
struct CAMERA
{
	float4 pos;
	float4 front;
};

cbuffer CameraBuffer : register(b5)
{
	CAMERA Camera;
}

//タイムバッファ
struct TIME
{
	float4 frame;
};

cbuffer TimeBuffer : register(b7)
{
	TIME Time;
}

SamplerState	g_SamplerState : register(s0);

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex : TEXCOORD0;

	float4 posL: TEXCOORD1;
};

#define depthmax 75

float hash(float n)
{
	return frac(sin(n)*43758.5454);
}

float noise(float3 x)
{
	float3 p = floor(x);
	float3 f = frac(x);

	f = f*f*(3.0 - 2.0*f);
	float n = p.x + p.y*57.0f + 113.0*p.z;
	return lerp(lerp(lerp(hash(n + 0.0f), hash(n + 1.0f), f.x),
					lerp(hash(n + 57.0f), hash(n + 58.0f), f.x), f.y),
				lerp(lerp(hash(n + 113.0f), hash(n + 114.0f), f.x),
					lerp(hash(n + 170.0f), hash(n + 171.0f), f.x), f.y), f.z);
}

//非整数ブラウン運動
float fbm(float3 p)
{
	float3x3 m = float3x3(0.00f, 1.60f, 1.20f, -1.60f, 0.72f, -0.96f, -1.20f, -0.96f, 1.28f);
	float f;
	f = 0.5f * noise(p);
	p = mul(m , p);
	f += 0.25f * noise(p);
	p = mul(m , p);
	f += 0.1666f * noise(p);
	p = mul(m , p);
	f += 0.0834f * noise(p);

	return f;
}

//カメラの位置調整
float3 camera(float3 pos, float time)
{
	return float3(pos.x + 15 * time, pos.y, pos.z * time);
}

#define pi 3.14159265359
#define d0(x) abs(x) + 1e-8

float3 scatter(float3 coeff, float depth) {
	return coeff * depth;
}

float3 absorb(float3 coeff, float depth) {
	return exp2(scatter(coeff, -depth));
}

float calculateSceneDepth(float depth) {
	depth = depth * 2.0 - 0.1;
	depth = max(depth + 0.01, 0.01);

	return depth * depth * (3.0 - 2.0 * depth);
}

float calcParticleThickness(float depth) {

	depth = calculateSceneDepth(depth);
	depth = 1.0 / depth;

	return 50000.0 * depth;
}

float rayleighPhase(float2 p, float2 lp) {
	return 0.375 * (1.0 + pow(1.0 - distance(p, lp), 2.0) / log(2.0));
}

float3 calcAtmosphericScatter(float2 p, float2 lp, float4 cloudcolor, float4 starcolor) {
	const float ln2 = log(2.0);

	const float up = 0.6;

	const float3 rayleighCoeff = float3(0.27, 0.5, 1.0) * 1e-5;
	const float mieCoeff = 1e-6;

	float3 totalCoeff = rayleighCoeff + mieCoeff;

	float opticalDepth = calcParticleThickness(p.y);
	float opticalDepthLight = calcParticleThickness(lp.y);

	float3 scatterView = scatter(totalCoeff, opticalDepth);
	float3 absorbView = absorb(totalCoeff, opticalDepth);

	float3 scatterLight = scatter(totalCoeff, opticalDepthLight);
	float3 absorbLight = absorb(totalCoeff, opticalDepthLight);

	float3 absorbSun = abs(absorbLight - absorbView) / d0((scatterLight - scatterView) * ln2);
	float3 scatterSun = scatterView * rayleighPhase(p, lp);

	float3 color = (scatterSun * absorbSun);
	float3 cloud_and_star = cloudcolor.xyz * absorbLight + starcolor.xyz * (1 - absorbLight);

	color = lerp(color, cloud_and_star, cloudcolor.a);

	return color;
}

float3 jodieReinhardTonemap(float3 c) {
	float l = dot(c, float3(0.2126, 0.7152, 0.0722));
	float3 tc = c / (c + 1.0);

	return lerp(c / (l + 1.0), tc, tc);
}

float Noise2d(float2 x)
{
	float xhash = cos(x.x * 37.0);
	float yhash = cos(x.y * 57.0);
	return frac(415.92653 * (xhash + yhash));
}

float NoisyStarField(float2 pos, float fThreshhold)
{
	float StarVal = Noise2d(pos);
	if (StarVal >= fThreshhold)
		StarVal = pow((StarVal - fThreshhold) / (1.0 - fThreshhold), 15.0);
	else
		StarVal = 0.0;
	return StarVal;
}

float StableStarField(float2 pos, float fThreshhold)
{
	// Linear interpolation between four samples.
	// Note: This approach has some visual artifacts.
	// There must be a better way to "anti alias" the star field.
	float fractX = frac(pos.x);
	float fractY = frac(pos.y);
	float2 floorSample = floor(pos);
	float v1 = NoisyStarField(floorSample, fThreshhold);
	float v2 = NoisyStarField(floorSample + float2(0.0, 1.0), fThreshhold);
	float v3 = NoisyStarField(floorSample + float2(1.0, 0.0), fThreshhold);
	float v4 = NoisyStarField(floorSample + float2(1.0, 1.0), fThreshhold);

	float StarVal = v1 * (1.0 - fractX) * (1.0 - fractY)
		+ v2 * (1.0 - fractX) * fractY
		+ v3 * fractX * (1.0 - fractY)
		+ v4 * fractX * fractY;

	return StarVal;
}

//=============================================================================
// ピクセルシェーダ
//=============================================================================
float4 main(in  PixelInputType input) : SV_Target
{
	float frame = Time.frame.z + Time.frame.y * 60;

	float time = (Time.frame.w + 13.5 + 44.) * 1.5;

	//float3 skytop = float3(0.05, 0.2, 0.5);
	float3 camtar = Camera.front.xyz + float3(0.0f, 0.1f, 0.07f);
	float3 campos = camera(Camera.pos.xyz, time);

	//カメラの前、右、上と注視点を取得
	float3 front = normalize(camtar);
	float3 right = normalize(cross(front, float3(0.0, 1.0, 0.0)));
	float3 up = normalize(cross(right, front));
	float3 fragAt = normalize(input.tex.x * right + (1.0f-input.tex.y) * up + front);

	// 雲作成
	//
	float4 sum = float4(0.0f, 0.0f, 0.0f, 0.0f);
	float4 starsum = float4(0.0f, 0.0f, 0.0f, 0.0f);

	for (float depth = 0.0; depth < depthmax; depth += 5)
	{
		float3 ray = campos + fragAt * depth * 750;

		float hei = saturate(ray.y + Camera.pos.y * (depth* 5.0f));

		float alpha = smoothstep(0.6f + (1.0f - hei)*0.4f, 1.0f, fbm(ray * 0.001f));

		float3 localcolor = lerp(float3(1.0f, 0.95f, 0.9f), float3(0.0, 0.0, 0.0), alpha);

		alpha = (1.0f - sum.a) * alpha;
		sum += float4(localcolor * alpha, alpha);
	}
	
	//ここから光源と合成
	float alpha = smoothstep(0.3, 1.0, sum.a);
	sum.rgb /= sum.a + 0.0001;

	float sundot = clamp(dot(fragAt, Light.Ambient), 0.0, 1.0);

	sum.rgb -= 0.6 * float3(0.8, 0.75, 0.7) * pow(sundot, 13.0) * alpha;

	sum.rgb += 0.2 * float3(1.3, 1.2, 1.0) * pow(sundot, 5.0) * (1.0 - alpha);

	float4 starcolor;
	//star color
	{
		float StarFieldThreshhold = 0.99;

		// Stars with a slow crawl.
		float2 starPos = input.posL;
		float StarVal = StableStarField(starPos, StarFieldThreshhold);

		starcolor = float4(StarVal, StarVal, StarVal, 1.0f);
	}

	//sun pos
	float2 uv = float2(input.tex.x, 1 - input.tex.y - 0.23f);
	float2 lp = float2(clamp(cos(frame * 0.01f), 0.01f, 0.8f), clamp(sin((frame * 1.0f)* (pi / 180.0f)), 0.01f, 0.6f));

	float3 skyColor = float3(0.0, 0.0f, 0.0f);
	skyColor = calcAtmosphericScatter(uv, lp, sum, starcolor);

	skyColor = jodieReinhardTonemap(skyColor);

	skyColor = pow(skyColor, float3(0.4545, 0.4545, 0.4545));

	return float4(skyColor, 1.0f);
}
