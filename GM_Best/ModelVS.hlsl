//*****************************************************************************
// 定数バッファ
//*****************************************************************************
// マトリクスバッファ
cbuffer WorldBuffer : register(b0)
{
	matrix World;
}
cbuffer ViewBuffer : register(b1)
{
	matrix View;
}
cbuffer ProjectionBuffer : register(b2)
{
	matrix Projection;
}

// ライトバッファ
struct LIGHT
{
	float3		Direction;
	float4		Diffuse;
	float4		Ambient;

	float3	Position;
	float3	LookAt;
	float4	Near_Far;

	matrix	ViewMatrix;
	matrix	OrthoMatrix;
	matrix	ProjectionMaterix;
};

cbuffer LightBuffer : register(b3)
{
	LIGHT Light;
}

// マテリアルバッファ
struct MATERIAL
{
	float4		Ambient;
	float4		Diffuse;
	float4		Specular;
	float4		Emission;
	float		Shininess;
	float3		Dummy;//16bit境界用
};

cbuffer MaterialBuffer : register(b4)
{
	MATERIAL	Material;
}

//*****************************************************************************
// インプット構造体
//*****************************************************************************
struct VertexInputType
{
	float4 pos : POSITION;
	float4 normal : NORMAL0;
	float4 diffuse	: COLOR0;
	float2 tex		: TEXCOORD0;
};

//*****************************************************************************
// アウトプット構造体
//*****************************************************************************
struct PixelInputType
{
	float4 pos : SV_POSITION;
	float4 posW	: POSITION1;
	float4 normal : NORMAL0;
	float2 tex : TEXCOORD0;
	float4 diffuse	: COLOR0;

	float4 lightViewPos : TEXCOORD1;

};

//=============================================================================
// 頂点シェーダ
//=============================================================================
PixelInputType main(in VertexInputType input)
{
	PixelInputType output;

	//pos
	{
		matrix wvp, lightwvp;
		wvp = mul(World, View);
		wvp = mul(wvp, Projection);

		lightwvp = mul(World, Light.ViewMatrix);
		lightwvp = mul(lightwvp, Light.OrthoMatrix);

		output.pos = mul(input.pos, wvp);
		output.posW = mul(input.pos, World);
		output.lightViewPos = mul(input.pos, lightwvp);
	}

	//UV
	{
		output.tex = input.tex;
	}

	//Normal
	{
		float4 worldNormal, normal;
		normal = float4(input.normal.xyz, 0.0);
		worldNormal = mul(normal, World);
		worldNormal = normalize(worldNormal);
		output.normal = worldNormal;
	}

	//diffuse
	{
		//float light = 0.5 - 0.5 * dot(Light.Direction.xyz, worldNormal.xyz);
		output.diffuse = input.diffuse * Material.Diffuse * Light.Diffuse;
		output.diffuse += input.diffuse * Material.Ambient * Light.Ambient;
		output.diffuse.a = input.diffuse.a * Material.Diffuse.a;
	}

	return output;

}
