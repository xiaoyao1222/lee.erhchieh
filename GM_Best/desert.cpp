#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"

#include "desert.h"

#include "factorydesert.h"

#include "modelmanager.h"
#include "texturemanager.h"
#include "shadermanager.h"

#include "timemanager.h"
#include "cameramanager.h"
#include "lightmanager.h"
#include "skymanager.h"
#include "fieldmanager.h"
#include "fogmanager.h"
#include "actormanager.h"
#include "uimanager.h"

#include "manager.h"
#include "title.h"
#include "island.h"
#include "mountain.h"
#include "dungeon.h"
#include "input.h"

void CDesert::Init()
{
	CFactory* f = new CFactoryDesert();

	m_pModelM = AddGameObject<CModelManager>(Layer_System);
	m_pTexM = AddGameObject<CTextureManager>(Layer_System);
	m_pShaderM = AddGameObject<CShaderManager>(Layer_System);
	m_pTimeM = AddGameObject<CTimeManager>(Layer_System);

	m_pCamM = AddGameObject<CCameraManager>(Layer_Camera);

	m_pLightM = AddGameObject<CLightManager>(Layer_Field);
	m_pFogM = AddGameObject<CFogManager>(Layer_Field);
	m_pSkyM = AddGameObject<CSkyManager>(Layer_Field);
	m_pFieldM = AddGameObject<CFieldManager>(Layer_Field);

	m_pActorM = AddGameObject<CActorManager>(Layer_Actor);

	m_pUIM = AddGameObject<CUIManager>(Layer_UI);

	f->SetObject(m_pModelM);
	f->SetObject(m_pTexM);
	f->SetObject(m_pShaderM);

	f->SetObject(m_pTimeM);
	f->SetObject(m_pCamM);
	f->SetObject(m_pLightM);
	f->SetObject(m_pSkyM);
	f->SetObject(m_pFieldM);
	f->SetObject(m_pFogM);
	f->SetObject(m_pActorM);
	f->SetObject(m_pUIM);

	f->BuildGameObject();
	f->LoadAssert();
	f->SetManager();

	delete f;

	CManager::SetLockFlag(true);
}

void CDesert::Uninit()
{
	CScene::Uninit();
}

void CDesert::Update()
{
	CScene::Update();

	if (CInput::GetKeyTrigger('P'))
	{
		std::thread T1(CManager::SetScene<CIsland>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('O'))
	{
		std::thread T1(CManager::SetScene<CTitle>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('I'))
	{
		std::thread T1(CManager::SetScene<CMountain>);

		T1.detach();
		return;
	}

	if (CInput::GetKeyTrigger('U'))
	{
		std::thread T1(CManager::SetScene<CDungeon>);

		T1.detach();
		return;
	}

}

void CDesert::Draw()
{
	ImGui::SetNextWindowSize(ImVec2(200, 200), ImGuiCond_Always);
	ImGui::Begin("Desert");
	ImGui::Text("Welcome to Desert");

	ImGui::BeginTabBar("Object");
	if (ImGui::BeginTabItem("Time")) {
		ImGui::Text("Time %.2f : %.2f : %.2f", m_pTimeM->GetTime().frame.x, m_pTimeM->GetTime().frame.y, m_pTimeM->GetTime().frame.z);
		ImGui::Text("FrameCnt = %.2f", m_pTimeM->GetTime().frame.w);
		ImGui::ProgressBar((m_pTimeM->GetTime().frame.z + m_pTimeM->GetTime().frame.y * 60) / MAX_TIME, ImVec2(0.0f, 0.0f));
		if (ImGui::Button("Time Freeze")) {
			m_pTimeM->SwitchFreezeCheck();
		}
		ImGui::EndTabItem();
	}
	ImGui::EndTabBar();
	ImGui::End();

	CScene::Draw();

}
