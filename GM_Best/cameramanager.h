//=================================================================
//制作者　李爾捷
//カメラ管理クラス（CCameraManager）
//=================================================================

#pragma once

#include "gameobject.h"
#include "camera.h"

class CActorManager;

class CCameraManager : public CGameObject
{
private:
	CCamera*		m_pCam;
	CActorManager*	m_pActorM;

public:
	CCameraManager():
		m_pCam(nullptr),m_pActorM(nullptr){}
	~CCameraManager(){ Uninit(); }

	void Init();
	void Uninit();
	void Update();
	void Draw(RenderMode mode);

	void Load(CActorManager* m) { m_pActorM = m; }

	CCamera*		GetCamera() { return m_pCam; }
	CActorManager*	GetActorM() { return m_pActorM; }
};
