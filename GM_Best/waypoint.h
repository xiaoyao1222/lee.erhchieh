//=================================================================
//制作者　李爾捷
//ウェイポイントクラス（CWayPoint）
//=================================================================

#pragma once

#define POINT_RADIUS 100.0f

class CFieldManager;
class CShader;
class CModel;

enum WAYPOINT_STATUS
{
	NONE,
	OPEN,
	CLOSED,
};

class CWayPoint
{
private:
	WAYPOINT_STATUS m_Status;
	int				m_ID;
	XMFLOAT3		m_Position;
	float			m_Distance;
	float			m_Heuristic;

	std::list<CWayPoint*> m_LinkPoints;

	//スタートまだはエンド地点の距離
	float CalculateDistance(XMFLOAT3 pos);

public:
	CWayPoint() {}
	CWayPoint(XMFLOAT3 pos, int id)
		: m_Status(NONE), m_ID(id), m_Position(pos), m_Distance(0.0f), m_Heuristic(0.0f){}
	~CWayPoint() { m_LinkPoints.clear(); }

	void Draw(CShader* s, CModel* m);

	void  CheckLinkList(CFieldManager* m);



	bool SetStatus(WAYPOINT_STATUS s, CWayPoint* startp = nullptr, CWayPoint* goalp = nullptr, std::list<CWayPoint*>* path = nullptr);
	void SetID(int id)				{ m_ID = id; }
	void SetPos(XMFLOAT3 pos)		{ m_Position = pos; }
	void SetLinkPoint(CWayPoint* p) { m_LinkPoints.push_back(p); }


	XMFLOAT3				GetPos() const	{ return m_Position; }
	int						GetID() const	{ return m_ID; }
	std::list<CWayPoint*>*	GetLinkPoints() { return &m_LinkPoints; }
	WAYPOINT_STATUS			GetStatus()		{ return m_Status; }
	float					GetScore()		{ return m_Distance + m_Heuristic; }

	float GetDistance_sq(XMFLOAT3 pos)
	{
		float x = m_Position.x - pos.x;
		float z = m_Position.z - pos.z;

		return x * x + z * z;
	}
};
