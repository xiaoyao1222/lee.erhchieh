//=================================================================
//制作者　李爾捷
//ゲームオブジェクトテンプレクラス（CGameObject）
//各マネージャーだけ使ってる
//=================================================================

#pragma once

#include "renderer.h"

class CGameObject
{
public:

	CGameObject() {}
	virtual ~CGameObject() {}

	virtual void Init() {}
	virtual void Uninit() {}
	virtual void Update() {}
	virtual void Draw(RenderMode mode) {}
};
