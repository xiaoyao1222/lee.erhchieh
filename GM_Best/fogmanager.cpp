//=================================================================
//制作者　李爾捷
//フォグ管理クラス（CFogManager）
//タイムマネージャーから時間を取得して
//黒←→白、時間に応じて色を変化する
//
//課題：フォグの色と大気散乱シンクロする
//=================================================================

#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"
#include "fogmanager.h"
#include "timemanager.h"

void CFogManager::Init()
{
	SetFog(COLOR(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(0.4f, 1.0f, 0.1f, 200.0f), XMFLOAT4(7.5f, 0.8f, 0.0f, 0.0f));
}

void CFogManager::Uninit()
{

}

void CFogManager::Update()
{
	float frame = m_pTimeM->GetTime().frame.z + m_pTimeM->GetTime().frame.y * 60.0f;

	float color = 0.5f + 0.4f * sinf(XMConvertToRadians(frame));

	m_pFog.Color.r = color;
	m_pFog.Color.g = color;
	m_pFog.Color.b = color;
}

void CFogManager::Draw(RenderMode mode)
{
	switch (mode)
	{
	case SHADOW_MODE:


		break;
	case NORMAL_MODE:
		CRenderer::SetFog(m_pFog);

		break;
	case IMGUI_MODE:
		if (ImGui::BeginTabItem("fog"))
		{
			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.5f);

			ImGui::ColorEdit4("Color", &m_pFog.Color.r);

			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.2f);

			ImGui::SliderFloat("fogStart", &m_pFog.DistanceFog.x, 0.1f, 1.0f, "%.2f");

			ImGui::SameLine();

			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.2f);

			ImGui::SliderFloat("fogfar", &m_pFog.DistanceFog.w, 1.0f, 400.0f, "%.2f");

			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.2f);

			ImGui::SliderFloat("density", &m_pFog.HeightFog.x, 0.0f, 20.0f, "%.2f");

			ImGui::SameLine();

			ImGui::SetNextItemWidth(ImGui::GetWindowWidth() * 0.2f);

			ImGui::SliderFloat("densityAttenuation", &m_pFog.HeightFog.y, 0.01f, 2.0f, "%.2f");

			if (ImGui::Button("Reset")) {
				SetFog(COLOR(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(0.4f, 1.0f, 0.1f, 200.0f), XMFLOAT4(7.5f, 0.8f, 0.0f, 0.0f));
			}

			ImGui::SameLine();

			if (ImGui::Button("Close")) {
				SetFog(COLOR(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(1.0f, 1.0f, 0.1f, 400.0f), XMFLOAT4(0.0f, 2.0f, 0.0f, 0.0f));
			}
			ImGui::EndTabItem();

		break;
	}

	
	}

}

void CFogManager::SetFog(COLOR color, XMFLOAT4 disFog, XMFLOAT4 HeiFog)
{
	m_pFog.Color = color;
	m_pFog.DistanceFog = disFog;
	m_pFog.HeightFog = HeiFog;
}