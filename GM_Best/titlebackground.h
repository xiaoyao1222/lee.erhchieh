#pragma once

#include "polygon.h"

class CTitleBackGround : public CPolygon
{
private:
	float m_Flame;

	//頂点バッファ
	ID3D11Buffer* m_VertexBuffer = NULL;

public:
	CTitleBackGround() {}
	CTitleBackGround(float dx, float dy, float width, float height);
	~CTitleBackGround() { Uninit(); }

	void Init() override;
	void Uninit() override;
	void Update() override;

	void Draw(CShader* s, std::list<CTexture*> t) override;
};
