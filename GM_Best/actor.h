//=================================================================
//制作者　李爾捷
//アクターテンプレクラス（CActor）
//トランスフォーム持ち（座標、回転、大きさ）
//描画は複数パターン対応
//１.引数なし						→	アセット全部使わない、基本ImGuiまだはデバック用
//２.シェーダのみ					→	シェーダだけで描画できるオブジェクト使用
//３.シェーダ、モデル				→	シェーダとモデルで描画する時使用
//４.シェーダ、テクスチャ			→	シェーダとテクスチャで描画する時使用
//５.シェーダ、テクスチャ、モデル	→	アセット全使用の時
//=================================================================

#pragma once

#define VALUE_GRAVITY (0.49) //重力

class CModel;
class CTexture;
class CShader;
class CGameObject;
class CActorManager;

class CActor
{
public:
	CActor(){};
	virtual ~CActor() {};

	virtual void Init() {}
	virtual void Init(CGameObject* m) {}

	virtual void Uninit() {}
	virtual void Update() {}

	virtual void Draw() {}
	virtual void Draw(CShader* s) {}
	virtual void Draw(CShader* s, CModel* m) {}
	virtual void Draw(CShader* s, std::list<CTexture*> t) {}
	virtual void Draw(CShader* s, std::list<CTexture*> t, CModel* m) {}

	void SetTransform(Transform t) { m_Transform = t; }
	void SetPos(XMFLOAT3 p) { m_Transform.Position = p; }
	void SetRot(XMFLOAT3 r) { m_Transform.Rotation = r; }
	void SetScale(XMFLOAT3 s) { m_Transform.Scale = s; }

	Transform& GetTransform() { return m_Transform; }
	const XMFLOAT3& GetPos() const { return m_Transform.Position; }
	const XMFLOAT3& GetRot() const { return m_Transform.Rotation; }
	const XMFLOAT3& GetScale() const { return m_Transform.Scale; }


	void SetDestroy() { m_Destroy = true; }
	bool IsDestroy() { return m_Destroy; }
	bool Destroy() {
		if (m_Destroy)
		{
			delete this;
			return true;
		}
		else
		{
			return false;
		}
	}
protected:
	Transform m_Transform;
	CActorManager* m_pActorM;
	
	bool m_Destroy = false;

};
