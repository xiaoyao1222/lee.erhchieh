#include "main.h"
#include "renderer.h"
#include "imgui\imgui.h"
#include "input.h"
#include "manager.h"

#include "audio_clip.h"
#include "title.h"
#include "loadingscene.h"

CScene* CManager::m_Scene = nullptr;
CScene* CManager::m_pLoadingScene = nullptr;
bool CManager::isLoaded = false;
std::mutex CManager::isLoadedMutex;

bool CManager::GetLockFlag()
{
	std::lock_guard<std::mutex>  lock(isLoadedMutex);
	return isLoaded;
}

void CManager::Init()
{
	CRenderer::Init();

	CInput::Init();

	CAudioClip::Init();

	m_pLoadingScene = new CLoadingScene();
	m_pLoadingScene->Init();

	std::thread T1(SetScene<CTitle>);

	T1.detach();
}

void CManager::Uninit()
{
	if (m_Scene != nullptr)
	{
		m_Scene->Uninit();
		delete m_Scene;
	}

	if (m_pLoadingScene != nullptr)
	{
		delete m_pLoadingScene;
	}

	CRenderer::Uninit();

	CAudioClip::Uninit();
}

void CManager::Update()
{
	if (!GetLockFlag())
	{
		m_pLoadingScene->Update();
	}
	else
	{
		CInput::Update();

		m_Scene->Update();
	}
}

void CManager::Draw()
{

	CRenderer::Begin();

	if (!GetLockFlag())
	{
		ImGui::SetNextWindowSize(ImVec2(200, 100), ImGuiCond_Always);
		ImGui::Begin("LoadingScene");
		ImGui::Text("Now Loading~~~~");

		ImGui::End();

		m_pLoadingScene->Draw();
	}
	else
	{
		m_Scene->Draw();
	}

	CRenderer::End();

}

void CManager::SetLockFlag(bool _)
{
	std::lock_guard<std::mutex>  lock(isLoadedMutex);
	isLoaded = _;
}