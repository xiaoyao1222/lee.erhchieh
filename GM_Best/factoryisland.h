//=================================================================
//制作者　李爾捷
//島ファクトリークラス（CFactoryIsland）
//=================================================================

#pragma once

#include "factory.h"

class CFactoryIsland : public CFactory
{
public:
	CFactoryIsland();
	~CFactoryIsland(){}

	void LoadAssert() override;
	void BuildGameObject() override;

	void SetManager() override;
};
