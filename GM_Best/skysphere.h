//=================================================================
//制作者　李爾捷
//スカイスフィアクラス（CSkySphere）
//=================================================================

#pragma once

#include "fieldbase.h"

class CSkyManager;

class CSkySphere : public CFieldBase
{
private:

	ID3D11Buffer*	m_VertexBuffer = NULL;
	ID3D11Buffer*	m_IndexBuffer = NULL;

	int				m_Indexcnt = 0;
	float			m_Radius;
	XMFLOAT2		m_Angle;
	
	CSkyManager*	m_pSkyM;
public:
	CSkySphere(){}
	CSkySphere(float radius, XMFLOAT2 angle);
	~CSkySphere(){ Uninit(); }

	void Init(CGameObject* m) override;
	void Uninit() override;
	void Update() override;
	void Draw(CShader* s);
};
