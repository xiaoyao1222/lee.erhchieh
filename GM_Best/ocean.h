//=================================================================
//制作者　李爾捷
//海フィールドクラス（COcean）
//=================================================================

#pragma once

#define OCEAN_HEI 4.0f

#include "fieldbase.h"

class CFieldManager;
class CTexture;

class COcean : public CFieldBase
{
private:

	ID3D11Buffer*	 m_VertexBuffer = NULL;
	ID3D11Buffer*	 m_IndexBuffer = NULL;

	int				 m_Indexcnt;

	VERTEX_FIELD_3D* m_pV;

public:
	COcean() {}
	COcean(XMFLOAT3 pos, XMFLOAT3 rot, XMFLOAT3 scale);
	~COcean() { Uninit(); }

	void Init(int cnt_x, int cnt_z);
	void Uninit();
	void Update();
	void Draw(CShader* s);
	void Draw(CShader* s, std::list<CTexture*> t);
};