#pragma once

#include "Scene.h"

class CManager
{
private:
	static CScene* m_Scene;
	static CScene* m_pLoadingScene;

	static bool isLoaded;
	static std::mutex isLoadedMutex;

public:
	template <typename Type>
	static void SetScene()
	{
		SetLockFlag(false);

		if (m_Scene != nullptr)
		{
			m_Scene->Uninit();
			delete m_Scene;
		}
			Type* Scene = new Type();

			Scene->Init();

			m_Scene = Scene;
	}

	static void Init();
	static void Uninit();
	static void Update();
	static void Draw();

	static CScene* GetScene(){ return m_Scene; }
	static void SetLockFlag(bool _);
	static bool GetLockFlag();

};